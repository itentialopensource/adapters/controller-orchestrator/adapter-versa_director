## 0.9.0 [08-11-2023]

- Migration to bring up to the latest foundation
  - Changes Order of Precedence for Token/Healthcheck (service instance config top)
  - Scrub sensitive data from logs
  - Update most dependencies
  - Compatibility with Node 14, 16 & 18
  - Added inventory, linting & testing calls
  - Major version of adapter-utils (5.x.x)
  - Generic Handler in adapter-utils (added new generic call)
  - Cache Handler in adapter-utils (added new cache capabilities)
  - Broker Handler in adapter-utils

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!16)

## 0.8.4 [08-01-2023]

* Update adapter base and import order

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!15)

---

## 0.8.3 [05-09-2023]

* Patch/migrate adapter base

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!14)

---

## 0.8.2 [03-03-2023]

* Updated mock data files

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!13)

---

## 0.8.1 [02-15-2023]

* Update healthcheck entitypath and schemaTokenResp

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!11)

---

## 0.8.0 [05-21-2022]

* Migration to the latest Adapter Foundation

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!10)

---

## 0.7.0 [12-31-2021]

- Add Device Broker integration to the Versa Adapter - this adds calls that allows for Versa Appliances to be viewed in Config Manager. There is one original adapter call getDevice that had to be renamed to getVersaDevice because the getDevice call is a broker call. So this call will break if it was being used. Everything else is non breaking.

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!9)

---

## 0.6.0 [10-28-2021]

- Add new calls

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!8)

---

## 0.5.0 [06-04-2021]

- Added 3 calls and added a forth with changed params

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!7)

---

## 0.4.1 [03-23-2021]

- Added a new call that is needed and also fix some issues where additional curly braces were being added to the body objects.

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!6)

---

## 0.4.0 [03-19-2021]

- Added the request new calls to the adapter - 4 added the other 2 already existed.

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!5)

---

## 0.3.2 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!4)

---

## 0.3.1 [02-19-2021]

- Make changes to importTemplateString

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!3)

---

## 0.3.0 [02-18-2021]

- Added 2 calls including getTemplateFromOrganization

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!2)

---

## 0.2.0 [02-17-2021]

- Add more calls and merge in the calls from the original adapter-versa_director

[Merge Request Details](https://itentialopensource/adapters/controller-orchestrator/adapter-versa_director!1)

---

## 0.1.1 [10-20-2020]

- Initial Commit

See commit 96fa9d2

---
