# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Versa_director System. The API that was used to build the adapter for Versa_director is usually available in the report directory of this adapter. The adapter utilizes the Versa_director API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Versa Networks Director adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Versa Networks Director. With this adapter you have the ability to perform operations such as:

- Create, Modify, Manage and Delete Versa Appliances. 
- Organization
- Region
- Template
- Policy
- Network

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
