## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Versa Director. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Versa Director.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Versa Networks Director. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">assignAlarmObject(alarm, assignee, description, state, callback)</td>
    <td style="padding:15px">Assign an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlarmAssignee(assignee, description, deviceName, managedObject, org, specificProblem, state, type, callback)</td>
    <td style="padding:15px">Assign an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAlarm(deviceName, managedObject, org, specificProblem, type, callback)</td>
    <td style="padding:15px">Clear an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleAlarmObject(alarmAlarmHandlings0AssignedBy, alarmAlarmHandlings0Description, alarmAlarmHandlings0State, alarmAlarmHandlings0Time, alarmAlarmHandlings0User, alarmDevice, alarmDeviceGroup, alarmDeviceName, alarmIsCleared, alarmLastAlarmText, alarmLastPerceivedSeverity, alarmLastStatusChangeTimeStamp, alarmObject, alarmOrg, alarmSerial, alarmSeverity, alarmStatusChanges0AlarmText, alarmStatusChanges0EventTime, alarmStatusChanges0ReceivedTime, alarmStatusChanges0Severity, alarmType, assignee, description, specificProblem, state, callback)</td>
    <td style="padding:15px">Handle an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/handle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHandleAlarm(description, deviceName, managedObject, org, specificProblem, state, type, callback)</td>
    <td style="padding:15px">Handle an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/handle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmHandling(deviceName, managedObject, org, specificProblem, type, callback)</td>
    <td style="padding:15px">Retrieve handlings associated with an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/handling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmHandlingObject(alarmHandlings0AssignedBy, alarmHandlings0Description, alarmHandlings0State, alarmHandlings0Time, alarmHandlings0User, device, deviceGroup, deviceName, isCleared, lastAlarmText, lastPerceivedSeverity, lastStatusChangeTimeStamp, object, org, serial, severity, specificProblem, statusChanges0AlarmText, statusChanges0EventTime, statusChanges0ReceivedTime, statusChanges0Severity, type, callback)</td>
    <td style="padding:15px">Retrieve handlings associated with an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/handling?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purgeAlarmObject(alarmHandlings0AssignedBy, alarmHandlings0Description, alarmHandlings0State, alarmHandlings0Time, alarmHandlings0User, device, deviceGroup, deviceName, isCleared, lastAlarmText, lastPerceivedSeverity, lastStatusChangeTimeStamp, object, org, serial, severity, specificProblem, statusChanges0AlarmText, statusChanges0EventTime, statusChanges0ReceivedTime, statusChanges0Severity, type, callback)</td>
    <td style="padding:15px">purgeAlarmObject</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/object/purge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purgeAlarm(deviceName, managedObject, org, specificProblem, type, callback)</td>
    <td style="padding:15px">purgeAlarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/purge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusChange(deviceName, managedObject, org, specificProblem, type, callback)</td>
    <td style="padding:15px">Retrieve status changes associated with an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatusChangeObject(alarmHandlings0AssignedBy, alarmHandlings0Description, alarmHandlings0State, alarmHandlings0Time, alarmHandlings0User, device, deviceGroup, deviceName, isCleared, lastAlarmText, lastPerceivedSeverity, lastStatusChangeTimeStamp, object, org, serial, severity, specificProblem, statusChanges0AlarmText, statusChanges0EventTime, statusChanges0ReceivedTime, statusChanges0Severity, type, callback)</td>
    <td style="padding:15px">Retrieve status changes associated with an alarm</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarm/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFilteredAlarms(deviceName, filtertype, isCleared, isDeep, lastAlarmText, lastPerceivedSeverity, lastStatusChange, org, type, callback)</td>
    <td style="padding:15px">Retrieve alarms satisfying certain conditions</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAllFilteredAlarms(assignee, description, deviceName, filtertype, isCleared, lastAlarmText, lastPerceivedSeverity, lastStatusChange, org, state, type, callback)</td>
    <td style="padding:15px">Bulk assign alarms matching certain conditions</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAllAlarms(alarmData, callback)</td>
    <td style="padding:15px">Bulk assign a list of specific alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/bulk/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAllAlarms(alarmData, callback)</td>
    <td style="padding:15px">Bulk clear multiple alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/bulk/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purgeAllAlarms(alarmData, callback)</td>
    <td style="padding:15px">purgeAllAlarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/bulk/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleAllAlarms(alarmData, callback)</td>
    <td style="padding:15px">Bulk handle a list of specific alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/bulk/handle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">handleAllFilteredAlarms(description, deviceName, filtertype, isCleared, lastAlarmText, lastPerceivedSeverity, lastStatusChange, org, state, type, callback)</td>
    <td style="padding:15px">Bulk handle all alarms satisfying certain parameters</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/handle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterPaginateAlarm(deviceName, filtertype, forceRefresh, includeChildren, isCleared, isDeep, lastAlarmText, lastPerceivedSeverity, lastStatusChange, limit, offset, org, showSystemAlarm, sortColumn, sortOrder, type, callback)</td>
    <td style="padding:15px">Retrieve alarms satisfying certain conditions</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/page?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purgeAllFilteredAlarms(alarmHandlingState, alarmHandlingUser, deviceName, filtertype, isCleared, lastAlarmHandlingChange, lastAlarmHandlingState, lastAlarmText, lastPerceivedSeverity, lastStatusChange, org, type, callback)</td>
    <td style="padding:15px">purgeAllFilteredAlarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/purge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmSummary(callback)</td>
    <td style="padding:15px">Retrieve alarm count by severity for all alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceAlarmSummary(deviceName, org, callback)</td>
    <td style="padding:15px">Retrieve alarm count by severity for one device</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/summary/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmSummaryPerOrg(includeChildren, includeSystem, org, callback)</td>
    <td style="padding:15px">Retrieve alarm count by severity for all alarms for a tenant</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectorAlarms(searchString, severity, callback)</td>
    <td style="padding:15px">Retrieve director-specific alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/director/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectorAlarmSummary(callback)</td>
    <td style="padding:15px">Retrieve summary alarm counts for director-specific alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/director/alarms/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectorFailOverAlarms(callback)</td>
    <td style="padding:15px">Retrieve fail-over alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/director/fail-over-alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectorHAAlarms(callback)</td>
    <td style="padding:15px">Retrieve HA-related alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/director/ha-alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImpAlarms(callback)</td>
    <td style="padding:15px">Retrieve license-related alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/director/pop-up?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImpAlarmSummary(callback)</td>
    <td style="padding:15px">Retrieve summary count of license-related alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/director/pop-up-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmTypes(callback)</td>
    <td style="padding:15px">Retrieve alarm types</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmNotifications(filters, limit, offset, callback)</td>
    <td style="padding:15px">Retrieve all alarm-based notification rules with filtering</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveAlarmNotification(rules, callback)</td>
    <td style="padding:15px">Save new notification rule(s)</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAlarmNotification(rules, callback)</td>
    <td style="padding:15px">Edit alarm-based notification rule(s)</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlarmNotification(groupkey, key, callback)</td>
    <td style="padding:15px">Delete the named notification rule</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadAllAlarmNotifications(callback)</td>
    <td style="padding:15px">Retrieve all alarm-based notification rules</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkDeleteAlarmNotification(groupkey, key, callback)</td>
    <td style="padding:15px">Delete multiple alarm-based notification rules</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification/bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshAlarmNotification(callback)</td>
    <td style="padding:15px">Refresh and reload all alarm-based notification rules</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmNotificationRule(key, callback)</td>
    <td style="padding:15px">Retrieve a notification rule</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAlarmNotification(searchString, callback)</td>
    <td style="padding:15px">Retrieve all alarm-based notification rules matching a search string</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarm/notification/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMQPEvents(limit, offset, callback)</td>
    <td style="padding:15px">Get AMQP Events</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/amqp/amqpevents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAMQPObjEvents(limit, offset, callback)</td>
    <td style="padding:15px">Get AMQP Object Events</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/amqp/amqpobjevents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplianceStatus(callback)</td>
    <td style="padding:15px">Shows Appliance status for all Appliances</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/appliance/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceStatusById(byName, id, callback)</td>
    <td style="padding:15px">Shows single Appliance status based on UUID</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/appliance/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceTemplateListing(deviceName, tenant, callback)</td>
    <td style="padding:15px">Shows Templates linked with Device</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/appliance/template_listing/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliances(limit, offset, tags, type, callback)</td>
    <td style="padding:15px">Get All Appliances By Type and Tags</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliances2(limit, offset, callback)</td>
    <td style="padding:15px">Get All Appliances</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/appliance2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesByTypeAndOrg(org, type, callback)</td>
    <td style="padding:15px">Get Appliances By Type and Org</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/appliancesByOrgAndType/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesByType(type, callback)</td>
    <td style="padding:15px">Get Appliances By Type</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/appliancesByType/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesForSecurityPackage(callback)</td>
    <td style="padding:15px">getAppliancesForSecurityPackage</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/appliancesForSecurityPackage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportApplianceConfiguration(applianceName, callback)</td>
    <td style="padding:15px">Export Appliance Configuration</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">filterAppliances(filterString, limit, offset, callback)</td>
    <td style="padding:15px">Search Appliance By Filter</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesForOrganization(filterString, limit, offset, org, callback)</td>
    <td style="padding:15px">Get Appliances For Given Org</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/filter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsForAppliance(callback)</td>
    <td style="padding:15px">Get Appliance Tags</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/getTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsForTenant(tenantName, callback)</td>
    <td style="padding:15px">getTagsForTenant</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/getTags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importApplianceConfiguration(applianceName, file, callback)</td>
    <td style="padding:15px">Import Appliance Configuration</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setApplianceOwner(applianceNames, orgname, callback)</td>
    <td style="padding:15px">Get Appliance Owner Status</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/owner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesForTenantOrgs(callback)</td>
    <td style="padding:15px">getAppliancesForTenantOrgs</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/ownerOrg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAppliances(limit, offset, sql, callback)</td>
    <td style="padding:15px">Search Appliance By Filter</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesSummary(filters, callback)</td>
    <td style="padding:15px">Get Appliances Summary</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">doSyncFromAppliance(device, callback)</td>
    <td style="padding:15px">Sync Config From Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/syncfrom/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">doSyncToAppliance(device, callback)</td>
    <td style="padding:15px">Sync Config To Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/syncto/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagsFromAppliance(applianceName, deleteTagList, callback)</td>
    <td style="padding:15px">Delete Appliance Tags</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/{pathv1}/deleteTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorForAppliance(applianceName, vmName, callback)</td>
    <td style="padding:15px">Get Guest VNF Vendor</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/{pathv1}/guest-vnfs/vm/{pathv2}/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplianceRoutingInstanceInformation(applianceName, callback)</td>
    <td style="padding:15px">Get Routing Instance Information</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/{pathv1}/routing-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceRoutingInstanceInformationById(applianceName, routingInstanceName, callback)</td>
    <td style="padding:15px">Get Routing Instance Information</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/{pathv1}/routing-instances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTagsForAppliance(applianceName, setTagList, callback)</td>
    <td style="padding:15px">Create Appliance Tags</td>
    <td style="padding:15px">{base_path}/{version}/vnms/appliance/{pathv1}/setTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationServiceTemplates(limit, offset, organization, callback)</td>
    <td style="padding:15px">Application Service Templates Fetch All</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplicationServiceTemplate(newApplicationServiceTemplate, callback)</td>
    <td style="padding:15px">Create  a specific Application Service Template</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationServiceTemplateSamples(callback)</td>
    <td style="padding:15px">Application Service Template Fetch All Samples</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate/allSamples?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneApplicationServiceTemplate(newOrg, newTemplate, serviceTemplateName, callback)</td>
    <td style="padding:15px">Clone  a specific Application Service Template</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate/clone/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployApplicationServiceTemplate(serviceTemplateName, callback)</td>
    <td style="padding:15px">Deploy a specific appliation Specific Template</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateApplicationServiceTemplate(astName, newApplicationServiceTemplate, callback)</td>
    <td style="padding:15px">Update  a Application Service Template</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplicationServiceTemplate(serviceTemplateNames, callback)</td>
    <td style="padding:15px">Delete a specific appliation Specific Template</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationServiceTemplateById(serviceTemplateName, callback)</td>
    <td style="padding:15px">Get a Application Service Template  given its name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/applicationServiceTemplate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCommunityStrings(organization, callback)</td>
    <td style="padding:15px">Community  Fetch All</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/community?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCommunity(community, callback)</td>
    <td style="padding:15px">Create  a Community</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/community?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkCommunities(communityBeanCeate, callback)</td>
    <td style="padding:15px">Create  a Set of Communities</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/community/bulkCommunityCreate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableCommunityId(org, callback)</td>
    <td style="padding:15px">Get the next available Community Id</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/community/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getControllerWorkflowList(requestParams, callback)</td>
    <td style="padding:15px">Fetch all controller workflows</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createJsonControllerWorkflow(sdwanControllerWorkflowVO, callback)</td>
    <td style="padding:15px">Create controller workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/controllers/controller?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteControllerWorkflows(name, callback)</td>
    <td style="padding:15px">Delete all controller workflows</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/controllers/controller?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployControllerWorkflow(controllerworkflowname, requestParams, taskid, callback)</td>
    <td style="padding:15px">Deploy a specific controller workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/controllers/controller/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJsonControllerWorkflow(controllerworkflowname, callback)</td>
    <td style="padding:15px">Fetch a specific controller workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/controllers/controller/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateJsonControllerWorkflow(controllerworkflowname, sdwanControllerWorkflowVO, callback)</td>
    <td style="padding:15px">Update controller workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/controllers/controller/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteControllerWorkflow(controllerworkflowname, callback)</td>
    <td style="padding:15px">Delete a specific controller workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/controllers/controller/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceWorkflows(filters, limit, offset, orgname, callback)</td>
    <td style="padding:15px">Device WorkFlow  Fetch All</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevicefromJson(sdwanWorkflowWrapperData, callback)</td>
    <td style="padding:15px">Create  a specific Device work flow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deploybulkDevicesJson(deviceWorkflowNamesListWrapper, callback)</td>
    <td style="padding:15px">Bulk Deploy Device WorkFlow  given its names</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/bulkDeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployDevice(deviceworkflowname, callback)</td>
    <td style="padding:15px">Deploy Device WorkFlow  given its name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceWorkflowTemplateDataInJson(deviceWorkflowDatainJson, deviceworkflowname, callback)</td>
    <td style="padding:15px">Get   Device work flow  template data </td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/template/data/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHaPairedSiteLocationIdMapByTemplate(org, templateName, callback)</td>
    <td style="padding:15px">Get Ha Pair SiteLocation IdMap by Template </td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/template/pairedsiteid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceJsonWorkflow(deviceworkflowname, callback)</td>
    <td style="padding:15px">Get a specific Device WorkFlow  given its name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevicefromJson(sdwanWorkflowWrapperData, deviceworkFlowName, callback)</td>
    <td style="padding:15px">Update  a specific Device work flow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deCommisionDeleteWorkflow(cleanConfig, deviceworkflowname, loadDeviceDefaults, resetConfig, callback)</td>
    <td style="padding:15px">Decommision a specific Device WorkFlow  given its name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportDeviceWorkflow(deviceworkflowname, includeAutoGenData, callback)</td>
    <td style="padding:15px">Export  a specific Device work flow  data</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/device/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDeviceWorkflows(file, callback)</td>
    <td style="padding:15px">Import one or more  Device work flow  data </td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDeviceWorkflowProgressStatus(uuid, callback)</td>
    <td style="padding:15px">Progress of import  Device work flow  data </td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/import/progress/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDeviceWorkflowResult(uuid, callback)</td>
    <td style="padding:15px">Result of import  Device work flow  data </td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/import/result/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceOwnerStatusInJson(appliancesNames, orgname, callback)</td>
    <td style="padding:15px">Get   Device owner status </td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/owner/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceteWorkflows(cleanConfig, deviceWorkflowNames, loadDeviceDefaults, resetConfig, callback)</td>
    <td style="padding:15px">Delete a specific Device WorkFlow  given its name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrateSDWANWorkflow(callback)</td>
    <td style="padding:15px">migrate</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/migrate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWorkflowList(limit, offset, searchKey, callback)</td>
    <td style="padding:15px">Fetch all organization workflows</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgWorkflowFromJson(content, callback)</td>
    <td style="padding:15px">Create organization workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/orgs/org?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployOrgWorkflow(orgWorkflowName, taskid, callback)</td>
    <td style="padding:15px">Deploy a specific organization workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/orgs/org/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWorkflowFromJson(orgWorkflowName, callback)</td>
    <td style="padding:15px">Fetch a specific organization workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/orgs/org/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWorkflowFromJson(orgWorkflowName, sdwanOrgWorkflowVOWrapper, callback)</td>
    <td style="padding:15px">Update organization workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/orgs/org/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decommissionOrgWorkflow(orgWorkflowName, taskid, callback)</td>
    <td style="padding:15px">Decommission a specific organization workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/orgs/org/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWorkflows(orgWorkflowName, callback)</td>
    <td style="padding:15px">Delete a specific organization workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/orgs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUCPEServiceChains(requestParams, callback)</td>
    <td style="padding:15px">Fetch all ServiceChain workflows</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/servicechains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUCPEServiceChain(ucpeServiceChainWorkflowVO, callback)</td>
    <td style="padding:15px">Create ServiceChain workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/servicechains/servicechain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployUCPEServiceChain(taskid, uCPEServiceChainWorkflowName, callback)</td>
    <td style="padding:15px">Deploy a specific ServiceChain workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/servicechains/servicechain/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUCPEServiceChainById(uCPEServiceChainWorkflowName, callback)</td>
    <td style="padding:15px">Fetch a specific ServiceChain workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/servicechains/servicechain/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUCPEServiceChain(uCPEServiceChainWorkflowName, ucpeServiceChainWorkflowVO, callback)</td>
    <td style="padding:15px">Update ServiceChain workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/servicechains/servicechain/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUCPEServiceChain(uCPEServiceChainWorkflowName, callback)</td>
    <td style="padding:15px">Delete a specific ServiceChain workflow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/servicechains/servicechain/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTemplatesWorkflows(limit, offset, orgname, searchKeyword, callback)</td>
    <td style="padding:15px">Template Fetch All</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplateWorkflow(content, callback)</td>
    <td style="padding:15px">Create  a specific template work flow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployTemplateWorkflow(templateworkflowname, verifyDiff, callback)</td>
    <td style="padding:15px">Deploy a specific template work flow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates/template/deploy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateWorkflowById(templateworkflowname, callback)</td>
    <td style="padding:15px">Get a specific template work flow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTemplateWorkflow(sdwanTemoplateWorkflowJsonWrapper, templateworkflowname, callback)</td>
    <td style="padding:15px">Update  a specific template work flow</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decommisionTemplateWorkflow(templatename, callback)</td>
    <td style="padding:15px">Decommision  template work flow given its name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateWorkflows(templatenames, callback)</td>
    <td style="padding:15px">Delete  template work flow given its name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPortToTemplateWorkflow(content, templateName, callback)</td>
    <td style="padding:15px">Add ports to the specified Template</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/workflow/templates/{pathv1}/addPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRbacPrivilege(callback)</td>
    <td style="padding:15px">Get all privileges in the system</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/privileges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshRbacRolesMetaInMemory(callback)</td>
    <td style="padding:15px">Refresh all roles in cache</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/cache/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshRbacRolesMetaInMemoryById(roleName, callback)</td>
    <td style="padding:15px">Refresh role by name in cache</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/cache/refresh/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacOrgRole(supportedOrgRoles, callback)</td>
    <td style="padding:15px">Associate roles to org</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/orgs/org?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableRbacOrgRoles(callback)</td>
    <td style="padding:15px">Fetch all available roles for assigning to org</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/orgs/org/available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacOrgRoles(rolename, callback)</td>
    <td style="padding:15px">Fetch assigned roles for a given org</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/orgs/org/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacProviderRoles(roleType = 'PROVIDER', searchKey, callback)</td>
    <td style="padding:15px">getProviderRoles</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRbacRole(createRole, callback)</td>
    <td style="padding:15px">Create custom role with list of privileges definition</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRbacRole(createRole, callback)</td>
    <td style="padding:15px">update custom role with list of privileges definition</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacRoleById(rolename, callback)</td>
    <td style="padding:15px">Get role definition by role name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacRole(rolename, callback)</td>
    <td style="padding:15px">Delete custom role</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployRbacRole(rolename, callback)</td>
    <td style="padding:15px">Deploy custom role</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/role/{pathv1}/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deployRbacRoleAsync(rolename, taskid, callback)</td>
    <td style="padding:15px">Deploy custom role in async</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/role/{pathv1}/deploy/async?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacTenantRoles(roleType = 'PROVIDER', searchKey, callback)</td>
    <td style="padding:15px">getTenantRoles</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/rbac/roles/tenant?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersaDevice(deviceName, callback)</td>
    <td style="padding:15px">Shows Templates associated to a Device</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceGroups(filters, limit, offset, organization, callback)</td>
    <td style="padding:15px">Device Group  Fetch All</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroup(deviceGroupWriteWrapper, callback)</td>
    <td style="padding:15px">Create  a specific Device group</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkDeleteDeviceGroup(deviceGroupNames, callback)</td>
    <td style="padding:15px">Delete a bunch of  Device groups  given its names</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/bulkDelete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceGroupsNames(organization, callback)</td>
    <td style="padding:15px">Device Group  Names Fetch All</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/deviceGroupNames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceGroupsByAnyTemplateAndOrg(organization, template, callback)</td>
    <td style="padding:15px">Device Group  Names Fetch  for a given template and org </td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/deviceGroupNamesByAnyTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceGroupsByTemplate(limit, offset, template, callback)</td>
    <td style="padding:15px">Device Group  Names Fetch  for a given template</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/deviceGroupNamesByTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupById(deviceGroupName, callback)</td>
    <td style="padding:15px">Get a specific Device group  given its name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroup(deviceGroupWriteWrapper, deviceGroupName, callback)</td>
    <td style="padding:15px">Update  a specific Device group</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroup(deviceGroupName, callback)</td>
    <td style="padding:15px">Delete a specific Device group  given its name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupStandbyInfo(deviceGroupName, callback)</td>
    <td style="padding:15px">Get a specific Device group  standby given its name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/{pathv1}/standby?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupTemplateAssociation(deviceGroupName, postStagingTemplateName, callback)</td>
    <td style="padding:15px">Get a specific Device group  assocated with template</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/deviceGroup/{pathv1}/template/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerDevice(dataAdditionalProperties, dataDeviceName, dataDeviceSerialNubmer, dataRegistrationToken, callback)</td>
    <td style="padding:15px">register</td>
    <td style="padding:15px">{base_path}/{version}/vnms/devicereg/api/sdwan/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegistrationDeviceInfo(regtoken, callback)</td>
    <td style="padding:15px">Get Device Registration info from token</td>
    <td style="padding:15px">{base_path}/{version}/vnms/devicereg/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerDeviceAsync(regtoken, callback)</td>
    <td style="padding:15px">External 2 Factor Auth Was completed outside if Versa Director and Start Device Registration Process</td>
    <td style="padding:15px">{base_path}/{version}/vnms/devicereg/device/{pathv1}/pre-staging?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDeviceEMAILSecureCode(regtoken, callback)</td>
    <td style="padding:15px">Send Short Code via email to Administrator of Device or Administrator of Device Group</td>
    <td style="padding:15px">{base_path}/{version}/vnms/devicereg/device/{pathv1}/securecode/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateDeviceSMSSecureCode(regtoken, callback)</td>
    <td style="padding:15px">Send Short SMS Code to Administrator of Device or Administrator of Device Group</td>
    <td style="padding:15px">{base_path}/{version}/vnms/devicereg/device/{pathv1}/securecode/sms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyDeviceSecureCodeAndRegisterAsync(regtoken, securecode, callback)</td>
    <td style="padding:15px">Verify Short Code send via email or SMS and upon sucessful verification start Device Registration Processs</td>
    <td style="padding:15px">{base_path}/{version}/vnms/devicereg/device/{pathv1}/verify/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportDocsSwagger(docType = 'JSON', group = 'Alarm Notification API', callback)</td>
    <td style="padding:15px">Export Yaml or JSON document based on group</td>
    <td style="padding:15px">{base_path}/{version}/export-docs/swagger/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssets(filters, limit, offset, organization, callback)</td>
    <td style="padding:15px">Get All Assets</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">availableAssets(callback)</td>
    <td style="padding:15px">Get list of available assets without serialnumber</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/asset/availableAssets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateAssetEmail(deviceNames, email, callback)</td>
    <td style="padding:15px">Generate email for asset registration</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/asset/{pathv1}/generateEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAsset(asset, deviceName, callback)</td>
    <td style="padding:15px">Update  a specific Asset </td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelAssetSerialNumberReplacement(originalSerialNo, callback)</td>
    <td style="padding:15px">Cancel Association of Branch with a serial number</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/asset/{pathv1}/cancel-replace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceAsset(originalSerialNo, replacementSerialNo, callback)</td>
    <td style="padding:15px">Replace the serial number of a asset</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/asset/{pathv1}/replace/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateBranchWithSerialNumber(branchName, serialNo, callback)</td>
    <td style="padding:15px">Associate Branch with a serial number</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/asset/{pathv1}/associate/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnknownDevices(callback)</td>
    <td style="padding:15px">Get the list of all unknown devices</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/unknownDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllUnknownDevices(serialNumber, callback)</td>
    <td style="padding:15px">Delete all unknown devices</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/unknownDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnknownDeviceById(serialNumber, callback)</td>
    <td style="padding:15px">Get a unknown device by serial number</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/unknownDevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnknownDeviceById(serialNumber, callback)</td>
    <td style="padding:15px">Delete the unknwn device with serial number</td>
    <td style="padding:15px">{base_path}/{version}/vnms/assets/unknownDevices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapGroups(attrname, device, groupprofile, ldapprofile, organization, search, templateName, callback)</td>
    <td style="padding:15px">Fetch LDAP Groups</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapServerProfile(ldapprofile, organization, callback)</td>
    <td style="padding:15px">Fetch LDAP Server Profile For Organization</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/ldapserverprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLdapServerProfile(device, ldapprofile, organization, callback)</td>
    <td style="padding:15px">Create LDAP Server Profile</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/ldapserverprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgToLDAPServerProfilesMap(callback)</td>
    <td style="padding:15px">Fetch Organization To LDAP Server Profiles</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/ldapserverprofile/orgtoldapserverprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapServerProfiles(organization, callback)</td>
    <td style="padding:15px">Fetch List Of LDAP Server Profile For Organization</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/ldapserverprofiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLdapUsers(attrname, device, groupprofile, ldapprofile, organization, search, templateName, callback)</td>
    <td style="padding:15px">Fetch LDAP Users</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLdapServerProfile(device, ldapprofile, organization, callback)</td>
    <td style="padding:15px">Update LDAP Server Profile</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLdapServerProfile(ldapprofile, organization, callback)</td>
    <td style="padding:15px">Delete LDAP Server Profile</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testSmtpSettingResponse(smtpTestParametersBean, callback)</td>
    <td style="padding:15px">Send Test Email</td>
    <td style="padding:15px">{base_path}/{version}/vnms/notification/testsetting/smtp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateAndSaveSPAndIDPMetadata(ssoConfigWrapper, callback)</td>
    <td style="padding:15px">Save OpenID Connect SP and IDP Metadata</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/openid/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">udpateSPAndIDPMetadata(ssoConfigWrapper, callback)</td>
    <td style="padding:15px">Update OpenID Connect SP and IDP Metadata</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/openid/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextgenOrganizations(column, limit, offset, uuidOnly, value, callback)</td>
    <td style="padding:15px">Get all organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrganization(organization, callback)</td>
    <td style="padding:15px">Create organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationAgents(orgName, callback)</td>
    <td style="padding:15px">Get agents for organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsDetails(orgUUIDs, callback)</td>
    <td style="padding:15px">Get VrfGroups and wan-networks by organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRootOrganizations(callback)</td>
    <td style="padding:15px">Get all the roots of organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/roots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailsForUUid(uuid, callback)</td>
    <td style="padding:15px">Get organization by uuid</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/uuid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgVrfs(orgName, callback)</td>
    <td style="padding:15px">Get VrfGroups by organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/vrfs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationDetails(orgname, uuidOnly, callback)</td>
    <td style="padding:15px">Get organization by name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganization(organization, callback)</td>
    <td style="padding:15px">Update organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganization(orgname, callback)</td>
    <td style="padding:15px">Delete Organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgWanNetworkGroups(orguuid, callback)</td>
    <td style="padding:15px">Get wan network group by organization</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}/wan-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrgWanNetworkGroup(wanNetworkGroup, orguuid, callback)</td>
    <td style="padding:15px">Create wan network group by uuid</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}/wan-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWanNetworkGroups(name, orguuid, callback)</td>
    <td style="padding:15px">Delete wan network groups</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}/wan-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrgWanNetworkGroup(wanNetworkGroups, orguuid, callback)</td>
    <td style="padding:15px">Update wan network group by org-uuid</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}/wan-networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrgWanNetworkGroup(name, orguuid, callback)</td>
    <td style="padding:15px">Delete wan network group</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/organization/{pathv1}/wan-networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizations(deep, filters, limit, offset, callback)</td>
    <td style="padding:15px">Get organizations</td>
    <td style="padding:15px">{base_path}/{version}/vnms/organization/orgs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgsCount(callback)</td>
    <td style="padding:15px">Get organization list count</td>
    <td style="padding:15px">{base_path}/{version}/vnms/organization/orgs/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setOrgWanPropagate(wanPropagate, callback)</td>
    <td style="padding:15px">setWanPropagate</td>
    <td style="padding:15px">{base_path}/{version}/vnms/organization/wanNetworkPropagate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZonesForOrganization(org, callback)</td>
    <td style="padding:15px">getZonesForOrganization</td>
    <td style="padding:15px">{base_path}/{version}/vnms/organization/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgChildren(deep, org, callback)</td>
    <td style="padding:15px">Get children for the organizations</td>
    <td style="padding:15px">{base_path}/{version}/vnms/organization/{pathv1}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllOsspackPossibleCurrentVersionOnDevice(callback)</td>
    <td style="padding:15px">Get all current versions of os spack for device</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/device/all-current-versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOsspackDownloadsForDevice(limit, offset, callback)</td>
    <td style="padding:15px">Get all os spack downloads for device</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/device/all-downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkDeviceOsspackUpdates(currVersion, updateType, callback)</td>
    <td style="padding:15px">Get os spack updates for device</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/device/check-osspack-updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkDeviceOsspackUpdatesByDeviceName(device, updateType, callback)</td>
    <td style="padding:15px">Get os spack updates given device name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/device/check-osspack-updates-by-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOsspackFromDevice(updateType, version, callback)</td>
    <td style="padding:15px">Delete os spack for device</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/device/delete-osspack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceOsspackDownloadsToBeInstalled(limit, offset, callback)</td>
    <td style="padding:15px">Get available os spack downloads for device</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/device/downloads-available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">instalOsspackOnlDevices(devicesInstallBean, taskid, callback)</td>
    <td style="padding:15px">Install os spack for device</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/device/install-osspack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOsspackDownloadsForDirector(limit, offset, callback)</td>
    <td style="padding:15px">Get all os spack downloads for director</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/director/all-downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkDirectorOsspackUpdates(updateType, callback)</td>
    <td style="padding:15px">Get os spack updates for director</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/director/check-osspack-updates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOsspackFromDirector(updateType, version, callback)</td>
    <td style="padding:15px">Delete os spack for director</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/director/delete-osspack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDirectorOsspackDownloadsToBeInstalled(limit, offset, callback)</td>
    <td style="padding:15px">Get available os spack downloads for director</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/director/downloads-available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installDirectorOsspack(directorInstallBean, taskid, callback)</td>
    <td style="padding:15px">Install os spack for director</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/director/install-osspack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadOsspack(product, taskid, updateType, version, callback)</td>
    <td style="padding:15px">Download os spack</td>
    <td style="padding:15px">{base_path}/{version}/vnms/osspack/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminAlerts(callback)</td>
    <td style="padding:15px">getAlerts</td>
    <td style="padding:15px">{base_path}/{version}/vnms/system/admin/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificateRemainingDays(callback)</td>
    <td style="padding:15px">Fetch expiry time and validity of Digital Certificate</td>
    <td style="padding:15px">{base_path}/{version}/vnms/system/admin/digital-certificate/remaining-time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadApplianceDebPackage(description, fileType, name, productType, uploadFile, url, urlFormData, callback)</td>
    <td style="padding:15px">Upload FlexVNF or Director Image to Director</td>
    <td style="padding:15px">{base_path}/{version}/vnms/system/admin/packages/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemLogLevel(logger, callback)</td>
    <td style="padding:15px">Fetch Logger Level</td>
    <td style="padding:15px">{base_path}/{version}/vnms/system/config/logging/{pathv1}/level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSystemLogLevel(level, logger, callback)</td>
    <td style="padding:15px">Update Logger Level</td>
    <td style="padding:15px">{base_path}/{version}/vnms/system/config/logging/{pathv1}/level/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkUpdateMinImageVersion(prefImageVersionJson, callback)</td>
    <td style="padding:15px">Update Preferred Image Vesion</td>
    <td style="padding:15px">{base_path}/{version}/vnms/system/updateMinImageVersion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRegions(filters, limit, offset, callback)</td>
    <td style="padding:15px">Fetch list of regions</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRegion(region, callback)</td>
    <td style="padding:15px">Create a specific region</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextAvailableRegionId(callback)</td>
    <td style="padding:15px">Fetch next available region id</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/regions/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegionById(regionName, callback)</td>
    <td style="padding:15px">Fetch a specific region</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRegion(region, regionName, callback)</td>
    <td style="padding:15px">Update a specific region</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRegion(regionName, callback)</td>
    <td style="padding:15px">Delete a specific region given its name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateAndSaveIDPMetadata(connectorname, idpmetadataxml, callback)</td>
    <td style="padding:15px">Save SAML IDP Metadata</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/idpmetadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSOOverallStatus(callback)</td>
    <td style="padding:15px">Get overall SSO Status</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/overallstatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSORolesMappingById(tenantname, callback)</td>
    <td style="padding:15px">Get SSO role mapping</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/rolesmapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSSORolesMapping(mapper, tenantname, callback)</td>
    <td style="padding:15px">Update SSO role mapping</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/rolesmapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSSORolesMapping(tenantname, callback)</td>
    <td style="padding:15px">Delete SSO role mapping</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/rolesmapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateAndSaveSPMetadata(ssoConfigWrapper, callback)</td>
    <td style="padding:15px">Save SAML SP Metadata</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/spmetadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSPMetadata(ssoConfigWrapper, callback)</td>
    <td style="padding:15px">Update SAML SP Metadata</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/spmetadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSOStatus(orgname, callback)</td>
    <td style="padding:15px">Get SSO Status</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSSOStatusWithURL(clientname, orgname, callback)</td>
    <td style="padding:15px">Get SSO Status</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sso/statuswithurl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANAssetsByOrgNameAndDeviceType(deviceType, orgName, region, callback)</td>
    <td style="padding:15px">getAssetsByOrgNameAndDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/assets/byOrgAndDevType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANDeviceMappingURL(controller, devicename, callback)</td>
    <td style="padding:15px">getDeviceMappingURL</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/device-url-mappings/device-url-mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANAvailableIds(count, objectType = 'Controller', callback)</td>
    <td style="padding:15px">Get next(n) available ids</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/availableIds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANNextGlobalId(objectType = 'Controller', callback)</td>
    <td style="padding:15px">Get next available id</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/{pathv1}/availableId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANNextGlobalIdWithSerialNumber(objectType = 'Controller', callback)</td>
    <td style="padding:15px">Get next available id and serial number</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/{pathv1}/availableId/withSerialNumber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">freeAllSDWANCachedValues(objectType = 'Controller', callback)</td>
    <td style="padding:15px">Free temporarily reserved global-id from cache by type</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/{pathv1}/freeAllCachedValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">freeSDWANCachedAvailableGlobalId(availableId, objectType = 'Controller', callback)</td>
    <td style="padding:15px">Free temporarily reserved global-id from cache by type and id</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/{pathv1}/freecached/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANStagingControllers(organization, callback)</td>
    <td style="padding:15px">getStagingControllers</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/staging-controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANStagingControllerVPNProfiles(controller, callback)</td>
    <td style="padding:15px">getStagingControllerVPNProfiles</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/staging-controllers/controller/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANURLBasedZTPInfo(deviceGroup, callback)</td>
    <td style="padding:15px">getURLBasedZTPInfo</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/url-ztp-info/device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteToSiteTunnelVPNProfile(siteToSiteTunnelVPNProfileVO, callback)</td>
    <td style="padding:15px">Create SiteToSite Tunnel VPN Profile</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sitetosite/vpn/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteToSiteTunnelVPNProfile(siteToSiteTunnelVPNProfileVO, callback)</td>
    <td style="padding:15px">Update SiteToSite Tunnel VPN Profile</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sitetosite/vpn/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteToSiteTunnelVPNProfileNames(orgUuid, callback)</td>
    <td style="padding:15px">Fetch List Of SiteToSite Tunnel VPN Profile Names</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sitetosite/vpn/profilenames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol(orgUuid, tunnelProtocol, callback)</td>
    <td style="padding:15px">getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sitetosite/vpn/profilenames/{pathv1}/tunnelProtocol/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteToSiteTunnelVPNProfiles(orgUuid, callback)</td>
    <td style="padding:15px">Fetch List Of SiteToSite Tunnel VPN Profiles</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sitetosite/vpn/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteToSiteTunnelVPNProfiles(orgUuid, siteToSiteTunnelVPNProfileNames, callback)</td>
    <td style="padding:15px">Delete SiteToSite Tunnel VPN Profile</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sitetosite/vpn/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelSpackDownload(rtuVersion, updatetype, versionToDownload, callback)</td>
    <td style="padding:15px">Spack Download Cancel</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkAvailableSpackUpdates(updatetype, callback)</td>
    <td style="padding:15px">Spack Fetch Latest List</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/checkavailableupdates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkSpackUpdate(updatetype, callback)</td>
    <td style="padding:15px">Spack Fetch Latest</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/checkupdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSpackPackages(packageName, callback)</td>
    <td style="padding:15px">Spack Delete</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/deletepackages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadSpack(taskid, updatetype, versionToDownload, callback)</td>
    <td style="padding:15px">Spack Download</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultiPredefinedSpack(xPath, callback)</td>
    <td style="padding:15px">getMultiPredefined</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/multipredefined?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPredefinedSpack(xPath, callback)</td>
    <td style="padding:15px">Predefined</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/predefined?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateToSpackAppliance(device, flavour, packageName, taskid, updateVD, updatetype, version, callback)</td>
    <td style="padding:15px">Spack Install</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/updateAppliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadSpack(flavour, spackChecksumFile, spackFile, taskid, updatetype, callback)</td>
    <td style="padding:15px">upload</td>
    <td style="padding:15px">{base_path}/{version}/vnms/spack/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpackCount(includeCount, limit, offset, onlyCount, orderBy, callback)</td>
    <td style="padding:15px">Fetch Downloaded Spacks Count</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/spack/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpackLogs(includeCount, limit, offset, onlyCount, orderBy, callback)</td>
    <td style="padding:15px">Fetch All Downloaded Spacks Info</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/spack/downloads?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpokeGroups(limit, offset, org, callback)</td>
    <td style="padding:15px">Spoke Groups Fetch All</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/spokegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSpokeGroup(spokeGroupBean, callback)</td>
    <td style="padding:15px">Create  a Spoke group</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/spokegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpokeGroupById(spokeGroupName, callback)</td>
    <td style="padding:15px">Get a specific Spoke group  given its name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/spokegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSpokeGroup(spokeGroupBean, spokeGroupName, callback)</td>
    <td style="padding:15px">Update  a specific Spoke group</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/spokegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSpokeGroup(spokeGroupName, callback)</td>
    <td style="padding:15px">Delete a specific Spoke group  given its name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/spokegroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasks(applianceName, includeCount, limit, offset, onlyCount, orderBy, status, callback)</td>
    <td style="padding:15px">Fetch Tasks</td>
    <td style="padding:15px">{base_path}/{version}/vnms/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBulkTasks(id, callback)</td>
    <td style="padding:15px">Delete Multiple Tasks</td>
    <td style="padding:15px">{base_path}/{version}/vnms/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskSummary(includeCount, limit, offset, onlyCount, orderBy, callback)</td>
    <td style="padding:15px">Fetch Tasks Summary</td>
    <td style="padding:15px">{base_path}/{version}/vnms/tasks/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskById(id, callback)</td>
    <td style="padding:15px">Get task by id</td>
    <td style="padding:15px">{base_path}/{version}/vnms/tasks/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTask(id, callback)</td>
    <td style="padding:15px">Delete Task by id</td>
    <td style="padding:15px">{base_path}/{version}/vnms/tasks/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateReferences(deviceGroup, template, callback)</td>
    <td style="padding:15px">Given Device Group and POST Staging Template find all Template Association.</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/template/{pathv1}/associations/references/deviceGroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTemplateReferences(deviceGroup, template, callback)</td>
    <td style="padding:15px">Given Device Group and POST Staging Template find all Template Association.</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/template/{pathv1}/associations/sequence/deviceGroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBindData(data, callback)</td>
    <td style="padding:15px">Create TemplateBindData</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/binddata/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBinddataHeaderAndCount(deviceGroupName, templateName, callback)</td>
    <td style="padding:15px">Get template binddata header and count </td>
    <td style="padding:15px">{base_path}/{version}/nextgen/binddata/header/template/{pathv1}/devicegroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBinddataForTemplate(templateName, callback)</td>
    <td style="padding:15px">getBinddataForTemplate</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/binddata/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateBindata(allBinddata, device, deviceGroupName, limit, offset, searchKeyword, templateName, callback)</td>
    <td style="padding:15px">Get all Template BindData variables</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/binddata/templateData/template/{pathv1}/devicegroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveTemplateBindata(deviceGroupName, mode, templateName, templateVariable, callback)</td>
    <td style="padding:15px">Update Template BindData</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/binddata/templateData/template/{pathv1}/devicegroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateBindata(deviceGroupName, templateName, templateVariable, callback)</td>
    <td style="padding:15px">Delete All TemplateBindData for a template and device </td>
    <td style="padding:15px">{base_path}/{version}/nextgen/binddata/templateData/template/{pathv1}/devicegroup/{pathv2}/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBindDataById(id, callback)</td>
    <td style="padding:15px">Get template binddata  </td>
    <td style="padding:15px">{base_path}/{version}/nextgen/binddata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateCategories(callback)</td>
    <td style="padding:15px">Get All Template Categories</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/template/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateByCategory(category, callback)</td>
    <td style="padding:15px">Get All  Template Categories  by category </td>
    <td style="padding:15px">{base_path}/{version}/nextgen/template/categories/category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateCategoriesForOrg(category, organization, callback)</td>
    <td style="padding:15px">Get All  Template Categories filtered by org and category </td>
    <td style="padding:15px">{base_path}/{version}/nextgen/template/categories/category/{pathv1}/organization/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateShareCategories(organization, searchKey, callback)</td>
    <td style="padding:15px">Get All Shared Template Categories filtered by org and searchkey </td>
    <td style="padding:15px">{base_path}/{version}/nextgen/template/categories/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateMetadataById(light, templateName, callback)</td>
    <td style="padding:15px">Get Template Metadata Information associated with Template Name.</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/templates/template-metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveTemplateMetadata(model, templateName, callback)</td>
    <td style="padding:15px">Save Template Metadata Information associated with Template Name.</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/templates/template-metadata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllImages(limit, offset, callback)</td>
    <td style="padding:15px">Get UCPE Images</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadImageDetails(cpuCount, description, diskSize, fileName, fileType, isAuxiliaryInterface, isSecondaryDiskNeeded, memory, name, secondaryDiskSize, serviceFunctions, vendor, vendorProductType, version, callback)</td>
    <td style="padding:15px">Add metadata for an Image</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteImage(uuid, callback)</td>
    <td style="padding:15px">Delete UCPE Image</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateImage(fileType, page, uuid, callback)</td>
    <td style="padding:15px">Update UCPE Image with UUID</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/image?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageLogo(vendor, vendorProductType, callback)</td>
    <td style="padding:15px">Get Logo of a Product</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/logo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateImageLogoforDoc(page, vendor, vendorProductType, callback)</td>
    <td style="padding:15px">Update Logo of UCPE Vendor's Prodcut</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/logo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateImageMetadata(cpuCount, description, diskSize, isAuxiliaryInterface, isSecondaryDiskNeeded, memory, secondaryDiskSize, serviceFunctions, uuid, callback)</td>
    <td style="padding:15px">Update UCPE Image's Metadata</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageByName(name, callback)</td>
    <td style="padding:15px">Get UCPE Image by Name</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">urlImageUpload(imageDetails, callback)</td>
    <td style="padding:15px">Upload a UCPE Image from a HTTP url</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/urlUpload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageByUuid(uuid, callback)</td>
    <td style="padding:15px">Get UCPE Image by UUID</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/uuid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageByVendor(limit, offset, vendor, callback)</td>
    <td style="padding:15px">Get UCPE Images of a Specific Vendor</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageList(limit, offset, vendor, vendorProductType, callback)</td>
    <td style="padding:15px">Get UCPE Images of a Specific Vendor and Product</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/vendor/vendorproducttype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImageByVendorProductType(limit, offset, vendorProductType, callback)</td>
    <td style="padding:15px">Get UCPE Images of a Specific Vendor's Product</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/images/vendorproducttype?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCaptivePortalToAppliance(actiontype, appliance, orgUuid, pagename, callback)</td>
    <td style="padding:15px">Upload Captive Portal File To Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/captiveportal/custompages/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCaptivePortalFromAppliance(actiontype, appliance, orgUuid, pagename, callback)</td>
    <td style="padding:15px">Delete Captive Portal File From Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/captiveportal/custompages/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListOfPortalPages(orgUuid, callback)</td>
    <td style="padding:15px">Fetch List Of Captive Portal Files</td>
    <td style="padding:15px">{base_path}/{version}/vnms/captiveportal/custompages/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCaptivePortalToVD(file, orgUuid, callback)</td>
    <td style="padding:15px">Upload Captive Portal File To VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/captiveportal/custompages/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCaptivePortalFromVD(orgUuid, pagename, callback)</td>
    <td style="padding:15px">Delete Captive Portal File From VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/captiveportal/custompages/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPSFilterTypes(filterpath, orgUuid, callback)</td>
    <td style="padding:15px">Fetch custom ips filter types</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/filterTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadIPSVulnerabilityRuleFile(file, orgUuid, callback)</td>
    <td style="padding:15px">Upload IPS Rule File To VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/rulefiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListOfIPSUnzippedRuleFiles(orgUuid, callback)</td>
    <td style="padding:15px">Fetch List Of UnZipped IPS Rule Files</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/rulefiles/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadIPSCustomRuleFileToAppliance(appliance, filename, orgUuid, callback)</td>
    <td style="padding:15px">Upload IPS Rule File To Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/rulefiles/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplianceIPSRuleFile(appliance, filename, orgUuid, callback)</td>
    <td style="padding:15px">Delete IPS Rule From Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/rulefiles/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configureIPSVulnerabilityRuleFiles(appliance, disablefile, enablefile, orgUuid, callback)</td>
    <td style="padding:15px">Enable/Disable IPS Rule File</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/rulefiles/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVDIPSRuleFile(filename, orgUuid, callback)</td>
    <td style="padding:15px">Delete IPS Rule File From VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/rulefiles/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPSSignatures(condition, filters, limit, offset, orgUuid, callback)</td>
    <td style="padding:15px">Fetch custom ips signatures</td>
    <td style="padding:15px">{base_path}/{version}/vnms/ipscustomrule/signatures?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadKeyTabToAppliance(appliance, keytab, orgUuid, callback)</td>
    <td style="padding:15px">Upload KeyTab File To Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/keytab/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKeyTabFromAppliance(appliance, keytab, orgUuid, callback)</td>
    <td style="padding:15px">Delete Key Tab File From Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/keytab/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadKeyTabToVD(file, orgUuid, callback)</td>
    <td style="padding:15px">Upload KeyTab File To VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/keytab/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKeyTabFromVD(keytab, orgUuid, callback)</td>
    <td style="padding:15px">Delete KeyTab File From VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/keytab/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPACToAppliance(appliance, orgUuid, pac, callback)</td>
    <td style="padding:15px">Upload PAC File To Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/pac/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePACFromAppliance(appliance, orgUuid, pac, callback)</td>
    <td style="padding:15px">Delete PAC File From Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/pac/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadPACToVD(file, orgUuid, callback)</td>
    <td style="padding:15px">Upload PAC File To VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/pac/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePACFromVD(orgUuid, pac, callback)</td>
    <td style="padding:15px">Delete PAC File From VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/pac/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceSecurityFileInfo(appliance, orgUuid, type, callback)</td>
    <td style="padding:15px">Fetch Appliance Security File Info </td>
    <td style="padding:15px">{base_path}/{version}/vnms/upload/{pathv1}/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadSecuityFileToAppliance(appliance, file, name, orgUuid, passPhrase, privKey, type, callback)</td>
    <td style="padding:15px">Upload Security File To Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/upload/{pathv1}/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityFileFromAppliance(appliance, file, name, orgUuid, type, callback)</td>
    <td style="padding:15px">Delete Security File From Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/upload/{pathv1}/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVDSecurityFileInfo(orgUuid, type, callback)</td>
    <td style="padding:15px">Fetch VD Security File Info </td>
    <td style="padding:15px">{base_path}/{version}/vnms/upload/{pathv1}/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadSecurityFileToVD(file, name, orgUuid, type, callback)</td>
    <td style="padding:15px">Upload Security File To VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/upload/{pathv1}/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityFileFromVD(file, name, orgUuid, type, callback)</td>
    <td style="padding:15px">Delete Security File From VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/upload/{pathv1}/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCertificateToAppliance(appliance, certificateAuthorityChainName, filename, orgUuid, callback)</td>
    <td style="padding:15px">Upload Trusted Certificate File To Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/certificatemanager/certificates/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplianceCertificate(appliance, certificateAuthorityChainName, orgUuid, callback)</td>
    <td style="padding:15px">Delete Trusted Certificate File From Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/certificatemanager/certificates/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCertificateToVD(certificateAuthorityChainName, file, orgUuid, callback)</td>
    <td style="padding:15px">Upload Trusted Certificate File To VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/certificatemanager/certificates/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVDCertificate(certificateAuthorityChainName, filename, orgUuid, callback)</td>
    <td style="padding:15px">Delete Trusted Certificate File From VD</td>
    <td style="padding:15px">{base_path}/{version}/vnms/certificatemanager/certificates/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findActiveLoginUsers(callback)</td>
    <td style="padding:15px">Get all login users</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/activelogin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportAllUsers(callback)</td>
    <td style="padding:15px">Export all provider and tenant users</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findAllLockedUsers(callback)</td>
    <td style="padding:15px">Get all locked users</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/lockedusers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPasswordScore(password, callback)</td>
    <td style="padding:15px">Get password score</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/passwordscore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockUsersAccount(usernameList, callback)</td>
    <td style="padding:15px">Bulck Un-lock users</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/unlockusers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUserById(username, callback)</td>
    <td style="padding:15px">Fetch user details by user name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAccountLocked(username, callback)</td>
    <td style="padding:15px">Find user lock status</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/user/{pathv1}/auth/locked?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockUserAccount(username, callback)</td>
    <td style="padding:15px">Lock user by name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/user/{pathv1}/auth/lockuser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockUserAccount(username, callback)</td>
    <td style="padding:15px">Un-lock user by name</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/user/{pathv1}/auth/unlockuser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserGlobalSettings(callback)</td>
    <td style="padding:15px">Get global user settings</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/usersettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveUserGlobalSettings(userSettings, callback)</td>
    <td style="padding:15px">save global user settings</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/track/users/usersettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadCommonFile(type, uuid, callback)</td>
    <td style="padding:15px">download file by type like ucpeCustomData and uuid</td>
    <td style="padding:15px">{base_path}/{version}/vnms/common/download/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCommonFileToVD(file, model, type, callback)</td>
    <td style="padding:15px">Upload file to a given type like ucpeCustomData</td>
    <td style="padding:15px">{base_path}/{version}/vnms/common/upload/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommonFileMiniSummary(type, callback)</td>
    <td style="padding:15px">Fetch all Files by type with miniumn details</td>
    <td style="padding:15px">{base_path}/{version}/vnms/common/upload/{pathv1}/minsummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCommonFilesByType(type, callback)</td>
    <td style="padding:15px">Fetch all Files by type</td>
    <td style="padding:15px">{base_path}/{version}/vnms/common/upload/{pathv1}/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCommonFilesByMultipleTypes(type, uuid, callback)</td>
    <td style="padding:15px">Fetch FileUploadInfo by type and uuid</td>
    <td style="padding:15px">{base_path}/{version}/vnms/common/upload/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCommonFileMeta(fileMetaData, type, uuid, callback)</td>
    <td style="padding:15px">Update file metadata to a given type like ucpeCustomData and uuid</td>
    <td style="padding:15px">{base_path}/{version}/vnms/common/upload/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommonFile(type, uuid, callback)</td>
    <td style="padding:15px">delete file by type like ucpeCustomData and uuid</td>
    <td style="padding:15px">{base_path}/{version}/vnms/common/upload/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerClientByAdmin(clientRegnRequest, callback)</td>
    <td style="padding:15px">Create OAuth client</td>
    <td style="padding:15px">{base_path}/{version}/auth/admin/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClientByAdmin(clientRegnRequest, id, callback)</td>
    <td style="padding:15px">Update OAuth client</td>
    <td style="padding:15px">{base_path}/{version}/auth/admin/clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClientByClient(id, callback)</td>
    <td style="padding:15px">Get OAuth client</td>
    <td style="padding:15px">{base_path}/{version}/auth/clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClient(clientUpdateRequest, id, callback)</td>
    <td style="padding:15px">Update OAuth client</td>
    <td style="padding:15px">{base_path}/{version}/auth/clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClientByClient(id, callback)</td>
    <td style="padding:15px">Delete OAuth client</td>
    <td style="padding:15px">{base_path}/{version}/auth/clients/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshClientSecrect(id, callback)</td>
    <td style="padding:15px">Refresh OAuth client Secrect</td>
    <td style="padding:15px">{base_path}/{version}/auth/clients/{pathv1}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalOAuthTokenServer(email, isEnabled, org, role, userInfoEndPoint, callback)</td>
    <td style="padding:15px">External OAuth token server</td>
    <td style="padding:15px">{base_path}/{version}/auth/externalserver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshAccessToken(userRefreshTokenRequest, callback)</td>
    <td style="padding:15px">Refresh OAuth token</td>
    <td style="padding:15px">{base_path}/{version}/auth/refresh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeToken(callback)</td>
    <td style="padding:15px">Revoke OAuth token</td>
    <td style="padding:15px">{base_path}/{version}/auth/revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateUserEMAILSecureCode(username, callback)</td>
    <td style="padding:15px">Send secure code to email for a given user</td>
    <td style="padding:15px">{base_path}/{version}/vnms/user/authenticate/{pathv1}/securecode/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateUserSMSSecureCode(username, callback)</td>
    <td style="padding:15px">Send secure code to mobile for a given user</td>
    <td style="padding:15px">{base_path}/{version}/vnms/user/authenticate/{pathv1}/securecode/sms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyUserSecureCodeAndRegisterAsync(securecode, username, callback)</td>
    <td style="padding:15px">Verify secure code</td>
    <td style="padding:15px">{base_path}/{version}/vnms/user/authenticate/{pathv1}/verify/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVendors(callback)</td>
    <td style="padding:15px">Fetch all Vendor Details</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVendorProductType(vendorId, vendorProductType, callback)</td>
    <td style="padding:15px">Add a Product to a Vendor</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/product-type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVendorProductType(uuid, callback)</td>
    <td style="padding:15px">Delete the ProductType of User-Defined Vendor</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/productType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDefinedVendors(callback)</td>
    <td style="padding:15px">Get User-Defined Vendors and their Product Details</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/userdefined-vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVendor(vendor, callback)</td>
    <td style="padding:15px">Create a User-Defined Vendor</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/userdefined-vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchUserDefinedVendor(searchStr, callback)</td>
    <td style="padding:15px">Search for a user-defined vendor</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/userdefined-vendor/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendors(callback)</td>
    <td style="padding:15px">Get Pre-defined Vendors and their Product Details</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVendor(vendorId, callback)</td>
    <td style="padding:15px">Delete the User-Defined Vendor</td>
    <td style="padding:15px">{base_path}/{version}/vnms/catalog/vendors/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCMSConnectorList(deep, callback)</td>
    <td style="padding:15px">This will get the list of all CMS connectors available</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/cmsconnectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnalyticsClusterList(select, callback)</td>
    <td style="padding:15px">Query the list of Analytics Cluster that has been configured in the Versa Director</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/analytics-cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwarePackageVersion(callback)</td>
    <td style="padding:15px">Get the details of the Versa Director software package</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/system/package-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailableOrganizationIds(callback)</td>
    <td style="padding:15px">Get Available (New) Org-ID for a New Org</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/Org/availableId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateMetadata(organization, type, offset, limit, callback)</td>
    <td style="padding:15px">List of Service Templates for an Org</td>
    <td style="padding:15px">{base_path}/{version}/vnms/template/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addServiceTemplate(body, callback)</td>
    <td style="padding:15px">Add Service Template</td>
    <td style="padding:15px">{base_path}/{version}/vnms/template/serviceTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportServiceTemplate(template, callback)</td>
    <td style="padding:15px">Export a Service Template</td>
    <td style="padding:15px">{base_path}/{version}/versa/templates/template/export/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneMasterTemplate(body, callback)</td>
    <td style="padding:15px">Clone the Master Template</td>
    <td style="padding:15px">{base_path}/{version}/vnms/template/cloneTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplateOrgLANZone(newOrgName, body, callback)</td>
    <td style="padding:15px">Create LAN Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}-DataStore/config/orgs/org-services/{pathv2}/objects/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importaServiceTemplate(body, callback)</td>
    <td style="padding:15px">Import a Service Template</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExistingSpokeGroups(offset, limit, callback)</td>
    <td style="padding:15px">Get Existing Spoke Groups</td>
    <td style="padding:15px">{base_path}/{version}/spokegroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceList(offset, limit, callback)</td>
    <td style="padding:15px">Get Appliance List - Extra API for reference</td>
    <td style="padding:15px">{base_path}/{version}/vnms/cloud/systems/getAllAppliancesBasicDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANAvailableIdWithSerial(callback)</td>
    <td style="padding:15px">Get Available DeviceID, SerialNum</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/Branch/availableId/withSerialNumber?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSDWANAvailableVRFIds(count, callback)</td>
    <td style="padding:15px">Get Available (New) VRF-ID for LAN-VR</td>
    <td style="padding:15px">{base_path}/{version}/vnms/sdwan/global/availableIds/VRF?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatitudeLongitudeofAddress(channel, key, address, body, callback)</td>
    <td style="padding:15px">Get Latitude Longitude of Address</td>
    <td style="padding:15px">{base_path}/{version}/json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrgApplianceUUID(callback)</td>
    <td style="padding:15px">Get Org UUID</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/organizations/organization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationWANNetworks(newOrgUuid, callback)</td>
    <td style="padding:15px">Get the available WAN Networks</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/organizations/organization/{pathv1}/wan-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrganizationWANNetwork(newOrgUuid, newNwName, body, callback)</td>
    <td style="padding:15px">Create a WAN Network</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/organizations/organization/{pathv1}/wan-networks/wan-network/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(body, callback)</td>
    <td style="padding:15px">Create New LAN VR during Org Creation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBulkCommunityGroup(callback)</td>
    <td style="padding:15px">Get Bulk Community Group</td>
    <td style="padding:15px">{base_path}/{version}/nextgen/community/bulkCommunityCreate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSolutionTieronsetofDevices(body, callback)</td>
    <td style="padding:15px">Update Solution Tier on set of Devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/actions/_operations/perform-subscription-action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterface(device, callback)</td>
    <td style="padding:15px">Get Interface</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/config/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceDetails(device, callback)</td>
    <td style="padding:15px">Get Interface Copy</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/interfaces/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPSecTunnelInterfaces(device, callback)</td>
    <td style="padding:15px">Get IPSec Tunnel Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/config/interfaces/ipsec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLogicalTunnelInterfaces(device, callback)</td>
    <td style="padding:15px">Get Logical Tunnel Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/interfaces/lt?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhysicalInterfaces(device, callback)</td>
    <td style="padding:15px">Get Physical Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/config/interfaces/vni?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersaTunnelVirtualInterfaces(device, callback)</td>
    <td style="padding:15px">Get Versa Tunnel Virtual Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/config/interfaces/tvi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersaPseudoTunnelVirtualInterfaces(device, callback)</td>
    <td style="padding:15px">Get Versa Pseudo Tunnel Virtual Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/config/interfaces/ptvi?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersaWlanInterface(device, callback)</td>
    <td style="padding:15px">Get Versa wlan Interface</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/config/interfaces/wlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersaWwanInterface(device, callback)</td>
    <td style="padding:15px">Get Versa wwan Interface</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/config/interfaces/wwan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPInformationforActiveInterfaces(device, callback)</td>
    <td style="padding:15px">Get IP Information for active interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/interfaces/ip/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPInformationforDynamicInterfaces(device, callback)</td>
    <td style="padding:15px">Get IP Information for Dynamic Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/interfaces/dynamic-tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficonPhysicalInterfaces(device, orgid, callback)</td>
    <td style="padding:15px">Get Traffic on Physical Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/interfaces/info/{pathv2}/org_intf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficonAllInterfaces(device, callback)</td>
    <td style="padding:15px">Get Traffic on All Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/interfaces/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficonSpecificInterfaces(device, interfaceParam, callback)</td>
    <td style="padding:15px">Get Traffic on Specific Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/interfaces/statistics/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAmetricsforAppliance(device, uuid, command, callback)</td>
    <td style="padding:15px">Get SLA Metrics for an Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/appliance/{pathv1}/live?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAmetricsforApplianceFilter(device, uuid, command, format, filters, callback)</td>
    <td style="padding:15px">Get SLA Metrics for an Appliance</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/appliance/{pathv1}/live?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSDWANPoliciesofanAppliance(device, orgid, poligyGroup, callback)</td>
    <td style="padding:15px">Get All SD-WAN Policies of an Appliance</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/config/orgs/org-services/{pathv2}/sd-wan/policies/sdwan-policy-group/{pathv3}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAvailableLicenseTiers(callback)</td>
    <td style="padding:15px">Get All Available Solution Tiers</td>
    <td style="padding:15px">{base_path}/{version}/vnms/license-key/all-available-tiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationDHCPOptionsProfile(organization, template, callback)</td>
    <td style="padding:15px">get DHCP Options Profies within the specified organization</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-options-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationZones(organization, template, callback)</td>
    <td style="padding:15px">get Zones within the specified organization</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/objects/zones/zone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePhysicalInterfaces(templateName, vniInterfaceName, callback)</td>
    <td style="padding:15px">delete Physical Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/interfaces/vni/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateCgnatPool(templateName, orgname, unhide, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Cgnat Pool</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/cgnat/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateNetworks(templateName, networkName, callback)</td>
    <td style="padding:15px">delete Post Staging Template Networks</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/networks/network/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateCgnatPools(templateName, orgname, cgnatPool, callback)</td>
    <td style="padding:15px">delete Post Staging Template Cgnat Pools</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/cgnat/pools/pool/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateCgnatRule(templateName, orgname, unhide, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Cgnat Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/cgnat/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateCgnatRules(templateName, orgname, cgnatRule, callback)</td>
    <td style="padding:15px">delete Post Staging Template Cgnat Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/cgnat/rules/rule/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClassOfServiceInterfaceNetworkAssociation(templateName, orgname, interfaceName, callback)</td>
    <td style="padding:15px">delete Class Of Service Interface Network Association</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/class-of-service/interfaces/interface/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateDhcpAddressPool(templateName, orgname, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Dhcp Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-dynamic-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcpAddressPools(templateName, orgname, dhcpPoolName, callback)</td>
    <td style="padding:15px">delete Post Staging Template Dhcp Address Pools</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-dynamic-pools/dhcp4-dynamic-pool/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateDhcpLeaseProfile(templateName, orgname, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Dhcp Lease Profile</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-lease-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcpLeaseProfiles(templateName, orgname, dhcpLeaseProfileName, callback)</td>
    <td style="padding:15px">delete Post Staging Template Dhcp Lease Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-lease-profiles/dhcp4-lease-profile/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateDhcpOptionsProfile(templateName, orgname, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Dhcp Options Profile</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-options-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcpOptionsProfiles(templateName, orgname, dhcpOptionProfileName, callback)</td>
    <td style="padding:15px">delete Post Staging Template Dhcp Options Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-options-profiles/dhcp4-options-profile/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateDhcpServer(templateName, orgname, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Dhcp Server</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-server-and-relay/service-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcpServers(templateName, orgname, dhcpServerProfile, callback)</td>
    <td style="padding:15px">delete Post Staging Template Dhcp Servers</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp4-server-and-relay/service-profiles/dhcp4-service-profile/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateZone(templateName, orgname, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/objects/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateOrgLANZone(templateName, orgname, zoneName, callback)</td>
    <td style="padding:15px">delete Template Org LAN Zone</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/objects/zones/zone/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateStatefulFirewallAccessPolicyRule(templateName, orgname, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Stateful Firewall Access Policy Rule</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/security/access-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateStatefulFirewallSecurityRules(templateName, orgname, securityAccessPolicy, callback)</td>
    <td style="padding:15px">delete Post Staging Template Stateful Firewall Security Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/security/access-policies/access-policy-group/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateStatefulFirewallSecurityRules(templateName, orgname, securityAccessPolicy, body, callback)</td>
    <td style="padding:15px">add Post Staging Template Stateful Firewall Security Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/security/access-policies/access-policy-group/{pathv3}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateStatefulFirewallAccessPolicyRules(templateName, orgname, securityAccessPolicy, securityAccessPolicyRule, callback)</td>
    <td style="padding:15px">delete Post Staging Template Stateful Firewall Access Policy Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/security/access-policies/access-policy-group/{pathv3}/rules/access-policy/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateVrfs(templateName, callback)</td>
    <td style="padding:15px">get Post Staging Template Vrfs</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/routing-instances/routing-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateVrf(templateName, routingInstanceName, body, callback)</td>
    <td style="padding:15px">update Post Staging Template Vrf</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/routing-instances/routing-instance/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importServiceTemplate(templateName, bodyFormData, callback)</td>
    <td style="padding:15px">import Service Template</td>
    <td style="padding:15px">{base_path}/{version}/vnms/template/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importTemplateString(templateName, body, callback)</td>
    <td style="padding:15px">import template string</td>
    <td style="padding:15px">{base_path}/{version}/vnms/template/importstr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePhysicalInterfaces(templateName, vni, body, callback)</td>
    <td style="padding:15px">update physical interface</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/interfaces/vni/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateNetworkInterface(templateName, callback)</td>
    <td style="padding:15px">get post staging template network interface</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateNetworkInterface(templateName, body, callback)</td>
    <td style="padding:15px">create post staging template network interface</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateCgnatRules(templateName, organization, query, callback)</td>
    <td style="padding:15px">get post staging template CGNAT rules</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/cgnat/rules/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateCgnatRules(templateName, organization, rule, unhide, body, callback)</td>
    <td style="padding:15px">update post staging template CGNAT rules</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/cgnat/rules/rule/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateDhcp6LeaseProfile(templateName, organization, callback)</td>
    <td style="padding:15px">get post staging template DHCP6 Lease Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-lease-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPostStagingTemplateDhcp6LeaseProfile(templateName, organization, body, callback)</td>
    <td style="padding:15px">create post staging template DHCP6 Lease Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-lease-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateDhcp6LeaseProfile(templateName, organization, leaseProfile, body, callback)</td>
    <td style="padding:15px">update post staging template DHCP6 Lease Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-lease-profiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcp6LeaseProfile(templateName, organization, leaseProfile, body, callback)</td>
    <td style="padding:15px">delete post staging template DHCP6 Lease Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-lease-profiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateDhcp6OptionsProfile(templateName, organization, callback)</td>
    <td style="padding:15px">get post staging template DHCP6 Options Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-options-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPostStagingTemplateDhcp6OptionsProfile(templateName, organization, body, callback)</td>
    <td style="padding:15px">create post staging template DHCP6 Options Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-options-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateDhcp6OptionsProfile(templateName, organization, optionsProfile, body, callback)</td>
    <td style="padding:15px">update post staging template DHCP6 Options Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-options-profiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcp6OptionsProfile(templateName, organization, optionsProfile, body, callback)</td>
    <td style="padding:15px">delete post staging template DHCP6 Options Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-options-profiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateDhcp6AddressPool(templateName, organization, callback)</td>
    <td style="padding:15px">get post staging template DHCP6 Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-dynamic-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPostStagingTemplateDhcp6AddressPool(templateName, organization, body, callback)</td>
    <td style="padding:15px">create post staging template DHCP6 Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-dynamic-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateDhcp6AddressPool(templateName, organization, dynamicPool, body, callback)</td>
    <td style="padding:15px">update post staging template DHCP6 Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-dynamic-pools/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcp6AddressPool(templateName, organization, dynamicPool, body, callback)</td>
    <td style="padding:15px">delete post staging template DHCP6 Address Pool</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-dynamic-pools/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateDhcp6Server(templateName, organization, callback)</td>
    <td style="padding:15px">get post staging template DHCP6 Server</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-server-and-relay/ipv6-service-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPostStagingTemplateDhcp6Server(templateName, organization, body, callback)</td>
    <td style="padding:15px">create post staging template DHCP6 Server</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-server-and-relay/ipv6-service-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateDhcp6Server(templateName, organization, serviceProfile, body, callback)</td>
    <td style="padding:15px">update post staging template DHCP6 Server</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-server-and-relay/ipv6-service-profiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePostStagingTemplateDhcp6Server(templateName, organization, serviceProfile, body, callback)</td>
    <td style="padding:15px">delete post staging template DHCP6 Server</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/dhcp/dhcp6-server-and-relay/ipv6-service-profiles/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateServiceNodeGroup(templateName, serviceNodeGroup, body, callback)</td>
    <td style="padding:15px">update post staging template service node group</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/service-node-groups/service-node-group/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPostStagingTemplateNetwork(templateName, body, callback)</td>
    <td style="padding:15px">create post staging template network</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateOrgLimits(templateName, organization, callback)</td>
    <td style="padding:15px">get post staging template org limits</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateOrgLimits(templateName, organization, body, callback)</td>
    <td style="padding:15px">update post staging template org limits</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPostStagingTemplateIpsecVpnProfile(templateName, organization, callback)</td>
    <td style="padding:15px">get post staging template ipsec vpn profile</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/ipsec/vpn-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePostStagingTemplateIpsecVpnProfile(templateName, organization, vpnProfile, unhide, body, callback)</td>
    <td style="padding:15px">update post staging template ipsec vpn profile</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/ipsec/vpn-profile/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplianceFileObject(body, callback)</td>
    <td style="padding:15px">Create file object</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/config-save/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplianceFileObject(fileName, callback)</td>
    <td style="padding:15px">Delete file object</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/config-save/files/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editApplianceFileObject(fileName, body, callback)</td>
    <td style="padding:15px">Edit file object</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/config-save/files/file/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editAppliaceConfiguration(body, callback)</td>
    <td style="padding:15px">Edit configuration for enable/disable appliance subjugation</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliaceConfiguration(callback)</td>
    <td style="padding:15px">Get configuration for enable/disable appliance subjugation</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/appliance-subjugate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceFileObject(callback)</td>
    <td style="padding:15px">Get list of file objects</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/provider/config-save/files/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceById(applianceUuid, callback)</td>
    <td style="padding:15px">get Appliance By Uuid</td>
    <td style="padding:15px">{base_path}/{version}/vnms/cloud/systems/getApplianceByUUID/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceNamesAndUuids(callback)</td>
    <td style="padding:15px">get Appliance Name and Uuid</td>
    <td style="padding:15px">{base_path}/{version}/vnms/cloud/systems/getAllApplianceNames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouteObject(body, callback)</td>
    <td style="padding:15px">Create route object</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/routing-options/static?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouteObject(destinationPrefix, nextHopAddress, outgoingInterface, callback)</td>
    <td style="padding:15px">Delete route object</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/routing-options/static/route/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editRouteObject(destinationPrefix, nextHopAddress, outgoingInterface, body, callback)</td>
    <td style="padding:15px">Edit route object</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/routing-options/static/route/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouteObjectList(callback)</td>
    <td style="padding:15px">Get list of route objects</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/routing-options/static/route?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseAlarmsSummary(callback)</td>
    <td style="padding:15px">Get summary of director license alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/director/pop-up/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkAlarms(body, callback)</td>
    <td style="padding:15px">Create bulk Alarms</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBulkAlarmByUser(body, callback)</td>
    <td style="padding:15px">Create bulk alarm by user</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms/handle?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmsByAppliance(deviceName, callback)</td>
    <td style="padding:15px">Request Alarm Summary per Appliance</td>
    <td style="padding:15px">{base_path}/{version}/alarms/summary/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmsSummary(callback)</td>
    <td style="padding:15px">Request Alarms Summary</td>
    <td style="padding:15px">{base_path}/{version}/vnms/alarms/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateBulkAlarm(body, callback)</td>
    <td style="padding:15px">Update Alarms information</td>
    <td style="padding:15px">{base_path}/{version}/vnms/fault/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplateDevices(body, callback)</td>
    <td style="padding:15px">Create Template devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplateAttributes(device, template, body, callback)</td>
    <td style="padding:15px">Create Template attributes</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/device-template-variable/{pathv1}/{pathv2}/variable-binding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTemplateTunnel(managedDevice, body, callback)</td>
    <td style="padding:15px">Create Template tunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateDeviceGroups(groupName, callback)</td>
    <td style="padding:15px">Delete groups of devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTemplateDeviceGroups(groupName, body, callback)</td>
    <td style="padding:15px">Edit groups of devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateDeviceTemplates(templateName, callback)</td>
    <td style="padding:15px">Delete named templates for devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTemplateDeviceTemplates(templateName, body, callback)</td>
    <td style="padding:15px">Edit named templates for devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateManagedDevices(managedDevice, callback)</td>
    <td style="padding:15px">Delete The list of managed devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTemplateManagedDevices(managedDevice, body, callback)</td>
    <td style="padding:15px">Edit The list of managed devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateAttributes(device, template, attribute, callback)</td>
    <td style="padding:15px">Delete Template Attributes</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/device-template-variable/{pathv1}/{pathv2}/variable-binding/attrs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTemplateAttributes(device, template, attribute, body, callback)</td>
    <td style="padding:15px">Edit Template Attributes</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/device-template-variable/{pathv1}/{pathv2}/variable-binding/attrs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateTunnel(managedDevice, tunnel, callback)</td>
    <td style="padding:15px">Delete Template Tunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/tunnels/tunnel/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTemplateTunnel(managedDevice, tunnel, body, callback)</td>
    <td style="padding:15px">Edit Template Tunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/tunnels/tunnel/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateDeviceGroups(callback)</td>
    <td style="padding:15px">Get Groups of devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateDeviceTemplates(callback)</td>
    <td style="padding:15px">Get Named configuration templates for devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateManagedDevices(callback)</td>
    <td style="padding:15px">Get The list of managed devices</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateAttributes(device, template, callback)</td>
    <td style="padding:15px">Get Template Attributes</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/device-template-variable/{pathv1}/{pathv2}/variable-binding/attrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateTunnel(managedDevice, callback)</td>
    <td style="padding:15px">Get Template Tunnel</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/tunnels/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editClassOfService(template, organization, network, body, callback)</td>
    <td style="padding:15px">Edit Template Class of Service</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/class-of-service/networks/network/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyTemplate(template, reboot, mode, body, callback)</td>
    <td style="padding:15px">Apply Template to devices</td>
    <td style="padding:15px">{base_path}/{version}/vnms/template/applyTemplate/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoringDatabase(body, callback)</td>
    <td style="padding:15px">Monitoring Database configuration is updated</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/polling/shutdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMonitoringStatus(enabled, body, callback)</td>
    <td style="padding:15px">Enable/disable monitoring</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/enableMonitoring/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startMonitorPolling(body, callback)</td>
    <td style="padding:15px">Enables periodic monitor polling</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/polling/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshMonitorData(body, callback)</td>
    <td style="padding:15px">Force refresh all cached monitor data</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/refreshAll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringCacheApplianceDataForTenant(tenantName, applianceUUID, callback)</td>
    <td style="padding:15px">Force refresh cached appliance status data for tenant</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/refresh/appliance/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringCacheApplianceData(applianceUUID, callback)</td>
    <td style="padding:15px">Force refresh cached appliance status data</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/refresh/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitoringCacheDataForTenant(tenantName, callback)</td>
    <td style="padding:15px">Force refresh cached appliance status data</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/refresh/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateFromOrganization(organization, callback)</td>
    <td style="padding:15px">get the templates for an organization</td>
    <td style="padding:15px">{base_path}/{version}/vnms/template/sdwan/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editTemplateSecurityPolicy(template, organization, policygroup, policy, body, callback)</td>
    <td style="padding:15px">Edit Template Security Policy</td>
    <td style="padding:15px">{base_path}/{version}api/config/devices/template/{pathv1}/config/orgs/org-services/{pathv2}/security/access-policies/access-policy-group/{pathv3}/rules/access-policy/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransportDomains(deep, callback)</td>
    <td style="padding:15px">Get Transport Domains</td>
    <td style="padding:15px">{base_path}/{version}/api/config/nms/sdwan/transport-domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardApplianceHardware(uuid, callback)</td>
    <td style="padding:15px">Get Dashboard Appliance Hardware</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/appliance/{pathv1}/hardware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancePageableRoutes(applianceName, uuid, orgName, routeInstance, afi, limit, callback)</td>
    <td style="padding:15px">Get Appliance Pageable Routes</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/appliance/{pathv1}/pageable_routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancePageableArp(applianceName, uuid, routeInstance, limit, callback)</td>
    <td style="padding:15px">Get Appliance Pageable Arp</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/appliance/{pathv1}/pageable_arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancePageableArpOrgName(applianceName, uuid, routeInstance, limit, orgName, callback)</td>
    <td style="padding:15px">Get Appliance Pageable Arp</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/appliance/{pathv1}/pageable_arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appliancePing(applianceName, body, callback)</td>
    <td style="padding:15px">Appliance Ping</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/live-status/diagnostics/ping/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOwnedRoutingInstances(applianceName, orgName, callback)</td>
    <td style="padding:15px">Get Owned Routing Instances</td>
    <td style="padding:15px">{base_path}/{version}/api/operational/devices/device/{pathv1}/config/orgs/org/{pathv2}/owned-routing-instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPageableInterfaces(applianceName, uuid, orgName, limit, offset, callback)</td>
    <td style="padding:15px">Get Pageable Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/vnms/dashboard/appliance/{pathv1}/pageable_interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceVni(applianceName, callback)</td>
    <td style="padding:15px">Get Pageable Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/config/interfaces/vni?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoutingInstancesRoutingInstance(applianceName, callback)</td>
    <td style="padding:15px">Get Routing Instances for Appliance</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/config/routing-instances/routing-instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(applianceName, callback)</td>
    <td style="padding:15px">Get Networks</td>
    <td style="padding:15px">{base_path}/{version}/api/config/devices/device/{pathv1}/config/networks/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
