# Versa Networks Director

Vendor: Versa Networks
Homepage: https://versa-networks.com/

Product: Versa Director
Product Page: https://versa-networks.com/products/components/

## Introduction
We classify Versa Networks Director into the Data Center domain as Versa Networks Director enables integration with various components and services within the data center infrastructure. We also classify Versa Networks Director into the Network Services domain as it manages and orchestrates network-related functionalities.

"Versa Director simplifies the creation, automation and delivery of services." 
"Versa Director provides the essential management, monitoring and orchestration capabilities needed to deliver Versa’s Secure Cloud IP architecture network and security software services."
"Services supported by Versa include routing and advanced connectivity to direct traffic across multilink WAN access and simplify control for SD-WAN with embedded advanced security."

The Versa Networks Director adapter can be integrated to the Itential Device Broker which will allow your Versa appliances to be managed within the Itential Configuration Manager Application.

## Why Integrate
The Versa Networks Director adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Versa Networks Director. With this adapter you have the ability to perform operations such as:

- Create, Modify, Manage and Delete Versa Appliances. 
- Organization
- Region
- Template
- Policy
- Network

## Additional Product Documentation
The [API documents for Versa Networks Director](https://docs.centreon.com/pp/integrations/plugin-packs/procedures/network-versa-director-restapi/#:~:text=Versa%20Director%20provides%20the%20management,equipments%20managed%20by%20the%20Director.)

[Versa Networks Director datasheet](chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/https://versa-networks.com/documents/datasheets/versa-director.pdf)