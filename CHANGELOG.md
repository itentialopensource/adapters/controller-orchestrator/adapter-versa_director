
## 0.12.4 [10-15-2024]

* Changes made at 2024.10.14_20:45PM

See merge request itentialopensource/adapters/adapter-versa_director!32

---

## 0.12.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-versa_director!30

---

## 0.12.2 [08-14-2024]

* Changes made at 2024.08.14_19:04PM

See merge request itentialopensource/adapters/adapter-versa_director!29

---

## 0.12.1 [08-08-2024]

* Remediation

See merge request itentialopensource/adapters/adapter-versa_director!28

---

## 0.12.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!27

---

## 0.11.6 [04-05-2024]

* Update metadata.json

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!26

---

## 0.11.5 [03-28-2024]

* Changes made at 2024.03.28_13:43PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!25

---

## 0.11.4 [03-18-2024]

* Fix field name

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!24

---

## 0.11.3 [03-11-2024]

* Changes made at 2024.03.11_15:59PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!23

---

## 0.11.2 [02-28-2024]

* Changes made at 2024.02.28_11:29AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!22

---

## 0.11.1 [02-20-2024]

* updates for broker calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!21

---

## 0.11.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!20

---

## 0.10.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!20

---

## 0.9.3 [09-12-2023]

* more migration & metadata changes

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!19

---

## 0.9.2 [08-16-2023]

* fix typo in metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!18

---

## 0.9.1 [08-11-2023]

* fix typo in metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!18

---

## 0.9.0 [08-11-2023]

* Minor/2023 migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!17

---

## 0.8.4 [08-01-2023]

* Update adapter base and import order

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!15

---

## 0.8.3 [05-09-2023]

* Patch/migrate adapter base

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!14

---

## 0.8.2 [03-03-2023]

* Updated mock data files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!13

---

## 0.8.1 [02-15-2023]

* Update healthcheck entitypath and schemaTokenResp

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!11

---

## 0.8.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!10

---

## 0.7.0 [12-31-2021]

- Add Device Broker integration to the Versa Adapter - this adds calls that allows for Versa Appliances to be viewed in Config Manager. There is one original adapter call getDevice that had to be renamed to getVersaDevice because the getDevice call is a broker call. So this call will break if it was being used. Everything else is non breaking.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!9

---

## 0.6.0 [10-28-2021]

- Add new calls

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!8

---

## 0.5.0 [06-04-2021]

- Added 3 calls and added a forth with changed params

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!7

---

## 0.4.1 [03-23-2021]

- Added a new call that is needed and also fix some issues where additional curly braces were being added to the body objects.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!6

---

## 0.4.0 [03-19-2021]

- Added the request new calls to the adapter - 4 added the other 2 already existed.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!5

---

## 0.3.2 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!4

---

## 0.3.1 [02-19-2021]

- Make changes to importTemplateString

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!3

---

## 0.3.0 [02-18-2021]

- Added 2 calls including getTemplateFromOrganization

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!2

---

## 0.2.0 [02-17-2021]

- Add more calls and merge in the calls from the original adapter-versa_director

See merge request itentialopensource/adapters/controller-orchestrator/adapter-versa_director!1

---

## 0.1.1 [10-20-2020]

- Initial Commit

See commit 96fa9d2

---
