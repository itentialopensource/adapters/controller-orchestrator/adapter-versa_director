/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-versa_director',
      type: 'VersaDirector',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const VersaDirector = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Versa_director Adapter Test', () => {
  describe('VersaDirector Class Tests', () => {
    const a = new VersaDirector(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('versa_director'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('versa_director'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('VersaDirector', pronghornDotJson.export);
          assert.equal('Versa_director', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-versa_director', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('versa_director'));
          assert.equal('VersaDirector', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-versa_director', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-versa_director', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#assignAlarmObject - errors', () => {
      it('should have a assignAlarmObject function', (done) => {
        try {
          assert.equal(true, typeof a.assignAlarmObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assignee', (done) => {
        try {
          a.assignAlarmObject('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'assignee is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-assignAlarmObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing description', (done) => {
        try {
          a.assignAlarmObject('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'description is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-assignAlarmObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.assignAlarmObject('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-assignAlarmObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlarmAssignee - errors', () => {
      it('should have a updateAlarmAssignee function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlarmAssignee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assignee', (done) => {
        try {
          a.updateAlarmAssignee(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'assignee is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAlarmAssignee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing description', (done) => {
        try {
          a.updateAlarmAssignee('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'description is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAlarmAssignee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.updateAlarmAssignee('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAlarmAssignee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedObject', (done) => {
        try {
          a.updateAlarmAssignee('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'managedObject is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAlarmAssignee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.updateAlarmAssignee('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAlarmAssignee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.updateAlarmAssignee('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAlarmAssignee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.updateAlarmAssignee('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAlarmAssignee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearAlarm - errors', () => {
      it('should have a clearAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.clearAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.clearAlarm(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-clearAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedObject', (done) => {
        try {
          a.clearAlarm('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'managedObject is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-clearAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.clearAlarm('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-clearAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.clearAlarm('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-clearAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#handleAlarmObject - errors', () => {
      it('should have a handleAlarmObject function', (done) => {
        try {
          assert.equal(true, typeof a.handleAlarmObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHandleAlarm - errors', () => {
      it('should have a updateHandleAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.updateHandleAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing description', (done) => {
        try {
          a.updateHandleAlarm(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'description is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateHandleAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.updateHandleAlarm('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateHandleAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedObject', (done) => {
        try {
          a.updateHandleAlarm('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'managedObject is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateHandleAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.updateHandleAlarm('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateHandleAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing specificProblem', (done) => {
        try {
          a.updateHandleAlarm('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'specificProblem is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateHandleAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.updateHandleAlarm('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateHandleAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.updateHandleAlarm('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateHandleAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmHandling - errors', () => {
      it('should have a getAlarmHandling function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmHandling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getAlarmHandling(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAlarmHandling', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedObject', (done) => {
        try {
          a.getAlarmHandling('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'managedObject is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAlarmHandling', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getAlarmHandling('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAlarmHandling', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAlarmHandling('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAlarmHandling', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmHandlingObject - errors', () => {
      it('should have a getAlarmHandlingObject function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmHandlingObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgeAlarmObject - errors', () => {
      it('should have a purgeAlarmObject function', (done) => {
        try {
          assert.equal(true, typeof a.purgeAlarmObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgeAlarm - errors', () => {
      it('should have a purgeAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.purgeAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusChange - errors', () => {
      it('should have a getStatusChange function', (done) => {
        try {
          assert.equal(true, typeof a.getStatusChange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getStatusChange(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getStatusChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedObject', (done) => {
        try {
          a.getStatusChange('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'managedObject is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getStatusChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getStatusChange('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getStatusChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getStatusChange('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getStatusChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusChangeObject - errors', () => {
      it('should have a getStatusChangeObject function', (done) => {
        try {
          assert.equal(true, typeof a.getStatusChangeObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFilteredAlarms - errors', () => {
      it('should have a getAllFilteredAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getAllFilteredAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignAllFilteredAlarms - errors', () => {
      it('should have a assignAllFilteredAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.assignAllFilteredAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assignee', (done) => {
        try {
          a.assignAllFilteredAlarms(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'assignee is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-assignAllFilteredAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing description', (done) => {
        try {
          a.assignAllFilteredAlarms('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'description is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-assignAllFilteredAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.assignAllFilteredAlarms('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-assignAllFilteredAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignAllAlarms - errors', () => {
      it('should have a assignAllAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.assignAllAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmData', (done) => {
        try {
          a.assignAllAlarms(null, (data, error) => {
            try {
              const displayE = 'alarmData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-assignAllAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearAllAlarms - errors', () => {
      it('should have a clearAllAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.clearAllAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmData', (done) => {
        try {
          a.clearAllAlarms(null, (data, error) => {
            try {
              const displayE = 'alarmData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-clearAllAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgeAllAlarms - errors', () => {
      it('should have a purgeAllAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.purgeAllAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmData', (done) => {
        try {
          a.purgeAllAlarms(null, (data, error) => {
            try {
              const displayE = 'alarmData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-purgeAllAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#handleAllAlarms - errors', () => {
      it('should have a handleAllAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.handleAllAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmData', (done) => {
        try {
          a.handleAllAlarms(null, (data, error) => {
            try {
              const displayE = 'alarmData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-handleAllAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#handleAllFilteredAlarms - errors', () => {
      it('should have a handleAllFilteredAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.handleAllFilteredAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing description', (done) => {
        try {
          a.handleAllFilteredAlarms(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'description is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-handleAllFilteredAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.handleAllFilteredAlarms('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-handleAllFilteredAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterPaginateAlarm - errors', () => {
      it('should have a filterPaginateAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.filterPaginateAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgeAllFilteredAlarms - errors', () => {
      it('should have a purgeAllFilteredAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.purgeAllFilteredAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmSummary - errors', () => {
      it('should have a getAlarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAlarmSummary - errors', () => {
      it('should have a getDeviceAlarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceAlarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getDeviceAlarmSummary(null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceAlarmSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmSummaryPerOrg - errors', () => {
      it('should have a getAlarmSummaryPerOrg function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmSummaryPerOrg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getAlarmSummaryPerOrg('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAlarmSummaryPerOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorAlarms - errors', () => {
      it('should have a getDirectorAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectorAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorAlarmSummary - errors', () => {
      it('should have a getDirectorAlarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectorAlarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorFailOverAlarms - errors', () => {
      it('should have a getDirectorFailOverAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectorFailOverAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorHAAlarms - errors', () => {
      it('should have a getDirectorHAAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectorHAAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImpAlarms - errors', () => {
      it('should have a getImpAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.getImpAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImpAlarmSummary - errors', () => {
      it('should have a getImpAlarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getImpAlarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmTypes - errors', () => {
      it('should have a getAlarmTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmNotifications - errors', () => {
      it('should have a getAlarmNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveAlarmNotification - errors', () => {
      it('should have a saveAlarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.saveAlarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAlarmNotification - errors', () => {
      it('should have a editAlarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.editAlarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarmNotification - errors', () => {
      it('should have a deleteAlarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAlarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupkey', (done) => {
        try {
          a.deleteAlarmNotification(null, null, (data, error) => {
            try {
              const displayE = 'groupkey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteAlarmNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.deleteAlarmNotification('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteAlarmNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadAllAlarmNotifications - errors', () => {
      it('should have a loadAllAlarmNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.loadAllAlarmNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkDeleteAlarmNotification - errors', () => {
      it('should have a bulkDeleteAlarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.bulkDeleteAlarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.bulkDeleteAlarmNotification('fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-bulkDeleteAlarmNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshAlarmNotification - errors', () => {
      it('should have a refreshAlarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.refreshAlarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmNotificationRule - errors', () => {
      it('should have a getAlarmNotificationRule function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmNotificationRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getAlarmNotificationRule(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAlarmNotificationRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAlarmNotification - errors', () => {
      it('should have a searchAlarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.searchAlarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchString', (done) => {
        try {
          a.searchAlarmNotification(null, (data, error) => {
            try {
              const displayE = 'searchString is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-searchAlarmNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMQPEvents - errors', () => {
      it('should have a getAMQPEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getAMQPEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMQPObjEvents - errors', () => {
      it('should have a getAMQPObjEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getAMQPObjEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplianceStatus - errors', () => {
      it('should have a getAllApplianceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplianceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceStatusById - errors', () => {
      it('should have a getApplianceStatusById function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceStatusById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getApplianceStatusById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceStatusById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceTemplateListing - errors', () => {
      it('should have a getApplianceTemplateListing function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceTemplateListing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getApplianceTemplateListing(null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceTemplateListing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliances - errors', () => {
      it('should have a getAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliances2 - errors', () => {
      it('should have a getAppliances2 function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliances2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getAppliances2(null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliances2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getAppliances2(20, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliances2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesByTypeAndOrg - errors', () => {
      it('should have a getAppliancesByTypeAndOrg function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesByTypeAndOrg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getAppliancesByTypeAndOrg(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancesByTypeAndOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesByType - errors', () => {
      it('should have a getAppliancesByType function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAppliancesByType(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancesByType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForSecurityPackage - errors', () => {
      it('should have a getAppliancesForSecurityPackage function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesForSecurityPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportApplianceConfiguration - errors', () => {
      it('should have a exportApplianceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.exportApplianceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterAppliances - errors', () => {
      it('should have a filterAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.filterAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForOrganization - errors', () => {
      it('should have a getAppliancesForOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesForOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getAppliancesForOrganization('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancesForOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsForAppliance - errors', () => {
      it('should have a getTagsForAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.getTagsForAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsForTenant - errors', () => {
      it('should have a getTagsForTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getTagsForTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantName', (done) => {
        try {
          a.getTagsForTenant(null, (data, error) => {
            try {
              const displayE = 'tenantName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTagsForTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importApplianceConfiguration - errors', () => {
      it('should have a importApplianceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.importApplianceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setApplianceOwner - errors', () => {
      it('should have a setApplianceOwner function', (done) => {
        try {
          assert.equal(true, typeof a.setApplianceOwner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.setApplianceOwner('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-setApplianceOwner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForTenantOrgs - errors', () => {
      it('should have a getAppliancesForTenantOrgs function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesForTenantOrgs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAppliances - errors', () => {
      it('should have a searchAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.searchAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesSummary - errors', () => {
      it('should have a getAppliancesSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#doSyncFromAppliance - errors', () => {
      it('should have a doSyncFromAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.doSyncFromAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.doSyncFromAppliance(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-doSyncFromAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#doSyncToAppliance - errors', () => {
      it('should have a doSyncToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.doSyncToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.doSyncToAppliance(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-doSyncToAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTagsFromAppliance - errors', () => {
      it('should have a deleteTagsFromAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTagsFromAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.deleteTagsFromAppliance(null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTagsFromAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendorForAppliance - errors', () => {
      it('should have a getVendorForAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.getVendorForAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getVendorForAppliance(null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVendorForAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vmName', (done) => {
        try {
          a.getVendorForAppliance('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vmName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVendorForAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplianceRoutingInstanceInformation - errors', () => {
      it('should have a getAllApplianceRoutingInstanceInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplianceRoutingInstanceInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getAllApplianceRoutingInstanceInformation(null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllApplianceRoutingInstanceInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceRoutingInstanceInformationById - errors', () => {
      it('should have a getApplianceRoutingInstanceInformationById function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceRoutingInstanceInformationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getApplianceRoutingInstanceInformationById(null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceRoutingInstanceInformationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routingInstanceName', (done) => {
        try {
          a.getApplianceRoutingInstanceInformationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routingInstanceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceRoutingInstanceInformationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setTagsForAppliance - errors', () => {
      it('should have a setTagsForAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.setTagsForAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.setTagsForAppliance(null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-setTagsForAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationServiceTemplates - errors', () => {
      it('should have a getAllApplicationServiceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationServiceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationServiceTemplate - errors', () => {
      it('should have a createApplicationServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newApplicationServiceTemplate', (done) => {
        try {
          a.createApplicationServiceTemplate(null, (data, error) => {
            try {
              const displayE = 'newApplicationServiceTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createApplicationServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationServiceTemplateSamples - errors', () => {
      it('should have a getAllApplicationServiceTemplateSamples function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplicationServiceTemplateSamples === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneApplicationServiceTemplate - errors', () => {
      it('should have a cloneApplicationServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.cloneApplicationServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newTemplate', (done) => {
        try {
          a.cloneApplicationServiceTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'newTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-cloneApplicationServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceTemplateName', (done) => {
        try {
          a.cloneApplicationServiceTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-cloneApplicationServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployApplicationServiceTemplate - errors', () => {
      it('should have a deployApplicationServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deployApplicationServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceTemplateName', (done) => {
        try {
          a.deployApplicationServiceTemplate(null, (data, error) => {
            try {
              const displayE = 'serviceTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployApplicationServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplicationServiceTemplate - errors', () => {
      it('should have a updateApplicationServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplicationServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing astName', (done) => {
        try {
          a.updateApplicationServiceTemplate(null, null, (data, error) => {
            try {
              const displayE = 'astName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateApplicationServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newApplicationServiceTemplate', (done) => {
        try {
          a.updateApplicationServiceTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'newApplicationServiceTemplate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateApplicationServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationServiceTemplate - errors', () => {
      it('should have a deleteApplicationServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceTemplateNames', (done) => {
        try {
          a.deleteApplicationServiceTemplate(null, (data, error) => {
            try {
              const displayE = 'serviceTemplateNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteApplicationServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationServiceTemplateById - errors', () => {
      it('should have a getApplicationServiceTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationServiceTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceTemplateName', (done) => {
        try {
          a.getApplicationServiceTemplateById(null, (data, error) => {
            try {
              const displayE = 'serviceTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplicationServiceTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommunityStrings - errors', () => {
      it('should have a getAllCommunityStrings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCommunityStrings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCommunity - errors', () => {
      it('should have a createCommunity function', (done) => {
        try {
          assert.equal(true, typeof a.createCommunity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing community', (done) => {
        try {
          a.createCommunity(null, (data, error) => {
            try {
              const displayE = 'community is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createCommunity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkCommunities - errors', () => {
      it('should have a createBulkCommunities function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkCommunities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing communityBeanCeate', (done) => {
        try {
          a.createBulkCommunities(null, (data, error) => {
            try {
              const displayE = 'communityBeanCeate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createBulkCommunities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextAvailableCommunityId - errors', () => {
      it('should have a getNextAvailableCommunityId function', (done) => {
        try {
          assert.equal(true, typeof a.getNextAvailableCommunityId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControllerWorkflowList - errors', () => {
      it('should have a getControllerWorkflowList function', (done) => {
        try {
          assert.equal(true, typeof a.getControllerWorkflowList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestParams', (done) => {
        try {
          a.getControllerWorkflowList(null, (data, error) => {
            try {
              const displayE = 'requestParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getControllerWorkflowList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createJsonControllerWorkflow - errors', () => {
      it('should have a createJsonControllerWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.createJsonControllerWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdwanControllerWorkflowVO', (done) => {
        try {
          a.createJsonControllerWorkflow(null, (data, error) => {
            try {
              const displayE = 'sdwanControllerWorkflowVO is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createJsonControllerWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteControllerWorkflows - errors', () => {
      it('should have a deleteControllerWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.deleteControllerWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteControllerWorkflows(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteControllerWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployControllerWorkflow - errors', () => {
      it('should have a deployControllerWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deployControllerWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerworkflowname', (done) => {
        try {
          a.deployControllerWorkflow(null, null, null, (data, error) => {
            try {
              const displayE = 'controllerworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployControllerWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestParams', (done) => {
        try {
          a.deployControllerWorkflow('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'requestParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployControllerWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJsonControllerWorkflow - errors', () => {
      it('should have a getJsonControllerWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getJsonControllerWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerworkflowname', (done) => {
        try {
          a.getJsonControllerWorkflow(null, (data, error) => {
            try {
              const displayE = 'controllerworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getJsonControllerWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateJsonControllerWorkflow - errors', () => {
      it('should have a updateJsonControllerWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.updateJsonControllerWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerworkflowname', (done) => {
        try {
          a.updateJsonControllerWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'controllerworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateJsonControllerWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdwanControllerWorkflowVO', (done) => {
        try {
          a.updateJsonControllerWorkflow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sdwanControllerWorkflowVO is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateJsonControllerWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteControllerWorkflow - errors', () => {
      it('should have a deleteControllerWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteControllerWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerworkflowname', (done) => {
        try {
          a.deleteControllerWorkflow(null, (data, error) => {
            try {
              const displayE = 'controllerworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteControllerWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceWorkflows - errors', () => {
      it('should have a getAllDeviceWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDevicefromJson - errors', () => {
      it('should have a createDevicefromJson function', (done) => {
        try {
          assert.equal(true, typeof a.createDevicefromJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdwanWorkflowWrapperData', (done) => {
        try {
          a.createDevicefromJson(null, (data, error) => {
            try {
              const displayE = 'sdwanWorkflowWrapperData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createDevicefromJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deploybulkDevicesJson - errors', () => {
      it('should have a deploybulkDevicesJson function', (done) => {
        try {
          assert.equal(true, typeof a.deploybulkDevicesJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceWorkflowNamesListWrapper', (done) => {
        try {
          a.deploybulkDevicesJson(null, (data, error) => {
            try {
              const displayE = 'deviceWorkflowNamesListWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deploybulkDevicesJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployDevice - errors', () => {
      it('should have a deployDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deployDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceworkflowname', (done) => {
        try {
          a.deployDevice(null, (data, error) => {
            try {
              const displayE = 'deviceworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceWorkflowTemplateDataInJson - errors', () => {
      it('should have a getDeviceWorkflowTemplateDataInJson function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceWorkflowTemplateDataInJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceWorkflowDatainJson', (done) => {
        try {
          a.getDeviceWorkflowTemplateDataInJson(null, null, (data, error) => {
            try {
              const displayE = 'deviceWorkflowDatainJson is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceWorkflowTemplateDataInJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceworkflowname', (done) => {
        try {
          a.getDeviceWorkflowTemplateDataInJson('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceWorkflowTemplateDataInJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHaPairedSiteLocationIdMapByTemplate - errors', () => {
      it('should have a getHaPairedSiteLocationIdMapByTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getHaPairedSiteLocationIdMapByTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getHaPairedSiteLocationIdMapByTemplate(null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getHaPairedSiteLocationIdMapByTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getHaPairedSiteLocationIdMapByTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getHaPairedSiteLocationIdMapByTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceJsonWorkflow - errors', () => {
      it('should have a getDeviceJsonWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceJsonWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceworkflowname', (done) => {
        try {
          a.getDeviceJsonWorkflow(null, (data, error) => {
            try {
              const displayE = 'deviceworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceJsonWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDevicefromJson - errors', () => {
      it('should have a updateDevicefromJson function', (done) => {
        try {
          assert.equal(true, typeof a.updateDevicefromJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdwanWorkflowWrapperData', (done) => {
        try {
          a.updateDevicefromJson(null, null, (data, error) => {
            try {
              const displayE = 'sdwanWorkflowWrapperData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateDevicefromJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceworkFlowName', (done) => {
        try {
          a.updateDevicefromJson('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceworkFlowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateDevicefromJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deCommisionDeleteWorkflow - errors', () => {
      it('should have a deCommisionDeleteWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deCommisionDeleteWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceworkflowname', (done) => {
        try {
          a.deCommisionDeleteWorkflow('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deCommisionDeleteWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportDeviceWorkflow - errors', () => {
      it('should have a exportDeviceWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.exportDeviceWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceworkflowname', (done) => {
        try {
          a.exportDeviceWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'deviceworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-exportDeviceWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDeviceWorkflows - errors', () => {
      it('should have a importDeviceWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.importDeviceWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing file', (done) => {
        try {
          a.importDeviceWorkflows(null, (data, error) => {
            try {
              const displayE = 'file is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importDeviceWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDeviceWorkflowProgressStatus - errors', () => {
      it('should have a importDeviceWorkflowProgressStatus function', (done) => {
        try {
          assert.equal(true, typeof a.importDeviceWorkflowProgressStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.importDeviceWorkflowProgressStatus(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importDeviceWorkflowProgressStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDeviceWorkflowResult - errors', () => {
      it('should have a importDeviceWorkflowResult function', (done) => {
        try {
          assert.equal(true, typeof a.importDeviceWorkflowResult === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.importDeviceWorkflowResult(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importDeviceWorkflowResult', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceOwnerStatusInJson - errors', () => {
      it('should have a getDeviceOwnerStatusInJson function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceOwnerStatusInJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.getDeviceOwnerStatusInJson('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceOwnerStatusInJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceteWorkflows - errors', () => {
      it('should have a deleteDeviceteWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceteWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceWorkflowNames', (done) => {
        try {
          a.deleteDeviceteWorkflows('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceWorkflowNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteDeviceteWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#migrateSDWANWorkflow - errors', () => {
      it('should have a migrateSDWANWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.migrateSDWANWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgWorkflowList - errors', () => {
      it('should have a getOrgWorkflowList function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgWorkflowList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrgWorkflowFromJson - errors', () => {
      it('should have a createOrgWorkflowFromJson function', (done) => {
        try {
          assert.equal(true, typeof a.createOrgWorkflowFromJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing content', (done) => {
        try {
          a.createOrgWorkflowFromJson(null, (data, error) => {
            try {
              const displayE = 'content is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createOrgWorkflowFromJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployOrgWorkflow - errors', () => {
      it('should have a deployOrgWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deployOrgWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgWorkflowName', (done) => {
        try {
          a.deployOrgWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'orgWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployOrgWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgWorkflowFromJson - errors', () => {
      it('should have a getOrgWorkflowFromJson function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgWorkflowFromJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgWorkflowName', (done) => {
        try {
          a.getOrgWorkflowFromJson(null, (data, error) => {
            try {
              const displayE = 'orgWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrgWorkflowFromJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrgWorkflowFromJson - errors', () => {
      it('should have a updateOrgWorkflowFromJson function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrgWorkflowFromJson === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgWorkflowName', (done) => {
        try {
          a.updateOrgWorkflowFromJson(null, null, (data, error) => {
            try {
              const displayE = 'orgWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateOrgWorkflowFromJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sdwanOrgWorkflowVOWrapper', (done) => {
        try {
          a.updateOrgWorkflowFromJson('fakeparam', null, (data, error) => {
            try {
              const displayE = 'sdwanOrgWorkflowVOWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateOrgWorkflowFromJson', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#decommissionOrgWorkflow - errors', () => {
      it('should have a decommissionOrgWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.decommissionOrgWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgWorkflowName', (done) => {
        try {
          a.decommissionOrgWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'orgWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-decommissionOrgWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgWorkflows - errors', () => {
      it('should have a deleteOrgWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrgWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgWorkflowName', (done) => {
        try {
          a.deleteOrgWorkflows(null, (data, error) => {
            try {
              const displayE = 'orgWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOrgWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUCPEServiceChains - errors', () => {
      it('should have a getUCPEServiceChains function', (done) => {
        try {
          assert.equal(true, typeof a.getUCPEServiceChains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestParams', (done) => {
        try {
          a.getUCPEServiceChains(null, (data, error) => {
            try {
              const displayE = 'requestParams is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getUCPEServiceChains', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUCPEServiceChain - errors', () => {
      it('should have a createUCPEServiceChain function', (done) => {
        try {
          assert.equal(true, typeof a.createUCPEServiceChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ucpeServiceChainWorkflowVO', (done) => {
        try {
          a.createUCPEServiceChain(null, (data, error) => {
            try {
              const displayE = 'ucpeServiceChainWorkflowVO is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createUCPEServiceChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployUCPEServiceChain - errors', () => {
      it('should have a deployUCPEServiceChain function', (done) => {
        try {
          assert.equal(true, typeof a.deployUCPEServiceChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uCPEServiceChainWorkflowName', (done) => {
        try {
          a.deployUCPEServiceChain('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uCPEServiceChainWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployUCPEServiceChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUCPEServiceChainById - errors', () => {
      it('should have a getUCPEServiceChainById function', (done) => {
        try {
          assert.equal(true, typeof a.getUCPEServiceChainById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uCPEServiceChainWorkflowName', (done) => {
        try {
          a.getUCPEServiceChainById(null, (data, error) => {
            try {
              const displayE = 'uCPEServiceChainWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getUCPEServiceChainById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUCPEServiceChain - errors', () => {
      it('should have a updateUCPEServiceChain function', (done) => {
        try {
          assert.equal(true, typeof a.updateUCPEServiceChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uCPEServiceChainWorkflowName', (done) => {
        try {
          a.updateUCPEServiceChain(null, null, (data, error) => {
            try {
              const displayE = 'uCPEServiceChainWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateUCPEServiceChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ucpeServiceChainWorkflowVO', (done) => {
        try {
          a.updateUCPEServiceChain('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ucpeServiceChainWorkflowVO is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateUCPEServiceChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUCPEServiceChain - errors', () => {
      it('should have a deleteUCPEServiceChain function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUCPEServiceChain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uCPEServiceChainWorkflowName', (done) => {
        try {
          a.deleteUCPEServiceChain(null, (data, error) => {
            try {
              const displayE = 'uCPEServiceChainWorkflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteUCPEServiceChain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTemplatesWorkflows - errors', () => {
      it('should have a getAllTemplatesWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTemplatesWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTemplateWorkflow - errors', () => {
      it('should have a createTemplateWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.createTemplateWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployTemplateWorkflow - errors', () => {
      it('should have a deployTemplateWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.deployTemplateWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateworkflowname', (done) => {
        try {
          a.deployTemplateWorkflow(null, null, (data, error) => {
            try {
              const displayE = 'templateworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployTemplateWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateWorkflowById - errors', () => {
      it('should have a getTemplateWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateworkflowname', (done) => {
        try {
          a.getTemplateWorkflowById(null, (data, error) => {
            try {
              const displayE = 'templateworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTemplateWorkflow - errors', () => {
      it('should have a updateTemplateWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.updateTemplateWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateworkflowname', (done) => {
        try {
          a.updateTemplateWorkflow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateworkflowname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateTemplateWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#decommisionTemplateWorkflow - errors', () => {
      it('should have a decommisionTemplateWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.decommisionTemplateWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templatename', (done) => {
        try {
          a.decommisionTemplateWorkflow(null, (data, error) => {
            try {
              const displayE = 'templatename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-decommisionTemplateWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateWorkflows - errors', () => {
      it('should have a deleteTemplateWorkflows function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateWorkflows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templatenames', (done) => {
        try {
          a.deleteTemplateWorkflows(null, (data, error) => {
            try {
              const displayE = 'templatenames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateWorkflows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPortToTemplateWorkflow - errors', () => {
      it('should have a addPortToTemplateWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.addPortToTemplateWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPortToTemplateWorkflow('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPortToTemplateWorkflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRbacPrivilege - errors', () => {
      it('should have a getAllRbacPrivilege function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRbacPrivilege === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshRbacRolesMetaInMemory - errors', () => {
      it('should have a refreshRbacRolesMetaInMemory function', (done) => {
        try {
          assert.equal(true, typeof a.refreshRbacRolesMetaInMemory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshRbacRolesMetaInMemoryById - errors', () => {
      it('should have a refreshRbacRolesMetaInMemoryById function', (done) => {
        try {
          assert.equal(true, typeof a.refreshRbacRolesMetaInMemoryById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.refreshRbacRolesMetaInMemoryById(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-refreshRbacRolesMetaInMemoryById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacOrgRole - errors', () => {
      it('should have a createRbacOrgRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacOrgRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing supportedOrgRoles', (done) => {
        try {
          a.createRbacOrgRole(null, (data, error) => {
            try {
              const displayE = 'supportedOrgRoles is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createRbacOrgRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableRbacOrgRoles - errors', () => {
      it('should have a getAvailableRbacOrgRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getAvailableRbacOrgRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacOrgRoles - errors', () => {
      it('should have a getRbacOrgRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacOrgRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rolename', (done) => {
        try {
          a.getRbacOrgRoles(null, (data, error) => {
            try {
              const displayE = 'rolename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getRbacOrgRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacProviderRoles - errors', () => {
      it('should have a getRbacProviderRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacProviderRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRbacRole - errors', () => {
      it('should have a createRbacRole function', (done) => {
        try {
          assert.equal(true, typeof a.createRbacRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createRole', (done) => {
        try {
          a.createRbacRole(null, (data, error) => {
            try {
              const displayE = 'createRole is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createRbacRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRbacRole - errors', () => {
      it('should have a updateRbacRole function', (done) => {
        try {
          assert.equal(true, typeof a.updateRbacRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createRole', (done) => {
        try {
          a.updateRbacRole(null, (data, error) => {
            try {
              const displayE = 'createRole is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateRbacRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacRoleById - errors', () => {
      it('should have a getRbacRoleById function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacRoleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rolename', (done) => {
        try {
          a.getRbacRoleById(null, (data, error) => {
            try {
              const displayE = 'rolename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getRbacRoleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacRole - errors', () => {
      it('should have a deleteRbacRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rolename', (done) => {
        try {
          a.deleteRbacRole(null, (data, error) => {
            try {
              const displayE = 'rolename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteRbacRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployRbacRole - errors', () => {
      it('should have a deployRbacRole function', (done) => {
        try {
          assert.equal(true, typeof a.deployRbacRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rolename', (done) => {
        try {
          a.deployRbacRole(null, (data, error) => {
            try {
              const displayE = 'rolename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployRbacRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployRbacRoleAsync - errors', () => {
      it('should have a deployRbacRoleAsync function', (done) => {
        try {
          assert.equal(true, typeof a.deployRbacRoleAsync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rolename', (done) => {
        try {
          a.deployRbacRoleAsync(null, null, (data, error) => {
            try {
              const displayE = 'rolename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deployRbacRoleAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacTenantRoles - errors', () => {
      it('should have a getRbacTenantRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacTenantRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaDevice - errors', () => {
      it('should have a getVersaDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getVersaDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getVersaDevice(null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVersaDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroups - errors', () => {
      it('should have a getAllDeviceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceGroup - errors', () => {
      it('should have a createDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupWriteWrapper', (done) => {
        try {
          a.createDeviceGroup(null, (data, error) => {
            try {
              const displayE = 'deviceGroupWriteWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkDeleteDeviceGroup - errors', () => {
      it('should have a bulkDeleteDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.bulkDeleteDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupNames', (done) => {
        try {
          a.bulkDeleteDeviceGroup(null, (data, error) => {
            try {
              const displayE = 'deviceGroupNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-bulkDeleteDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroupsNames - errors', () => {
      it('should have a getAllDeviceGroupsNames function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceGroupsNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getAllDeviceGroupsNames(null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllDeviceGroupsNames', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroupsByAnyTemplateAndOrg - errors', () => {
      it('should have a getAllDeviceGroupsByAnyTemplateAndOrg function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceGroupsByAnyTemplateAndOrg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.getAllDeviceGroupsByAnyTemplateAndOrg('fakeparam', null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllDeviceGroupsByAnyTemplateAndOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroupsByTemplate - errors', () => {
      it('should have a getAllDeviceGroupsByTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDeviceGroupsByTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.getAllDeviceGroupsByTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllDeviceGroupsByTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupById - errors', () => {
      it('should have a getDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.getDeviceGroupById(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceGroup - errors', () => {
      it('should have a updateDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupWriteWrapper', (done) => {
        try {
          a.updateDeviceGroup(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupWriteWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.updateDeviceGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroup - errors', () => {
      it('should have a deleteDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.deleteDeviceGroup(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupStandbyInfo - errors', () => {
      it('should have a getDeviceGroupStandbyInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupStandbyInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.getDeviceGroupStandbyInfo(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceGroupStandbyInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupTemplateAssociation - errors', () => {
      it('should have a getDeviceGroupTemplateAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceGroupTemplateAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.getDeviceGroupTemplateAssociation(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceGroupTemplateAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing postStagingTemplateName', (done) => {
        try {
          a.getDeviceGroupTemplateAssociation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'postStagingTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDeviceGroupTemplateAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerDevice - errors', () => {
      it('should have a registerDevice function', (done) => {
        try {
          assert.equal(true, typeof a.registerDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegistrationDeviceInfo - errors', () => {
      it('should have a getRegistrationDeviceInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getRegistrationDeviceInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regtoken', (done) => {
        try {
          a.getRegistrationDeviceInfo(null, (data, error) => {
            try {
              const displayE = 'regtoken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getRegistrationDeviceInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerDeviceAsync - errors', () => {
      it('should have a registerDeviceAsync function', (done) => {
        try {
          assert.equal(true, typeof a.registerDeviceAsync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regtoken', (done) => {
        try {
          a.registerDeviceAsync(null, (data, error) => {
            try {
              const displayE = 'regtoken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-registerDeviceAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDeviceEMAILSecureCode - errors', () => {
      it('should have a generateDeviceEMAILSecureCode function', (done) => {
        try {
          assert.equal(true, typeof a.generateDeviceEMAILSecureCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regtoken', (done) => {
        try {
          a.generateDeviceEMAILSecureCode(null, (data, error) => {
            try {
              const displayE = 'regtoken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateDeviceEMAILSecureCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDeviceSMSSecureCode - errors', () => {
      it('should have a generateDeviceSMSSecureCode function', (done) => {
        try {
          assert.equal(true, typeof a.generateDeviceSMSSecureCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regtoken', (done) => {
        try {
          a.generateDeviceSMSSecureCode(null, (data, error) => {
            try {
              const displayE = 'regtoken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateDeviceSMSSecureCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyDeviceSecureCodeAndRegisterAsync - errors', () => {
      it('should have a verifyDeviceSecureCodeAndRegisterAsync function', (done) => {
        try {
          assert.equal(true, typeof a.verifyDeviceSecureCodeAndRegisterAsync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regtoken', (done) => {
        try {
          a.verifyDeviceSecureCodeAndRegisterAsync(null, null, (data, error) => {
            try {
              const displayE = 'regtoken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-verifyDeviceSecureCodeAndRegisterAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securecode', (done) => {
        try {
          a.verifyDeviceSecureCodeAndRegisterAsync('fakeparam', null, (data, error) => {
            try {
              const displayE = 'securecode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-verifyDeviceSecureCodeAndRegisterAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportDocsSwagger - errors', () => {
      it('should have a exportDocsSwagger function', (done) => {
        try {
          assert.equal(true, typeof a.exportDocsSwagger === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing group', (done) => {
        try {
          a.exportDocsSwagger('fakeparam', null, (data, error) => {
            try {
              const displayE = 'group is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-exportDocsSwagger', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssets - errors', () => {
      it('should have a getAssets function', (done) => {
        try {
          assert.equal(true, typeof a.getAssets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableAssets - errors', () => {
      it('should have a availableAssets function', (done) => {
        try {
          assert.equal(true, typeof a.availableAssets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAssetEmail - errors', () => {
      it('should have a generateAssetEmail function', (done) => {
        try {
          assert.equal(true, typeof a.generateAssetEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceNames', (done) => {
        try {
          a.generateAssetEmail(null, null, (data, error) => {
            try {
              const displayE = 'deviceNames is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateAssetEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing email', (done) => {
        try {
          a.generateAssetEmail('fakeparam', null, (data, error) => {
            try {
              const displayE = 'email is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateAssetEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAsset - errors', () => {
      it('should have a updateAsset function', (done) => {
        try {
          assert.equal(true, typeof a.updateAsset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing asset', (done) => {
        try {
          a.updateAsset(null, null, (data, error) => {
            try {
              const displayE = 'asset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAsset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.updateAsset('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateAsset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelAssetSerialNumberReplacement - errors', () => {
      it('should have a cancelAssetSerialNumberReplacement function', (done) => {
        try {
          assert.equal(true, typeof a.cancelAssetSerialNumberReplacement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing originalSerialNo', (done) => {
        try {
          a.cancelAssetSerialNumberReplacement(null, (data, error) => {
            try {
              const displayE = 'originalSerialNo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-cancelAssetSerialNumberReplacement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAsset - errors', () => {
      it('should have a replaceAsset function', (done) => {
        try {
          assert.equal(true, typeof a.replaceAsset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing originalSerialNo', (done) => {
        try {
          a.replaceAsset(null, null, (data, error) => {
            try {
              const displayE = 'originalSerialNo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-replaceAsset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing replacementSerialNo', (done) => {
        try {
          a.replaceAsset('fakeparam', null, (data, error) => {
            try {
              const displayE = 'replacementSerialNo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-replaceAsset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateBranchWithSerialNumber - errors', () => {
      it('should have a associateBranchWithSerialNumber function', (done) => {
        try {
          assert.equal(true, typeof a.associateBranchWithSerialNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing branchName', (done) => {
        try {
          a.associateBranchWithSerialNumber(null, null, (data, error) => {
            try {
              const displayE = 'branchName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-associateBranchWithSerialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNo', (done) => {
        try {
          a.associateBranchWithSerialNumber('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serialNo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-associateBranchWithSerialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownDevices - errors', () => {
      it('should have a getUnknownDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getUnknownDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllUnknownDevices - errors', () => {
      it('should have a deleteAllUnknownDevices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAllUnknownDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.deleteAllUnknownDevices(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteAllUnknownDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownDeviceById - errors', () => {
      it('should have a getUnknownDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.getUnknownDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.getUnknownDeviceById(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getUnknownDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnknownDeviceById - errors', () => {
      it('should have a deleteUnknownDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnknownDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serialNumber', (done) => {
        try {
          a.deleteUnknownDeviceById(null, (data, error) => {
            try {
              const displayE = 'serialNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteUnknownDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapGroups - errors', () => {
      it('should have a getLdapGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getLdapGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapServerProfile - errors', () => {
      it('should have a getLdapServerProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getLdapServerProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLdapServerProfile - errors', () => {
      it('should have a createLdapServerProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createLdapServerProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgToLDAPServerProfilesMap - errors', () => {
      it('should have a getOrgToLDAPServerProfilesMap function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgToLDAPServerProfilesMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapServerProfiles - errors', () => {
      it('should have a getLdapServerProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getLdapServerProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapUsers - errors', () => {
      it('should have a getLdapUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getLdapUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLdapServerProfile - errors', () => {
      it('should have a updateLdapServerProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateLdapServerProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLdapServerProfile - errors', () => {
      it('should have a deleteLdapServerProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLdapServerProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testSmtpSettingResponse - errors', () => {
      it('should have a testSmtpSettingResponse function', (done) => {
        try {
          assert.equal(true, typeof a.testSmtpSettingResponse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAndSaveSPAndIDPMetadata - errors', () => {
      it('should have a generateAndSaveSPAndIDPMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.generateAndSaveSPAndIDPMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssoConfigWrapper', (done) => {
        try {
          a.generateAndSaveSPAndIDPMetadata(null, (data, error) => {
            try {
              const displayE = 'ssoConfigWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateAndSaveSPAndIDPMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#udpateSPAndIDPMetadata - errors', () => {
      it('should have a udpateSPAndIDPMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.udpateSPAndIDPMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssoConfigWrapper', (done) => {
        try {
          a.udpateSPAndIDPMetadata(null, (data, error) => {
            try {
              const displayE = 'ssoConfigWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-udpateSPAndIDPMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextgenOrganizations - errors', () => {
      it('should have a getNextgenOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.getNextgenOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganization - errors', () => {
      it('should have a createOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.createOrganization(null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationAgents - errors', () => {
      it('should have a getOrganizationAgents function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationAgents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getOrganizationAgents(null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrganizationAgents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgsDetails - errors', () => {
      it('should have a getOrgsDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgsDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgUUIDs', (done) => {
        try {
          a.getOrgsDetails(null, (data, error) => {
            try {
              const displayE = 'orgUUIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrgsDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRootOrganizations - errors', () => {
      it('should have a getRootOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.getRootOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetailsForUUid - errors', () => {
      it('should have a getDetailsForUUid function', (done) => {
        try {
          assert.equal(true, typeof a.getDetailsForUUid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getDetailsForUUid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDetailsForUUid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgVrfs - errors', () => {
      it('should have a getOrgVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getOrgVrfs(null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrgVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDetails - errors', () => {
      it('should have a getOrganizationDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.getOrganizationDetails(null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrganizationDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganization - errors', () => {
      it('should have a updateOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updateOrganization(null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganization - errors', () => {
      it('should have a deleteOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deleteOrganization(null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgWanNetworkGroups - errors', () => {
      it('should have a getOrgWanNetworkGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgWanNetworkGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orguuid', (done) => {
        try {
          a.getOrgWanNetworkGroups(null, (data, error) => {
            try {
              const displayE = 'orguuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrgWanNetworkGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrgWanNetworkGroup - errors', () => {
      it('should have a createOrgWanNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createOrgWanNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanNetworkGroup', (done) => {
        try {
          a.createOrgWanNetworkGroup(null, null, (data, error) => {
            try {
              const displayE = 'wanNetworkGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createOrgWanNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orguuid', (done) => {
        try {
          a.createOrgWanNetworkGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orguuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createOrgWanNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgWanNetworkGroups - errors', () => {
      it('should have a deleteOrgWanNetworkGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrgWanNetworkGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteOrgWanNetworkGroups(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOrgWanNetworkGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orguuid', (done) => {
        try {
          a.deleteOrgWanNetworkGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orguuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOrgWanNetworkGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrgWanNetworkGroup - errors', () => {
      it('should have a updateOrgWanNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrgWanNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanNetworkGroups', (done) => {
        try {
          a.updateOrgWanNetworkGroup(null, null, (data, error) => {
            try {
              const displayE = 'wanNetworkGroups is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateOrgWanNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orguuid', (done) => {
        try {
          a.updateOrgWanNetworkGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orguuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateOrgWanNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgWanNetworkGroup - errors', () => {
      it('should have a deleteOrgWanNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrgWanNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteOrgWanNetworkGroup(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOrgWanNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orguuid', (done) => {
        try {
          a.deleteOrgWanNetworkGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orguuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOrgWanNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizations - errors', () => {
      it('should have a getOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgsCount - errors', () => {
      it('should have a getOrgsCount function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgsCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setOrgWanPropagate - errors', () => {
      it('should have a setOrgWanPropagate function', (done) => {
        try {
          assert.equal(true, typeof a.setOrgWanPropagate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanPropagate', (done) => {
        try {
          a.setOrgWanPropagate(null, (data, error) => {
            try {
              const displayE = 'wanPropagate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-setOrgWanPropagate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZonesForOrganization - errors', () => {
      it('should have a getZonesForOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.getZonesForOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getZonesForOrganization(null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getZonesForOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgChildren - errors', () => {
      it('should have a getOrgChildren function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgChildren === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.getOrgChildren('fakeparam', null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrgChildren', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllOsspackPossibleCurrentVersionOnDevice - errors', () => {
      it('should have a listAllOsspackPossibleCurrentVersionOnDevice function', (done) => {
        try {
          assert.equal(true, typeof a.listAllOsspackPossibleCurrentVersionOnDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOsspackDownloadsForDevice - errors', () => {
      it('should have a getAllOsspackDownloadsForDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOsspackDownloadsForDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDeviceOsspackUpdates - errors', () => {
      it('should have a checkDeviceOsspackUpdates function', (done) => {
        try {
          assert.equal(true, typeof a.checkDeviceOsspackUpdates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateType', (done) => {
        try {
          a.checkDeviceOsspackUpdates('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-checkDeviceOsspackUpdates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDeviceOsspackUpdatesByDeviceName - errors', () => {
      it('should have a checkDeviceOsspackUpdatesByDeviceName function', (done) => {
        try {
          assert.equal(true, typeof a.checkDeviceOsspackUpdatesByDeviceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.checkDeviceOsspackUpdatesByDeviceName(null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-checkDeviceOsspackUpdatesByDeviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateType', (done) => {
        try {
          a.checkDeviceOsspackUpdatesByDeviceName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-checkDeviceOsspackUpdatesByDeviceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOsspackFromDevice - errors', () => {
      it('should have a deleteOsspackFromDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOsspackFromDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateType', (done) => {
        try {
          a.deleteOsspackFromDevice(null, null, (data, error) => {
            try {
              const displayE = 'updateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOsspackFromDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.deleteOsspackFromDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOsspackFromDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceOsspackDownloadsToBeInstalled - errors', () => {
      it('should have a getDeviceOsspackDownloadsToBeInstalled function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceOsspackDownloadsToBeInstalled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instalOsspackOnlDevices - errors', () => {
      it('should have a instalOsspackOnlDevices function', (done) => {
        try {
          assert.equal(true, typeof a.instalOsspackOnlDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicesInstallBean', (done) => {
        try {
          a.instalOsspackOnlDevices(null, null, (data, error) => {
            try {
              const displayE = 'devicesInstallBean is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-instalOsspackOnlDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOsspackDownloadsForDirector - errors', () => {
      it('should have a getAllOsspackDownloadsForDirector function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOsspackDownloadsForDirector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDirectorOsspackUpdates - errors', () => {
      it('should have a checkDirectorOsspackUpdates function', (done) => {
        try {
          assert.equal(true, typeof a.checkDirectorOsspackUpdates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateType', (done) => {
        try {
          a.checkDirectorOsspackUpdates(null, (data, error) => {
            try {
              const displayE = 'updateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-checkDirectorOsspackUpdates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOsspackFromDirector - errors', () => {
      it('should have a deleteOsspackFromDirector function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOsspackFromDirector === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateType', (done) => {
        try {
          a.deleteOsspackFromDirector(null, null, (data, error) => {
            try {
              const displayE = 'updateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOsspackFromDirector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.deleteOsspackFromDirector('fakeparam', null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteOsspackFromDirector', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorOsspackDownloadsToBeInstalled - errors', () => {
      it('should have a getDirectorOsspackDownloadsToBeInstalled function', (done) => {
        try {
          assert.equal(true, typeof a.getDirectorOsspackDownloadsToBeInstalled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#installDirectorOsspack - errors', () => {
      it('should have a installDirectorOsspack function', (done) => {
        try {
          assert.equal(true, typeof a.installDirectorOsspack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing directorInstallBean', (done) => {
        try {
          a.installDirectorOsspack(null, null, (data, error) => {
            try {
              const displayE = 'directorInstallBean is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-installDirectorOsspack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadOsspack - errors', () => {
      it('should have a downloadOsspack function', (done) => {
        try {
          assert.equal(true, typeof a.downloadOsspack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing product', (done) => {
        try {
          a.downloadOsspack(null, null, null, null, (data, error) => {
            try {
              const displayE = 'product is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-downloadOsspack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateType', (done) => {
        try {
          a.downloadOsspack('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'updateType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-downloadOsspack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.downloadOsspack('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-downloadOsspack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminAlerts - errors', () => {
      it('should have a getAdminAlerts function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminAlerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateRemainingDays - errors', () => {
      it('should have a getCertificateRemainingDays function', (done) => {
        try {
          assert.equal(true, typeof a.getCertificateRemainingDays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadApplianceDebPackage - errors', () => {
      it('should have a uploadApplianceDebPackage function', (done) => {
        try {
          assert.equal(true, typeof a.uploadApplianceDebPackage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileType', (done) => {
        try {
          a.uploadApplianceDebPackage('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadApplianceDebPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.uploadApplianceDebPackage('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadApplianceDebPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing productType', (done) => {
        try {
          a.uploadApplianceDebPackage('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'productType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadApplianceDebPackage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemLogLevel - errors', () => {
      it('should have a getSystemLogLevel function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemLogLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logger', (done) => {
        try {
          a.getSystemLogLevel(null, (data, error) => {
            try {
              const displayE = 'logger is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSystemLogLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSystemLogLevel - errors', () => {
      it('should have a updateSystemLogLevel function', (done) => {
        try {
          assert.equal(true, typeof a.updateSystemLogLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing level', (done) => {
        try {
          a.updateSystemLogLevel(null, null, (data, error) => {
            try {
              const displayE = 'level is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSystemLogLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logger', (done) => {
        try {
          a.updateSystemLogLevel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'logger is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSystemLogLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkUpdateMinImageVersion - errors', () => {
      it('should have a checkUpdateMinImageVersion function', (done) => {
        try {
          assert.equal(true, typeof a.checkUpdateMinImageVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRegions - errors', () => {
      it('should have a getAllRegions function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRegion - errors', () => {
      it('should have a createRegion function', (done) => {
        try {
          assert.equal(true, typeof a.createRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing region', (done) => {
        try {
          a.createRegion(null, (data, error) => {
            try {
              const displayE = 'region is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextAvailableRegionId - errors', () => {
      it('should have a getNextAvailableRegionId function', (done) => {
        try {
          assert.equal(true, typeof a.getNextAvailableRegionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegionById - errors', () => {
      it('should have a getRegionById function', (done) => {
        try {
          assert.equal(true, typeof a.getRegionById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionName', (done) => {
        try {
          a.getRegionById(null, (data, error) => {
            try {
              const displayE = 'regionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getRegionById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRegion - errors', () => {
      it('should have a updateRegion function', (done) => {
        try {
          assert.equal(true, typeof a.updateRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing region', (done) => {
        try {
          a.updateRegion(null, null, (data, error) => {
            try {
              const displayE = 'region is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionName', (done) => {
        try {
          a.updateRegion('fakeparam', null, (data, error) => {
            try {
              const displayE = 'regionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRegion - errors', () => {
      it('should have a deleteRegion function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionName', (done) => {
        try {
          a.deleteRegion(null, (data, error) => {
            try {
              const displayE = 'regionName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAndSaveIDPMetadata - errors', () => {
      it('should have a generateAndSaveIDPMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.generateAndSaveIDPMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSOOverallStatus - errors', () => {
      it('should have a getSSOOverallStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSSOOverallStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSORolesMappingById - errors', () => {
      it('should have a getSSORolesMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.getSSORolesMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantname', (done) => {
        try {
          a.getSSORolesMappingById(null, (data, error) => {
            try {
              const displayE = 'tenantname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSSORolesMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSSORolesMapping - errors', () => {
      it('should have a updateSSORolesMapping function', (done) => {
        try {
          assert.equal(true, typeof a.updateSSORolesMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mapper', (done) => {
        try {
          a.updateSSORolesMapping(null, null, (data, error) => {
            try {
              const displayE = 'mapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSSORolesMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantname', (done) => {
        try {
          a.updateSSORolesMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSSORolesMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSSORolesMapping - errors', () => {
      it('should have a deleteSSORolesMapping function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSSORolesMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantname', (done) => {
        try {
          a.deleteSSORolesMapping(null, (data, error) => {
            try {
              const displayE = 'tenantname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteSSORolesMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAndSaveSPMetadata - errors', () => {
      it('should have a generateAndSaveSPMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.generateAndSaveSPMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssoConfigWrapper', (done) => {
        try {
          a.generateAndSaveSPMetadata(null, (data, error) => {
            try {
              const displayE = 'ssoConfigWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateAndSaveSPMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSPMetadata - errors', () => {
      it('should have a updateSPMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.updateSPMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ssoConfigWrapper', (done) => {
        try {
          a.updateSPMetadata(null, (data, error) => {
            try {
              const displayE = 'ssoConfigWrapper is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSPMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSOStatus - errors', () => {
      it('should have a getSSOStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getSSOStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSOStatusWithURL - errors', () => {
      it('should have a getSSOStatusWithURL function', (done) => {
        try {
          assert.equal(true, typeof a.getSSOStatusWithURL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAssetsByOrgNameAndDeviceType - errors', () => {
      it('should have a getSDWANAssetsByOrgNameAndDeviceType function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANAssetsByOrgNameAndDeviceType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceType', (done) => {
        try {
          a.getSDWANAssetsByOrgNameAndDeviceType(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANAssetsByOrgNameAndDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getSDWANAssetsByOrgNameAndDeviceType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANAssetsByOrgNameAndDeviceType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANDeviceMappingURL - errors', () => {
      it('should have a getSDWANDeviceMappingURL function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANDeviceMappingURL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing devicename', (done) => {
        try {
          a.getSDWANDeviceMappingURL('fakeparam', null, (data, error) => {
            try {
              const displayE = 'devicename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANDeviceMappingURL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAvailableIds - errors', () => {
      it('should have a getSDWANAvailableIds function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANAvailableIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing count', (done) => {
        try {
          a.getSDWANAvailableIds(null, null, (data, error) => {
            try {
              const displayE = 'count is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANAvailableIds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.getSDWANAvailableIds(1, null, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANAvailableIds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANNextGlobalId - errors', () => {
      it('should have a getSDWANNextGlobalId function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANNextGlobalId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.getSDWANNextGlobalId(null, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANNextGlobalId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANNextGlobalIdWithSerialNumber - errors', () => {
      it('should have a getSDWANNextGlobalIdWithSerialNumber function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANNextGlobalIdWithSerialNumber === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.getSDWANNextGlobalIdWithSerialNumber(null, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANNextGlobalIdWithSerialNumber', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#freeAllSDWANCachedValues - errors', () => {
      it('should have a freeAllSDWANCachedValues function', (done) => {
        try {
          assert.equal(true, typeof a.freeAllSDWANCachedValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.freeAllSDWANCachedValues(null, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-freeAllSDWANCachedValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#freeSDWANCachedAvailableGlobalId - errors', () => {
      it('should have a freeSDWANCachedAvailableGlobalId function', (done) => {
        try {
          assert.equal(true, typeof a.freeSDWANCachedAvailableGlobalId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing availableId', (done) => {
        try {
          a.freeSDWANCachedAvailableGlobalId(null, null, (data, error) => {
            try {
              const displayE = 'availableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-freeSDWANCachedAvailableGlobalId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.freeSDWANCachedAvailableGlobalId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-freeSDWANCachedAvailableGlobalId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANStagingControllers - errors', () => {
      it('should have a getSDWANStagingControllers function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANStagingControllers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANStagingControllerVPNProfiles - errors', () => {
      it('should have a getSDWANStagingControllerVPNProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANStagingControllerVPNProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controller', (done) => {
        try {
          a.getSDWANStagingControllerVPNProfiles(null, (data, error) => {
            try {
              const displayE = 'controller is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANStagingControllerVPNProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANURLBasedZTPInfo - errors', () => {
      it('should have a getSDWANURLBasedZTPInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANURLBasedZTPInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroup', (done) => {
        try {
          a.getSDWANURLBasedZTPInfo(null, (data, error) => {
            try {
              const displayE = 'deviceGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANURLBasedZTPInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSiteToSiteTunnelVPNProfile - errors', () => {
      it('should have a createSiteToSiteTunnelVPNProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createSiteToSiteTunnelVPNProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSiteToSiteTunnelVPNProfile - errors', () => {
      it('should have a updateSiteToSiteTunnelVPNProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateSiteToSiteTunnelVPNProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteToSiteTunnelVPNProfileNames - errors', () => {
      it('should have a getSiteToSiteTunnelVPNProfileNames function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteToSiteTunnelVPNProfileNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol - errors', () => {
      it('should have a getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgUuid', (done) => {
        try {
          a.getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol(null, null, (data, error) => {
            try {
              const displayE = 'orgUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelProtocol', (done) => {
        try {
          a.getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tunnelProtocol is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteToSiteTunnelVPNProfiles - errors', () => {
      it('should have a getSiteToSiteTunnelVPNProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getSiteToSiteTunnelVPNProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteToSiteTunnelVPNProfiles - errors', () => {
      it('should have a deleteSiteToSiteTunnelVPNProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSiteToSiteTunnelVPNProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelSpackDownload - errors', () => {
      it('should have a cancelSpackDownload function', (done) => {
        try {
          assert.equal(true, typeof a.cancelSpackDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkAvailableSpackUpdates - errors', () => {
      it('should have a checkAvailableSpackUpdates function', (done) => {
        try {
          assert.equal(true, typeof a.checkAvailableSpackUpdates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkSpackUpdate - errors', () => {
      it('should have a checkSpackUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.checkSpackUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSpackPackages - errors', () => {
      it('should have a deleteSpackPackages function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSpackPackages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadSpack - errors', () => {
      it('should have a downloadSpack function', (done) => {
        try {
          assert.equal(true, typeof a.downloadSpack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultiPredefinedSpack - errors', () => {
      it('should have a getMultiPredefinedSpack function', (done) => {
        try {
          assert.equal(true, typeof a.getMultiPredefinedSpack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing xPath', (done) => {
        try {
          a.getMultiPredefinedSpack(null, (data, error) => {
            try {
              const displayE = 'xPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getMultiPredefinedSpack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPredefinedSpack - errors', () => {
      it('should have a getPredefinedSpack function', (done) => {
        try {
          assert.equal(true, typeof a.getPredefinedSpack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateToSpackAppliance - errors', () => {
      it('should have a updateToSpackAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.updateToSpackAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flavour', (done) => {
        try {
          a.updateToSpackAppliance('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'flavour is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateToSpackAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadSpack - errors', () => {
      it('should have a uploadSpack function', (done) => {
        try {
          assert.equal(true, typeof a.uploadSpack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spackChecksumFile', (done) => {
        try {
          a.uploadSpack('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'spackChecksumFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadSpack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spackFile', (done) => {
        try {
          a.uploadSpack('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'spackFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadSpack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpackCount - errors', () => {
      it('should have a getSpackCount function', (done) => {
        try {
          assert.equal(true, typeof a.getSpackCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpackLogs - errors', () => {
      it('should have a getSpackLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getSpackLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpokeGroups - errors', () => {
      it('should have a getSpokeGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getSpokeGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSpokeGroup - errors', () => {
      it('should have a createSpokeGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createSpokeGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spokeGroupBean', (done) => {
        try {
          a.createSpokeGroup(null, (data, error) => {
            try {
              const displayE = 'spokeGroupBean is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createSpokeGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpokeGroupById - errors', () => {
      it('should have a getSpokeGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.getSpokeGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spokeGroupName', (done) => {
        try {
          a.getSpokeGroupById(null, (data, error) => {
            try {
              const displayE = 'spokeGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSpokeGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSpokeGroup - errors', () => {
      it('should have a updateSpokeGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateSpokeGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spokeGroupBean', (done) => {
        try {
          a.updateSpokeGroup(null, null, (data, error) => {
            try {
              const displayE = 'spokeGroupBean is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSpokeGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spokeGroupName', (done) => {
        try {
          a.updateSpokeGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spokeGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSpokeGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSpokeGroup - errors', () => {
      it('should have a deleteSpokeGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSpokeGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spokeGroupName', (done) => {
        try {
          a.deleteSpokeGroup(null, (data, error) => {
            try {
              const displayE = 'spokeGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteSpokeGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasks - errors', () => {
      it('should have a getTasks function', (done) => {
        try {
          assert.equal(true, typeof a.getTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBulkTasks - errors', () => {
      it('should have a deleteBulkTasks function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBulkTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteBulkTasks(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteBulkTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskSummary - errors', () => {
      it('should have a getTaskSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getTaskSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskById - errors', () => {
      it('should have a getTaskById function', (done) => {
        try {
          assert.equal(true, typeof a.getTaskById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTaskById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTaskById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTask - errors', () => {
      it('should have a deleteTask function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTask(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateReferences - errors', () => {
      it('should have a getTemplateReferences function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateReferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroup', (done) => {
        try {
          a.getTemplateReferences(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.getTemplateReferences('fakeparam', null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTemplateReferences - errors', () => {
      it('should have a getAllTemplateReferences function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTemplateReferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroup', (done) => {
        try {
          a.getAllTemplateReferences(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllTemplateReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.getAllTemplateReferences('fakeparam', null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllTemplateReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBindData - errors', () => {
      it('should have a createBindData function', (done) => {
        try {
          assert.equal(true, typeof a.createBindData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.createBindData(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createBindData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBinddataHeaderAndCount - errors', () => {
      it('should have a getBinddataHeaderAndCount function', (done) => {
        try {
          assert.equal(true, typeof a.getBinddataHeaderAndCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.getBinddataHeaderAndCount(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getBinddataHeaderAndCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getBinddataHeaderAndCount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getBinddataHeaderAndCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBinddataForTemplate - errors', () => {
      it('should have a getBinddataForTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getBinddataForTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getBinddataForTemplate(null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getBinddataForTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateBindata - errors', () => {
      it('should have a getTemplateBindata function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateBindata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.getTemplateBindata('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getTemplateBindata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveTemplateBindata - errors', () => {
      it('should have a saveTemplateBindata function', (done) => {
        try {
          assert.equal(true, typeof a.saveTemplateBindata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.saveTemplateBindata(null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-saveTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.saveTemplateBindata('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-saveTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateVariable', (done) => {
        try {
          a.saveTemplateBindata('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateVariable is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-saveTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateBindata - errors', () => {
      it('should have a deleteTemplateBindata function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateBindata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.deleteTemplateBindata(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deleteTemplateBindata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateVariable', (done) => {
        try {
          a.deleteTemplateBindata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateVariable is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateBindata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBindDataById - errors', () => {
      it('should have a getBindDataById function', (done) => {
        try {
          assert.equal(true, typeof a.getBindDataById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getBindDataById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getBindDataById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateCategories - errors', () => {
      it('should have a getTemplateCategories function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateByCategory - errors', () => {
      it('should have a getTemplateByCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateByCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing category', (done) => {
        try {
          a.getTemplateByCategory(null, (data, error) => {
            try {
              const displayE = 'category is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateByCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateCategoriesForOrg - errors', () => {
      it('should have a getTemplateCategoriesForOrg function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateCategoriesForOrg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing category', (done) => {
        try {
          a.getTemplateCategoriesForOrg(null, null, (data, error) => {
            try {
              const displayE = 'category is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateCategoriesForOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getTemplateCategoriesForOrg('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateCategoriesForOrg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateShareCategories - errors', () => {
      it('should have a getTemplateShareCategories function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateShareCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getTemplateShareCategories(null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateShareCategories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateMetadataById - errors', () => {
      it('should have a getTemplateMetadataById function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateMetadataById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getTemplateMetadataById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateMetadataById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveTemplateMetadata - errors', () => {
      it('should have a saveTemplateMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.saveTemplateMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing model', (done) => {
        try {
          a.saveTemplateMetadata(null, null, (data, error) => {
            try {
              const displayE = 'model is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-saveTemplateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.saveTemplateMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-saveTemplateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllImages - errors', () => {
      it('should have a getAllImages function', (done) => {
        try {
          assert.equal(true, typeof a.getAllImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadImageDetails - errors', () => {
      it('should have a uploadImageDetails function', (done) => {
        try {
          assert.equal(true, typeof a.uploadImageDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cpuCount', (done) => {
        try {
          a.uploadImageDetails(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'cpuCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing description', (done) => {
        try {
          a.uploadImageDetails('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'description is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing diskSize', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'diskSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileType', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing isAuxiliaryInterface', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'isAuxiliaryInterface is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing isSecondaryDiskNeeded', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'isSecondaryDiskNeeded is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memory', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'memory is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secondaryDiskSize', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'secondaryDiskSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorProductType', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vendorProductType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.uploadImageDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadImageDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteImage - errors', () => {
      it('should have a deleteImage function', (done) => {
        try {
          assert.equal(true, typeof a.deleteImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.deleteImage(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateImage - errors', () => {
      it('should have a updateImage function', (done) => {
        try {
          assert.equal(true, typeof a.updateImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileType', (done) => {
        try {
          a.updateImage(null, null, null, (data, error) => {
            try {
              const displayE = 'fileType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing page', (done) => {
        try {
          a.updateImage('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'page is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.updateImage('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageLogo - errors', () => {
      it('should have a getImageLogo function', (done) => {
        try {
          assert.equal(true, typeof a.getImageLogo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.getImageLogo(null, null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageLogo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorProductType', (done) => {
        try {
          a.getImageLogo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendorProductType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageLogo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateImageLogoforDoc - errors', () => {
      it('should have a updateImageLogoforDoc function', (done) => {
        try {
          assert.equal(true, typeof a.updateImageLogoforDoc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing page', (done) => {
        try {
          a.updateImageLogoforDoc(null, null, null, (data, error) => {
            try {
              const displayE = 'page is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageLogoforDoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.updateImageLogoforDoc('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageLogoforDoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorProductType', (done) => {
        try {
          a.updateImageLogoforDoc('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendorProductType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageLogoforDoc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateImageMetadata - errors', () => {
      it('should have a updateImageMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.updateImageMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cpuCount', (done) => {
        try {
          a.updateImageMetadata(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'cpuCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing description', (done) => {
        try {
          a.updateImageMetadata('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'description is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing diskSize', (done) => {
        try {
          a.updateImageMetadata('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'diskSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing isAuxiliaryInterface', (done) => {
        try {
          a.updateImageMetadata('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'isAuxiliaryInterface is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing isSecondaryDiskNeeded', (done) => {
        try {
          a.updateImageMetadata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'isSecondaryDiskNeeded is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing memory', (done) => {
        try {
          a.updateImageMetadata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'memory is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing secondaryDiskSize', (done) => {
        try {
          a.updateImageMetadata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'secondaryDiskSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceFunctions', (done) => {
        try {
          a.updateImageMetadata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceFunctions is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.updateImageMetadata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateImageMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByName - errors', () => {
      it('should have a getImageByName function', (done) => {
        try {
          assert.equal(true, typeof a.getImageByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getImageByName(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#urlImageUpload - errors', () => {
      it('should have a urlImageUpload function', (done) => {
        try {
          assert.equal(true, typeof a.urlImageUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageDetails', (done) => {
        try {
          a.urlImageUpload(null, (data, error) => {
            try {
              const displayE = 'imageDetails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-urlImageUpload', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByUuid - errors', () => {
      it('should have a getImageByUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getImageByUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getImageByUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByVendor - errors', () => {
      it('should have a getImageByVendor function', (done) => {
        try {
          assert.equal(true, typeof a.getImageByVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getImageByVendor(null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getImageByVendor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.getImageByVendor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageList - errors', () => {
      it('should have a getImageList function', (done) => {
        try {
          assert.equal(true, typeof a.getImageList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getImageList(null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getImageList('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.getImageList('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorProductType', (done) => {
        try {
          a.getImageList('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendorProductType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByVendorProductType - errors', () => {
      it('should have a getImageByVendorProductType function', (done) => {
        try {
          assert.equal(true, typeof a.getImageByVendorProductType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getImageByVendorProductType(null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByVendorProductType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getImageByVendorProductType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByVendorProductType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorProductType', (done) => {
        try {
          a.getImageByVendorProductType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendorProductType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getImageByVendorProductType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCaptivePortalToAppliance - errors', () => {
      it('should have a uploadCaptivePortalToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.uploadCaptivePortalToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCaptivePortalFromAppliance - errors', () => {
      it('should have a deleteCaptivePortalFromAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCaptivePortalFromAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListOfPortalPages - errors', () => {
      it('should have a getListOfPortalPages function', (done) => {
        try {
          assert.equal(true, typeof a.getListOfPortalPages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCaptivePortalToVD - errors', () => {
      it('should have a uploadCaptivePortalToVD function', (done) => {
        try {
          assert.equal(true, typeof a.uploadCaptivePortalToVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCaptivePortalFromVD - errors', () => {
      it('should have a deleteCaptivePortalFromVD function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCaptivePortalFromVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSFilterTypes - errors', () => {
      it('should have a getIPSFilterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getIPSFilterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filterpath', (done) => {
        try {
          a.getIPSFilterTypes(null, null, (data, error) => {
            try {
              const displayE = 'filterpath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPSFilterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgUuid', (done) => {
        try {
          a.getIPSFilterTypes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orgUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPSFilterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadIPSVulnerabilityRuleFile - errors', () => {
      it('should have a uploadIPSVulnerabilityRuleFile function', (done) => {
        try {
          assert.equal(true, typeof a.uploadIPSVulnerabilityRuleFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListOfIPSUnzippedRuleFiles - errors', () => {
      it('should have a getListOfIPSUnzippedRuleFiles function', (done) => {
        try {
          assert.equal(true, typeof a.getListOfIPSUnzippedRuleFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadIPSCustomRuleFileToAppliance - errors', () => {
      it('should have a uploadIPSCustomRuleFileToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.uploadIPSCustomRuleFileToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceIPSRuleFile - errors', () => {
      it('should have a deleteApplianceIPSRuleFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplianceIPSRuleFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configureIPSVulnerabilityRuleFiles - errors', () => {
      it('should have a configureIPSVulnerabilityRuleFiles function', (done) => {
        try {
          assert.equal(true, typeof a.configureIPSVulnerabilityRuleFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDIPSRuleFile - errors', () => {
      it('should have a deleteVDIPSRuleFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVDIPSRuleFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSSignatures - errors', () => {
      it('should have a getIPSSignatures function', (done) => {
        try {
          assert.equal(true, typeof a.getIPSSignatures === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getIPSSignatures('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPSSignatures', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getIPSSignatures('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPSSignatures', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgUuid', (done) => {
        try {
          a.getIPSSignatures('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'orgUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPSSignatures', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadKeyTabToAppliance - errors', () => {
      it('should have a uploadKeyTabToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.uploadKeyTabToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyTabFromAppliance - errors', () => {
      it('should have a deleteKeyTabFromAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKeyTabFromAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadKeyTabToVD - errors', () => {
      it('should have a uploadKeyTabToVD function', (done) => {
        try {
          assert.equal(true, typeof a.uploadKeyTabToVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyTabFromVD - errors', () => {
      it('should have a deleteKeyTabFromVD function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKeyTabFromVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadPACToAppliance - errors', () => {
      it('should have a uploadPACToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.uploadPACToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePACFromAppliance - errors', () => {
      it('should have a deletePACFromAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.deletePACFromAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadPACToVD - errors', () => {
      it('should have a uploadPACToVD function', (done) => {
        try {
          assert.equal(true, typeof a.uploadPACToVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePACFromVD - errors', () => {
      it('should have a deletePACFromVD function', (done) => {
        try {
          assert.equal(true, typeof a.deletePACFromVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceSecurityFileInfo - errors', () => {
      it('should have a getApplianceSecurityFileInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceSecurityFileInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getApplianceSecurityFileInfo('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceSecurityFileInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadSecuityFileToAppliance - errors', () => {
      it('should have a uploadSecuityFileToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.uploadSecuityFileToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.uploadSecuityFileToAppliance('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadSecuityFileToAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityFileFromAppliance - errors', () => {
      it('should have a deleteSecurityFileFromAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityFileFromAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.deleteSecurityFileFromAppliance('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteSecurityFileFromAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDSecurityFileInfo - errors', () => {
      it('should have a getVDSecurityFileInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getVDSecurityFileInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgUuid', (done) => {
        try {
          a.getVDSecurityFileInfo(null, null, (data, error) => {
            try {
              const displayE = 'orgUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVDSecurityFileInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getVDSecurityFileInfo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVDSecurityFileInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadSecurityFileToVD - errors', () => {
      it('should have a uploadSecurityFileToVD function', (done) => {
        try {
          assert.equal(true, typeof a.uploadSecurityFileToVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.uploadSecurityFileToVD('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadSecurityFileToVD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityFileFromVD - errors', () => {
      it('should have a deleteSecurityFileFromVD function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityFileFromVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.deleteSecurityFileFromVD('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteSecurityFileFromVD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCertificateToAppliance - errors', () => {
      it('should have a uploadCertificateToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.uploadCertificateToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceCertificate - errors', () => {
      it('should have a deleteApplianceCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplianceCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCertificateToVD - errors', () => {
      it('should have a uploadCertificateToVD function', (done) => {
        try {
          assert.equal(true, typeof a.uploadCertificateToVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDCertificate - errors', () => {
      it('should have a deleteVDCertificate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVDCertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findActiveLoginUsers - errors', () => {
      it('should have a findActiveLoginUsers function', (done) => {
        try {
          assert.equal(true, typeof a.findActiveLoginUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportAllUsers - errors', () => {
      it('should have a exportAllUsers function', (done) => {
        try {
          assert.equal(true, typeof a.exportAllUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findAllLockedUsers - errors', () => {
      it('should have a findAllLockedUsers function', (done) => {
        try {
          assert.equal(true, typeof a.findAllLockedUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPasswordScore - errors', () => {
      it('should have a getPasswordScore function', (done) => {
        try {
          assert.equal(true, typeof a.getPasswordScore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.getPasswordScore(null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPasswordScore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlockUsersAccount - errors', () => {
      it('should have a unlockUsersAccount function', (done) => {
        try {
          assert.equal(true, typeof a.unlockUsersAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usernameList', (done) => {
        try {
          a.unlockUsersAccount(null, (data, error) => {
            try {
              const displayE = 'usernameList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-unlockUsersAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUserById - errors', () => {
      it('should have a findUserById function', (done) => {
        try {
          assert.equal(true, typeof a.findUserById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.findUserById(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-findUserById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAccountLocked - errors', () => {
      it('should have a userAccountLocked function', (done) => {
        try {
          assert.equal(true, typeof a.userAccountLocked === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.userAccountLocked(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-userAccountLocked', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lockUserAccount - errors', () => {
      it('should have a lockUserAccount function', (done) => {
        try {
          assert.equal(true, typeof a.lockUserAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.lockUserAccount(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-lockUserAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlockUserAccount - errors', () => {
      it('should have a unlockUserAccount function', (done) => {
        try {
          assert.equal(true, typeof a.unlockUserAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.unlockUserAccount(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-unlockUserAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserGlobalSettings - errors', () => {
      it('should have a getUserGlobalSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getUserGlobalSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveUserGlobalSettings - errors', () => {
      it('should have a saveUserGlobalSettings function', (done) => {
        try {
          assert.equal(true, typeof a.saveUserGlobalSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSettings', (done) => {
        try {
          a.saveUserGlobalSettings(null, (data, error) => {
            try {
              const displayE = 'userSettings is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-saveUserGlobalSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCommonFile - errors', () => {
      it('should have a downloadCommonFile function', (done) => {
        try {
          assert.equal(true, typeof a.downloadCommonFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.downloadCommonFile(null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-downloadCommonFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.downloadCommonFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-downloadCommonFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCommonFileToVD - errors', () => {
      it('should have a uploadCommonFileToVD function', (done) => {
        try {
          assert.equal(true, typeof a.uploadCommonFileToVD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing file', (done) => {
        try {
          a.uploadCommonFileToVD(null, null, null, (data, error) => {
            try {
              const displayE = 'file is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadCommonFileToVD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing model', (done) => {
        try {
          a.uploadCommonFileToVD('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'model is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadCommonFileToVD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.uploadCommonFileToVD('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-uploadCommonFileToVD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommonFileMiniSummary - errors', () => {
      it('should have a getCommonFileMiniSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getCommonFileMiniSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getCommonFileMiniSummary(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getCommonFileMiniSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommonFilesByType - errors', () => {
      it('should have a getAllCommonFilesByType function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCommonFilesByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAllCommonFilesByType(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllCommonFilesByType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommonFilesByMultipleTypes - errors', () => {
      it('should have a getAllCommonFilesByMultipleTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCommonFilesByMultipleTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getAllCommonFilesByMultipleTypes(null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllCommonFilesByMultipleTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getAllCommonFilesByMultipleTypes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllCommonFilesByMultipleTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCommonFileMeta - errors', () => {
      it('should have a updateCommonFileMeta function', (done) => {
        try {
          assert.equal(true, typeof a.updateCommonFileMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileMetaData', (done) => {
        try {
          a.updateCommonFileMeta(null, null, null, (data, error) => {
            try {
              const displayE = 'fileMetaData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateCommonFileMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.updateCommonFileMeta('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateCommonFileMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.updateCommonFileMeta('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateCommonFileMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCommonFile - errors', () => {
      it('should have a deleteCommonFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCommonFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.deleteCommonFile(null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteCommonFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.deleteCommonFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteCommonFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerClientByAdmin - errors', () => {
      it('should have a registerClientByAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.registerClientByAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientRegnRequest', (done) => {
        try {
          a.registerClientByAdmin(null, (data, error) => {
            try {
              const displayE = 'clientRegnRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-registerClientByAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateClientByAdmin - errors', () => {
      it('should have a updateClientByAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.updateClientByAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientRegnRequest', (done) => {
        try {
          a.updateClientByAdmin(null, null, (data, error) => {
            try {
              const displayE = 'clientRegnRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateClientByAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateClientByAdmin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateClientByAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClientByClient - errors', () => {
      it('should have a getClientByClient function', (done) => {
        try {
          assert.equal(true, typeof a.getClientByClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getClientByClient(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getClientByClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateClient - errors', () => {
      it('should have a updateClient function', (done) => {
        try {
          assert.equal(true, typeof a.updateClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientUpdateRequest', (done) => {
        try {
          a.updateClient(null, null, (data, error) => {
            try {
              const displayE = 'clientUpdateRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateClient('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClientByClient - errors', () => {
      it('should have a deleteClientByClient function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClientByClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteClientByClient(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteClientByClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshClientSecrect - errors', () => {
      it('should have a refreshClientSecrect function', (done) => {
        try {
          assert.equal(true, typeof a.refreshClientSecrect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.refreshClientSecrect(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-refreshClientSecrect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExternalOAuthTokenServer - errors', () => {
      it('should have a updateExternalOAuthTokenServer function', (done) => {
        try {
          assert.equal(true, typeof a.updateExternalOAuthTokenServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing email', (done) => {
        try {
          a.updateExternalOAuthTokenServer(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'email is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateExternalOAuthTokenServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing org', (done) => {
        try {
          a.updateExternalOAuthTokenServer('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'org is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateExternalOAuthTokenServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.updateExternalOAuthTokenServer('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateExternalOAuthTokenServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshAccessToken - errors', () => {
      it('should have a refreshAccessToken function', (done) => {
        try {
          assert.equal(true, typeof a.refreshAccessToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userRefreshTokenRequest', (done) => {
        try {
          a.refreshAccessToken(null, (data, error) => {
            try {
              const displayE = 'userRefreshTokenRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-refreshAccessToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeToken - errors', () => {
      it('should have a revokeToken function', (done) => {
        try {
          assert.equal(true, typeof a.revokeToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateUserEMAILSecureCode - errors', () => {
      it('should have a generateUserEMAILSecureCode function', (done) => {
        try {
          assert.equal(true, typeof a.generateUserEMAILSecureCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.generateUserEMAILSecureCode(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateUserEMAILSecureCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateUserSMSSecureCode - errors', () => {
      it('should have a generateUserSMSSecureCode function', (done) => {
        try {
          assert.equal(true, typeof a.generateUserSMSSecureCode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.generateUserSMSSecureCode(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-generateUserSMSSecureCode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyUserSecureCodeAndRegisterAsync - errors', () => {
      it('should have a verifyUserSecureCodeAndRegisterAsync function', (done) => {
        try {
          assert.equal(true, typeof a.verifyUserSecureCodeAndRegisterAsync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securecode', (done) => {
        try {
          a.verifyUserSecureCodeAndRegisterAsync(null, null, (data, error) => {
            try {
              const displayE = 'securecode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-verifyUserSecureCodeAndRegisterAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.verifyUserSecureCodeAndRegisterAsync('fakeparam', null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-verifyUserSecureCodeAndRegisterAsync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVendors - errors', () => {
      it('should have a getAllVendors function', (done) => {
        try {
          assert.equal(true, typeof a.getAllVendors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVendorProductType - errors', () => {
      it('should have a addVendorProductType function', (done) => {
        try {
          assert.equal(true, typeof a.addVendorProductType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.addVendorProductType(null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addVendorProductType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorProductType', (done) => {
        try {
          a.addVendorProductType('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendorProductType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addVendorProductType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVendorProductType - errors', () => {
      it('should have a deleteVendorProductType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVendorProductType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.deleteVendorProductType(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteVendorProductType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDefinedVendors - errors', () => {
      it('should have a getUserDefinedVendors function', (done) => {
        try {
          assert.equal(true, typeof a.getUserDefinedVendors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addVendor - errors', () => {
      it('should have a addVendor function', (done) => {
        try {
          assert.equal(true, typeof a.addVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendor', (done) => {
        try {
          a.addVendor(null, (data, error) => {
            try {
              const displayE = 'vendor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchUserDefinedVendor - errors', () => {
      it('should have a searchUserDefinedVendor function', (done) => {
        try {
          assert.equal(true, typeof a.searchUserDefinedVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchStr', (done) => {
        try {
          a.searchUserDefinedVendor(null, (data, error) => {
            try {
              const displayE = 'searchStr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-searchUserDefinedVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendors - errors', () => {
      it('should have a getVendors function', (done) => {
        try {
          assert.equal(true, typeof a.getVendors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVendor - errors', () => {
      it('should have a deleteVendor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.deleteVendor(null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableOrganizationIds - errors', () => {
      it('should have a getAvailableOrganizationIds function', (done) => {
        try {
          assert.equal(true, typeof a.getAvailableOrganizationIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateMetadata - errors', () => {
      it('should have a getTemplateMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getTemplateMetadata(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getTemplateMetadata('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getTemplateMetadata('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getTemplateMetadata('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addServiceTemplate - errors', () => {
      it('should have a addServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.addServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addServiceTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportServiceTemplate - errors', () => {
      it('should have a exportServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.exportServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.exportServiceTemplate(null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-exportServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneMasterTemplate - errors', () => {
      it('should have a cloneMasterTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.cloneMasterTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.cloneMasterTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-cloneMasterTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTemplateOrgLANZone - errors', () => {
      it('should have a createTemplateOrgLANZone function', (done) => {
        try {
          assert.equal(true, typeof a.createTemplateOrgLANZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newOrgName', (done) => {
        try {
          a.createTemplateOrgLANZone(null, null, (data, error) => {
            try {
              const displayE = 'newOrgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateOrgLANZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTemplateOrgLANZone('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateOrgLANZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importaServiceTemplate - errors', () => {
      it('should have a importaServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.importaServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importaServiceTemplate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importaServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExistingSpokeGroups - errors', () => {
      it('should have a getExistingSpokeGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getExistingSpokeGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getExistingSpokeGroups(null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getExistingSpokeGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getExistingSpokeGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getExistingSpokeGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceList - errors', () => {
      it('should have a getApplianceList function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offset', (done) => {
        try {
          a.getApplianceList(null, null, (data, error) => {
            try {
              const displayE = 'offset is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getApplianceList('fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAvailableIdWithSerial - errors', () => {
      it('should have a getSDWANAvailableIdWithSerial function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANAvailableIdWithSerial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAvailableVRFIds - errors', () => {
      it('should have a getSDWANAvailableVRFIds function', (done) => {
        try {
          assert.equal(true, typeof a.getSDWANAvailableVRFIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing count', (done) => {
        try {
          a.getSDWANAvailableVRFIds(null, (data, error) => {
            try {
              const displayE = 'count is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSDWANAvailableVRFIds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLatitudeLongitudeofAddress - errors', () => {
      it('should have a getLatitudeLongitudeofAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getLatitudeLongitudeofAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing channel', (done) => {
        try {
          a.getLatitudeLongitudeofAddress(null, null, null, null, (data, error) => {
            try {
              const displayE = 'channel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getLatitudeLongitudeofAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.getLatitudeLongitudeofAddress('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getLatitudeLongitudeofAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address', (done) => {
        try {
          a.getLatitudeLongitudeofAddress('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'address is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getLatitudeLongitudeofAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getLatitudeLongitudeofAddress('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getLatitudeLongitudeofAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgApplianceUUID - errors', () => {
      it('should have a getOrgApplianceUUID function', (done) => {
        try {
          assert.equal(true, typeof a.getOrgApplianceUUID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationWANNetworks - errors', () => {
      it('should have a getOrganizationWANNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationWANNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newOrgUuid', (done) => {
        try {
          a.getOrganizationWANNetworks(null, (data, error) => {
            try {
              const displayE = 'newOrgUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrganizationWANNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationWANNetwork - errors', () => {
      it('should have a createOrganizationWANNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganizationWANNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newOrgUuid', (done) => {
        try {
          a.createOrganizationWANNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'newOrgUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createOrganizationWANNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newNwName', (done) => {
        try {
          a.createOrganizationWANNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'newNwName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createOrganizationWANNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createOrganizationWANNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createOrganizationWANNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get - errors', () => {
      it('should have a get function', (done) => {
        try {
          assert.equal(true, typeof a.get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkCommunityGroup - errors', () => {
      it('should have a getBulkCommunityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getBulkCommunityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSolutionTieronsetofDevices - errors', () => {
      it('should have a updateSolutionTieronsetofDevices function', (done) => {
        try {
          assert.equal(true, typeof a.updateSolutionTieronsetofDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSolutionTieronsetofDevices(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateSolutionTieronsetofDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterface - errors', () => {
      it('should have a getInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getInterface(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceDetails - errors', () => {
      it('should have a getInterfaceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getInterfaceDetails(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getInterfaceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSecTunnelInterfaces - errors', () => {
      it('should have a getIPSecTunnelInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getIPSecTunnelInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getIPSecTunnelInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPSecTunnelInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalTunnelInterfaces - errors', () => {
      it('should have a getLogicalTunnelInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getLogicalTunnelInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getLogicalTunnelInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getLogicalTunnelInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalInterfaces - errors', () => {
      it('should have a getPhysicalInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getPhysicalInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getPhysicalInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaTunnelVirtualInterfaces - errors', () => {
      it('should have a getVersaTunnelVirtualInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getVersaTunnelVirtualInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getVersaTunnelVirtualInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVersaTunnelVirtualInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaPseudoTunnelVirtualInterfaces - errors', () => {
      it('should have a getVersaPseudoTunnelVirtualInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getVersaPseudoTunnelVirtualInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getVersaPseudoTunnelVirtualInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVersaPseudoTunnelVirtualInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaWlanInterface - errors', () => {
      it('should have a getVersaWlanInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getVersaWlanInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getVersaWlanInterface(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVersaWlanInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaWwanInterface - errors', () => {
      it('should have a getVersaWwanInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getVersaWwanInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getVersaWwanInterface(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getVersaWwanInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPInformationforActiveInterfaces - errors', () => {
      it('should have a getIPInformationforActiveInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getIPInformationforActiveInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getIPInformationforActiveInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPInformationforActiveInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPInformationforDynamicInterfaces - errors', () => {
      it('should have a getIPInformationforDynamicInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getIPInformationforDynamicInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getIPInformationforDynamicInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getIPInformationforDynamicInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficonPhysicalInterfaces - errors', () => {
      it('should have a getTrafficonPhysicalInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getTrafficonPhysicalInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getTrafficonPhysicalInterfaces(null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTrafficonPhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getTrafficonPhysicalInterfaces('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTrafficonPhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficonAllInterfaces - errors', () => {
      it('should have a getTrafficonAllInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getTrafficonAllInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getTrafficonAllInterfaces(null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTrafficonAllInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficonSpecificInterfaces - errors', () => {
      it('should have a getTrafficonSpecificInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getTrafficonSpecificInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getTrafficonSpecificInterfaces(null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTrafficonSpecificInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceParam', (done) => {
        try {
          a.getTrafficonSpecificInterfaces('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTrafficonSpecificInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAmetricsforAppliance - errors', () => {
      it('should have a getSLAmetricsforAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAmetricsforAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getSLAmetricsforAppliance(null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSLAmetricsforAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAmetricsforApplianceFilter - errors', () => {
      it('should have a getSLAmetricsforApplianceFilter function', (done) => {
        try {
          assert.equal(true, typeof a.getSLAmetricsforApplianceFilter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getSLAmetricsforApplianceFilter(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getSLAmetricsforApplianceFilter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSDWANPoliciesofanAppliance - errors', () => {
      it('should have a getAllSDWANPoliciesofanAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSDWANPoliciesofanAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getAllSDWANPoliciesofanAppliance(null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllSDWANPoliciesofanAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgid', (done) => {
        try {
          a.getAllSDWANPoliciesofanAppliance('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllSDWANPoliciesofanAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing poligyGroup', (done) => {
        try {
          a.getAllSDWANPoliciesofanAppliance('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'poligyGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAllSDWANPoliciesofanAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAvailableLicenseTiers - errors', () => {
      it('should have a getAllAvailableLicenseTiers function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAvailableLicenseTiers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDHCPOptionsProfile - errors', () => {
      it('should have a getOrganizationDHCPOptionsProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationDHCPOptionsProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getOrganizationDHCPOptionsProfile(null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrganizationDHCPOptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.getOrganizationDHCPOptionsProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrganizationDHCPOptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationZones - errors', () => {
      it('should have a getOrganizationZones function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getOrganizationZones(null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrganizationZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.getOrganizationZones('fakeparam', null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOrganizationZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePhysicalInterfaces - errors', () => {
      it('should have a deletePhysicalInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.deletePhysicalInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePhysicalInterfaces(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vniInterfaceName', (done) => {
        try {
          a.deletePhysicalInterfaces('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vniInterfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateCgnatPool - errors', () => {
      it('should have a addPostStagingTemplateCgnatPool function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateCgnatPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateCgnatPool(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateCgnatPool('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing unhide', (done) => {
        try {
          a.addPostStagingTemplateCgnatPool('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'unhide is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateCgnatPool('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateNetworks - errors', () => {
      it('should have a deletePostStagingTemplateNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateNetworks(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkName', (done) => {
        try {
          a.deletePostStagingTemplateNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateCgnatPools - errors', () => {
      it('should have a deletePostStagingTemplateCgnatPools function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateCgnatPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateCgnatPools(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateCgnatPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateCgnatPools('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateCgnatPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cgnatPool', (done) => {
        try {
          a.deletePostStagingTemplateCgnatPools('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'cgnatPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateCgnatPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateCgnatRule - errors', () => {
      it('should have a addPostStagingTemplateCgnatRule function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateCgnatRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateCgnatRule(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateCgnatRule('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing unhide', (done) => {
        try {
          a.addPostStagingTemplateCgnatRule('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'unhide is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateCgnatRule('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateCgnatRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateCgnatRules - errors', () => {
      it('should have a deletePostStagingTemplateCgnatRules function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateCgnatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateCgnatRules(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateCgnatRules('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cgnatRule', (done) => {
        try {
          a.deletePostStagingTemplateCgnatRules('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'cgnatRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClassOfServiceInterfaceNetworkAssociation - errors', () => {
      it('should have a deleteClassOfServiceInterfaceNetworkAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClassOfServiceInterfaceNetworkAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deleteClassOfServiceInterfaceNetworkAssociation(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteClassOfServiceInterfaceNetworkAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deleteClassOfServiceInterfaceNetworkAssociation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteClassOfServiceInterfaceNetworkAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.deleteClassOfServiceInterfaceNetworkAssociation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteClassOfServiceInterfaceNetworkAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateDhcpAddressPool - errors', () => {
      it('should have a addPostStagingTemplateDhcpAddressPool function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateDhcpAddressPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateDhcpAddressPool(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpAddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateDhcpAddressPool('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpAddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateDhcpAddressPool('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpAddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpAddressPools - errors', () => {
      it('should have a deletePostStagingTemplateDhcpAddressPools function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcpAddressPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcpAddressPools(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpAddressPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateDhcpAddressPools('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpAddressPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpPoolName', (done) => {
        try {
          a.deletePostStagingTemplateDhcpAddressPools('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'dhcpPoolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpAddressPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateDhcpLeaseProfile - errors', () => {
      it('should have a addPostStagingTemplateDhcpLeaseProfile function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateDhcpLeaseProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateDhcpLeaseProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpLeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateDhcpLeaseProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpLeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateDhcpLeaseProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpLeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpLeaseProfiles - errors', () => {
      it('should have a deletePostStagingTemplateDhcpLeaseProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcpLeaseProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcpLeaseProfiles(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpLeaseProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateDhcpLeaseProfiles('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpLeaseProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpLeaseProfileName', (done) => {
        try {
          a.deletePostStagingTemplateDhcpLeaseProfiles('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'dhcpLeaseProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpLeaseProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateDhcpOptionsProfile - errors', () => {
      it('should have a addPostStagingTemplateDhcpOptionsProfile function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateDhcpOptionsProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateDhcpOptionsProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpOptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateDhcpOptionsProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpOptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateDhcpOptionsProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpOptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpOptionsProfiles - errors', () => {
      it('should have a deletePostStagingTemplateDhcpOptionsProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcpOptionsProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcpOptionsProfiles(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpOptionsProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateDhcpOptionsProfiles('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpOptionsProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpOptionProfileName', (done) => {
        try {
          a.deletePostStagingTemplateDhcpOptionsProfiles('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'dhcpOptionProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpOptionsProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateDhcpServer - errors', () => {
      it('should have a addPostStagingTemplateDhcpServer function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateDhcpServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateDhcpServer(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateDhcpServer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateDhcpServer('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateDhcpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpServers - errors', () => {
      it('should have a deletePostStagingTemplateDhcpServers function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcpServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcpServers(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateDhcpServers('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpServerProfile', (done) => {
        try {
          a.deletePostStagingTemplateDhcpServers('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'dhcpServerProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcpServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateZone - errors', () => {
      it('should have a addPostStagingTemplateZone function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateZone(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateZone('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateZone('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateOrgLANZone - errors', () => {
      it('should have a deleteTemplateOrgLANZone function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateOrgLANZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deleteTemplateOrgLANZone(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateOrgLANZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deleteTemplateOrgLANZone('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateOrgLANZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneName', (done) => {
        try {
          a.deleteTemplateOrgLANZone('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'zoneName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateOrgLANZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateStatefulFirewallAccessPolicyRule - errors', () => {
      it('should have a addPostStagingTemplateStatefulFirewallAccessPolicyRule function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateStatefulFirewallAccessPolicyRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallAccessPolicyRule(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateStatefulFirewallAccessPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallAccessPolicyRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateStatefulFirewallAccessPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallAccessPolicyRule('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateStatefulFirewallAccessPolicyRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateStatefulFirewallSecurityRules - errors', () => {
      it('should have a deletePostStagingTemplateStatefulFirewallSecurityRules function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateStatefulFirewallSecurityRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallSecurityRules(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateStatefulFirewallSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallSecurityRules('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateStatefulFirewallSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityAccessPolicy', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallSecurityRules('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'securityAccessPolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateStatefulFirewallSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateStatefulFirewallSecurityRules - errors', () => {
      it('should have a addPostStagingTemplateStatefulFirewallSecurityRules function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateStatefulFirewallSecurityRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallSecurityRules(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateStatefulFirewallSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallSecurityRules('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateStatefulFirewallSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityAccessPolicy', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallSecurityRules('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'securityAccessPolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateStatefulFirewallSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallSecurityRules('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateStatefulFirewallSecurityRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateStatefulFirewallAccessPolicyRules - errors', () => {
      it('should have a deletePostStagingTemplateStatefulFirewallAccessPolicyRules function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateStatefulFirewallAccessPolicyRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallAccessPolicyRules(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateStatefulFirewallAccessPolicyRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgname', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallAccessPolicyRules('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'orgname is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateStatefulFirewallAccessPolicyRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityAccessPolicy', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallAccessPolicyRules('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'securityAccessPolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateStatefulFirewallAccessPolicyRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityAccessPolicyRule', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallAccessPolicyRules('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'securityAccessPolicyRule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateStatefulFirewallAccessPolicyRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateVrfs - errors', () => {
      it('should have a getPostStagingTemplateVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateVrfs(null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateVrf - errors', () => {
      it('should have a updatePostStagingTemplateVrf function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateVrf === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateVrf(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateVrf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routingInstanceName', (done) => {
        try {
          a.updatePostStagingTemplateVrf('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routingInstanceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateVrf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateVrf('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateVrf', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importServiceTemplate - errors', () => {
      it('should have a importServiceTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.importServiceTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.importServiceTemplate(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bodyFormData', (done) => {
        try {
          a.importServiceTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bodyFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importServiceTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importTemplateString - errors', () => {
      it('should have a importTemplateString function', (done) => {
        try {
          assert.equal(true, typeof a.importTemplateString === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.importTemplateString(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importTemplateString', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importTemplateString('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-importTemplateString', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePhysicalInterfaces - errors', () => {
      it('should have a updatePhysicalInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.updatePhysicalInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePhysicalInterfaces(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vni', (done) => {
        try {
          a.updatePhysicalInterfaces('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vni is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePhysicalInterfaces('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePhysicalInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateNetworkInterface - errors', () => {
      it('should have a getPostStagingTemplateNetworkInterface function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateNetworkInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateNetworkInterface(null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateNetworkInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateNetworkInterface - errors', () => {
      it('should have a addPostStagingTemplateNetworkInterface function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateNetworkInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateNetworkInterface(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateNetworkInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateNetworkInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateNetworkInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateCgnatRules - errors', () => {
      it('should have a getPostStagingTemplateCgnatRules function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateCgnatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateCgnatRules(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getPostStagingTemplateCgnatRules('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateCgnatRules - errors', () => {
      it('should have a updatePostStagingTemplateCgnatRules function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateCgnatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateCgnatRules(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updatePostStagingTemplateCgnatRules('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rule', (done) => {
        try {
          a.updatePostStagingTemplateCgnatRules('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'rule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateCgnatRules('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateCgnatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should have a getPostStagingTemplateDhcp6LeaseProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateDhcp6LeaseProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateDhcp6LeaseProfile(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getPostStagingTemplateDhcp6LeaseProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should have a createPostStagingTemplateDhcp6LeaseProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createPostStagingTemplateDhcp6LeaseProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.createPostStagingTemplateDhcp6LeaseProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.createPostStagingTemplateDhcp6LeaseProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPostStagingTemplateDhcp6LeaseProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should have a updatePostStagingTemplateDhcp6LeaseProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateDhcp6LeaseProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6LeaseProfile(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6LeaseProfile('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing leaseProfile', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6LeaseProfile('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'leaseProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6LeaseProfile('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should have a deletePostStagingTemplateDhcp6LeaseProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcp6LeaseProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6LeaseProfile(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6LeaseProfile('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing leaseProfile', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6LeaseProfile('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'leaseProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6LeaseProfile('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6LeaseProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should have a getPostStagingTemplateDhcp6OptionsProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateDhcp6OptionsProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateDhcp6OptionsProfile(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getPostStagingTemplateDhcp6OptionsProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should have a createPostStagingTemplateDhcp6OptionsProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createPostStagingTemplateDhcp6OptionsProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.createPostStagingTemplateDhcp6OptionsProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.createPostStagingTemplateDhcp6OptionsProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPostStagingTemplateDhcp6OptionsProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should have a updatePostStagingTemplateDhcp6OptionsProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateDhcp6OptionsProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6OptionsProfile(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6OptionsProfile('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionsProfile', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6OptionsProfile('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'optionsProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6OptionsProfile('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should have a deletePostStagingTemplateDhcp6OptionsProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcp6OptionsProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6OptionsProfile(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6OptionsProfile('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionsProfile', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6OptionsProfile('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'optionsProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6OptionsProfile('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6OptionsProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should have a getPostStagingTemplateDhcp6AddressPool function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateDhcp6AddressPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateDhcp6AddressPool(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getPostStagingTemplateDhcp6AddressPool('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should have a createPostStagingTemplateDhcp6AddressPool function', (done) => {
        try {
          assert.equal(true, typeof a.createPostStagingTemplateDhcp6AddressPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.createPostStagingTemplateDhcp6AddressPool(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.createPostStagingTemplateDhcp6AddressPool('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPostStagingTemplateDhcp6AddressPool('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should have a updatePostStagingTemplateDhcp6AddressPool function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateDhcp6AddressPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6AddressPool(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6AddressPool('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dynamicPool', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6AddressPool('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dynamicPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6AddressPool('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should have a deletePostStagingTemplateDhcp6AddressPool function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcp6AddressPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6AddressPool(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6AddressPool('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dynamicPool', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6AddressPool('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dynamicPool is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6AddressPool('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6AddressPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6Server - errors', () => {
      it('should have a getPostStagingTemplateDhcp6Server function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateDhcp6Server === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateDhcp6Server(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getPostStagingTemplateDhcp6Server('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPostStagingTemplateDhcp6Server - errors', () => {
      it('should have a createPostStagingTemplateDhcp6Server function', (done) => {
        try {
          assert.equal(true, typeof a.createPostStagingTemplateDhcp6Server === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.createPostStagingTemplateDhcp6Server(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.createPostStagingTemplateDhcp6Server('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPostStagingTemplateDhcp6Server('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createPostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateDhcp6Server - errors', () => {
      it('should have a updatePostStagingTemplateDhcp6Server function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateDhcp6Server === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6Server(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6Server('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceProfile', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6Server('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6Server('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcp6Server - errors', () => {
      it('should have a deletePostStagingTemplateDhcp6Server function', (done) => {
        try {
          assert.equal(true, typeof a.deletePostStagingTemplateDhcp6Server === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6Server(null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6Server('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceProfile', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6Server('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6Server('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deletePostStagingTemplateDhcp6Server', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateServiceNodeGroup - errors', () => {
      it('should have a updatePostStagingTemplateServiceNodeGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateServiceNodeGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateServiceNodeGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateServiceNodeGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceNodeGroup', (done) => {
        try {
          a.updatePostStagingTemplateServiceNodeGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceNodeGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateServiceNodeGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateServiceNodeGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateServiceNodeGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addPostStagingTemplateNetwork - errors', () => {
      it('should have a addPostStagingTemplateNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.addPostStagingTemplateNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.addPostStagingTemplateNetwork(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addPostStagingTemplateNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-addPostStagingTemplateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateOrgLimits - errors', () => {
      it('should have a getPostStagingTemplateOrgLimits function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateOrgLimits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateOrgLimits(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateOrgLimits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getPostStagingTemplateOrgLimits('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateOrgLimits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateOrgLimits - errors', () => {
      it('should have a updatePostStagingTemplateOrgLimits function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateOrgLimits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateOrgLimits(null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateOrgLimits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updatePostStagingTemplateOrgLimits('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateOrgLimits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateOrgLimits('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateOrgLimits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateIpsecVpnProfile - errors', () => {
      it('should have a getPostStagingTemplateIpsecVpnProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getPostStagingTemplateIpsecVpnProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.getPostStagingTemplateIpsecVpnProfile(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateIpsecVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getPostStagingTemplateIpsecVpnProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPostStagingTemplateIpsecVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePostStagingTemplateIpsecVpnProfile - errors', () => {
      it('should have a updatePostStagingTemplateIpsecVpnProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updatePostStagingTemplateIpsecVpnProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.updatePostStagingTemplateIpsecVpnProfile(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateIpsecVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.updatePostStagingTemplateIpsecVpnProfile('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateIpsecVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnProfile', (done) => {
        try {
          a.updatePostStagingTemplateIpsecVpnProfile('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vpnProfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateIpsecVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePostStagingTemplateIpsecVpnProfile('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updatePostStagingTemplateIpsecVpnProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplianceFileObject - errors', () => {
      it('should have a createApplianceFileObject function', (done) => {
        try {
          assert.equal(true, typeof a.createApplianceFileObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplianceFileObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createApplianceFileObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceFileObject - errors', () => {
      it('should have a deleteApplianceFileObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplianceFileObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.deleteApplianceFileObject(null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteApplianceFileObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editApplianceFileObject - errors', () => {
      it('should have a editApplianceFileObject function', (done) => {
        try {
          assert.equal(true, typeof a.editApplianceFileObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.editApplianceFileObject(null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editApplianceFileObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editApplianceFileObject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editApplianceFileObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editAppliaceConfiguration - errors', () => {
      it('should have a editAppliaceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.editAppliaceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editAppliaceConfiguration(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editAppliaceConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliaceConfiguration - errors', () => {
      it('should have a getAppliaceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliaceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceFileObject - errors', () => {
      it('should have a getApplianceFileObject function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceFileObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceById - errors', () => {
      it('should have a getApplianceById function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceUuid', (done) => {
        try {
          a.getApplianceById(null, (data, error) => {
            try {
              const displayE = 'applianceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getApplianceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceNamesAndUuids - errors', () => {
      it('should have a getApplianceNamesAndUuids function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceNamesAndUuids === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRouteObject - errors', () => {
      it('should have a createRouteObject function', (done) => {
        try {
          assert.equal(true, typeof a.createRouteObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRouteObject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouteObject - errors', () => {
      it('should have a deleteRouteObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouteObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationPrefix', (done) => {
        try {
          a.deleteRouteObject(null, null, null, (data, error) => {
            try {
              const displayE = 'destinationPrefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nextHopAddress', (done) => {
        try {
          a.deleteRouteObject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nextHopAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing outgoingInterface', (done) => {
        try {
          a.deleteRouteObject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'outgoingInterface is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editRouteObject - errors', () => {
      it('should have a editRouteObject function', (done) => {
        try {
          assert.equal(true, typeof a.editRouteObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationPrefix', (done) => {
        try {
          a.editRouteObject(null, null, null, null, (data, error) => {
            try {
              const displayE = 'destinationPrefix is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nextHopAddress', (done) => {
        try {
          a.editRouteObject('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nextHopAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing outgoingInterface', (done) => {
        try {
          a.editRouteObject('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'outgoingInterface is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editRouteObject('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editRouteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteObjectList - errors', () => {
      it('should have a getRouteObjectList function', (done) => {
        try {
          assert.equal(true, typeof a.getRouteObjectList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseAlarmsSummary - errors', () => {
      it('should have a getLicenseAlarmsSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseAlarmsSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkAlarms - errors', () => {
      it('should have a createBulkAlarms function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkAlarms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkAlarms(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createBulkAlarms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBulkAlarmByUser - errors', () => {
      it('should have a createBulkAlarmByUser function', (done) => {
        try {
          assert.equal(true, typeof a.createBulkAlarmByUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBulkAlarmByUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createBulkAlarmByUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmsByAppliance - errors', () => {
      it('should have a getAlarmsByAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmsByAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getAlarmsByAppliance(null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAlarmsByAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmsSummary - errors', () => {
      it('should have a getAlarmsSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getAlarmsSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateBulkAlarm - errors', () => {
      it('should have a updateBulkAlarm function', (done) => {
        try {
          assert.equal(true, typeof a.updateBulkAlarm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateBulkAlarm(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateBulkAlarm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTemplateDevices - errors', () => {
      it('should have a createTemplateDevices function', (done) => {
        try {
          assert.equal(true, typeof a.createTemplateDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTemplateDevices(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTemplateAttributes - errors', () => {
      it('should have a createTemplateAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.createTemplateAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.createTemplateAttributes(null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.createTemplateAttributes('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTemplateAttributes('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTemplateTunnel - errors', () => {
      it('should have a createTemplateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.createTemplateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedDevice', (done) => {
        try {
          a.createTemplateTunnel(null, null, (data, error) => {
            try {
              const displayE = 'managedDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTemplateTunnel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-createTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateDeviceGroups - errors', () => {
      it('should have a deleteTemplateDeviceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateDeviceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.deleteTemplateDeviceGroups(null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateDeviceGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTemplateDeviceGroups - errors', () => {
      it('should have a editTemplateDeviceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.editTemplateDeviceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.editTemplateDeviceGroups(null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateDeviceGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTemplateDeviceGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateDeviceGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateDeviceTemplates - errors', () => {
      it('should have a deleteTemplateDeviceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateDeviceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.deleteTemplateDeviceTemplates(null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateDeviceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTemplateDeviceTemplates - errors', () => {
      it('should have a editTemplateDeviceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.editTemplateDeviceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateName', (done) => {
        try {
          a.editTemplateDeviceTemplates(null, null, (data, error) => {
            try {
              const displayE = 'templateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateDeviceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTemplateDeviceTemplates('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateDeviceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateManagedDevices - errors', () => {
      it('should have a deleteTemplateManagedDevices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateManagedDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedDevice', (done) => {
        try {
          a.deleteTemplateManagedDevices(null, (data, error) => {
            try {
              const displayE = 'managedDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateManagedDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTemplateManagedDevices - errors', () => {
      it('should have a editTemplateManagedDevices function', (done) => {
        try {
          assert.equal(true, typeof a.editTemplateManagedDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedDevice', (done) => {
        try {
          a.editTemplateManagedDevices(null, null, (data, error) => {
            try {
              const displayE = 'managedDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateManagedDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTemplateManagedDevices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateManagedDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateAttributes - errors', () => {
      it('should have a deleteTemplateAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.deleteTemplateAttributes(null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.deleteTemplateAttributes('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.deleteTemplateAttributes('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTemplateAttributes - errors', () => {
      it('should have a editTemplateAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.editTemplateAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.editTemplateAttributes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.editTemplateAttributes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.editTemplateAttributes('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTemplateAttributes('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateTunnel - errors', () => {
      it('should have a deleteTemplateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedDevice', (done) => {
        try {
          a.deleteTemplateTunnel(null, null, (data, error) => {
            try {
              const displayE = 'managedDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnel', (done) => {
        try {
          a.deleteTemplateTunnel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tunnel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-deleteTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTemplateTunnel - errors', () => {
      it('should have a editTemplateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.editTemplateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedDevice', (done) => {
        try {
          a.editTemplateTunnel(null, null, null, (data, error) => {
            try {
              const displayE = 'managedDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnel', (done) => {
        try {
          a.editTemplateTunnel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tunnel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTemplateTunnel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDeviceGroups - errors', () => {
      it('should have a getTemplateDeviceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateDeviceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDeviceTemplates - errors', () => {
      it('should have a getTemplateDeviceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateDeviceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateManagedDevices - errors', () => {
      it('should have a getTemplateManagedDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateManagedDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateAttributes - errors', () => {
      it('should have a getTemplateAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getTemplateAttributes(null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.getTemplateAttributes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTunnel - errors', () => {
      it('should have a getTemplateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managedDevice', (done) => {
        try {
          a.getTemplateTunnel(null, (data, error) => {
            try {
              const displayE = 'managedDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editClassOfService - errors', () => {
      it('should have a editClassOfService function', (done) => {
        try {
          assert.equal(true, typeof a.editClassOfService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.editClassOfService(null, null, null, null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editClassOfService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.editClassOfService('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editClassOfService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing network', (done) => {
        try {
          a.editClassOfService('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'network is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editClassOfService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editClassOfService('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editClassOfService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyTemplate - errors', () => {
      it('should have a applyTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.applyTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.applyTemplate(null, null, null, null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-applyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reboot', (done) => {
        try {
          a.applyTemplate('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'reboot is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-applyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mode', (done) => {
        try {
          a.applyTemplate('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-applyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.applyTemplate('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-applyTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMonitoringDatabase - errors', () => {
      it('should have a updateMonitoringDatabase function', (done) => {
        try {
          assert.equal(true, typeof a.updateMonitoringDatabase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMonitoringDatabase(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateMonitoringDatabase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMonitoringStatus - errors', () => {
      it('should have a updateMonitoringStatus function', (done) => {
        try {
          assert.equal(true, typeof a.updateMonitoringStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enabled', (done) => {
        try {
          a.updateMonitoringStatus(null, null, (data, error) => {
            try {
              const displayE = 'enabled is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateMonitoringStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMonitoringStatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-updateMonitoringStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startMonitorPolling - errors', () => {
      it('should have a startMonitorPolling function', (done) => {
        try {
          assert.equal(true, typeof a.startMonitorPolling === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.startMonitorPolling(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-startMonitorPolling', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshMonitorData - errors', () => {
      it('should have a refreshMonitorData function', (done) => {
        try {
          assert.equal(true, typeof a.refreshMonitorData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.refreshMonitorData(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-refreshMonitorData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringCacheApplianceDataForTenant - errors', () => {
      it('should have a getMonitoringCacheApplianceDataForTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoringCacheApplianceDataForTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantName', (done) => {
        try {
          a.getMonitoringCacheApplianceDataForTenant(null, null, (data, error) => {
            try {
              const displayE = 'tenantName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getMonitoringCacheApplianceDataForTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceUUID', (done) => {
        try {
          a.getMonitoringCacheApplianceDataForTenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getMonitoringCacheApplianceDataForTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringCacheApplianceData - errors', () => {
      it('should have a getMonitoringCacheApplianceData function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoringCacheApplianceData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceUUID', (done) => {
        try {
          a.getMonitoringCacheApplianceData(null, (data, error) => {
            try {
              const displayE = 'applianceUUID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getMonitoringCacheApplianceData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringCacheDataForTenant - errors', () => {
      it('should have a getMonitoringCacheDataForTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getMonitoringCacheDataForTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantName', (done) => {
        try {
          a.getMonitoringCacheDataForTenant(null, (data, error) => {
            try {
              const displayE = 'tenantName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getMonitoringCacheDataForTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateFromOrganization - errors', () => {
      it('should have a getTemplateFromOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateFromOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.getTemplateFromOrganization(null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getTemplateFromOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editTemplateSecurityPolicy - errors', () => {
      it('should have a editTemplateSecurityPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.editTemplateSecurityPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing template', (done) => {
        try {
          a.editTemplateSecurityPolicy(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'template is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organization', (done) => {
        try {
          a.editTemplateSecurityPolicy('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'organization is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policygroup', (done) => {
        try {
          a.editTemplateSecurityPolicy('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'policygroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policy', (done) => {
        try {
          a.editTemplateSecurityPolicy('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.editTemplateSecurityPolicy('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-editTemplateSecurityPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportDomains - errors', () => {
      it('should have a getTransportDomains function', (done) => {
        try {
          assert.equal(true, typeof a.getTransportDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDashboardApplianceHardware - errors', () => {
      it('should have a getDashboardApplianceHardware function', (done) => {
        try {
          assert.equal(true, typeof a.getDashboardApplianceHardware === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getDashboardApplianceHardware(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getDashboardApplianceHardware', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancePageableRoutes - errors', () => {
      it('should have a getAppliancePageableRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancePageableRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getAppliancePageableRoutes(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getAppliancePageableRoutes('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getAppliancePageableRoutes('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancePageableArp - errors', () => {
      it('should have a getAppliancePageableArp function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancePageableArp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getAppliancePageableArp(null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableArp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getAppliancePageableArp('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableArp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancePageableArpOrgName - errors', () => {
      it('should have a getAppliancePageableArpOrgName function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancePageableArpOrgName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getAppliancePageableArpOrgName(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableArpOrgName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getAppliancePageableArpOrgName('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableArpOrgName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getAppliancePageableArpOrgName('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getAppliancePageableArpOrgName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appliancePing - errors', () => {
      it('should have a appliancePing function', (done) => {
        try {
          assert.equal(true, typeof a.appliancePing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.appliancePing(null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-appliancePing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.appliancePing('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-appliancePing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOwnedRoutingInstances - errors', () => {
      it('should have a getOwnedRoutingInstances function', (done) => {
        try {
          assert.equal(true, typeof a.getOwnedRoutingInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getOwnedRoutingInstances(null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOwnedRoutingInstances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getOwnedRoutingInstances('fakeparam', null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getOwnedRoutingInstances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPageableInterfaces - errors', () => {
      it('should have a getPageableInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getPageableInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getPageableInterfaces(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPageableInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getPageableInterfaces('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPageableInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing orgName', (done) => {
        try {
          a.getPageableInterfaces('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'orgName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getPageableInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceVni - errors', () => {
      it('should have a getInterfaceVni function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceVni === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getInterfaceVni(null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getInterfaceVni', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutingInstancesRoutingInstance - errors', () => {
      it('should have a getRoutingInstancesRoutingInstance function', (done) => {
        try {
          assert.equal(true, typeof a.getRoutingInstancesRoutingInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getRoutingInstancesRoutingInstance(null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getRoutingInstancesRoutingInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should have a getNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceName', (done) => {
        try {
          a.getNetworks(null, (data, error) => {
            try {
              const displayE = 'applianceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-versa_director-adapter-getNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
