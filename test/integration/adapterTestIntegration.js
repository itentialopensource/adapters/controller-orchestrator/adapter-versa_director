/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-versa_director',
      type: 'VersaDirector',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const VersaDirector = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Versa_director Adapter Test', () => {
  describe('VersaDirector Class Tests', () => {
    const a = new VersaDirector(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const alarmsAssignee = 'fakedata';
    const alarmsDescription = 'fakedata';
    const alarmsState = 'fakedata';
    describe('#assignAlarmObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignAlarmObject(null, alarmsAssignee, alarmsDescription, alarmsState, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'assignAlarmObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsDeviceName = 'fakedata';
    const alarmsManagedObject = 'fakedata';
    const alarmsOrg = 'fakedata';
    const alarmsType = 'fakedata';
    const alarmsSpecificProblem = 'fakedata';
    describe('#clearAlarm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clearAlarm(alarmsDeviceName, alarmsManagedObject, alarmsOrg, alarmsSpecificProblem, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'clearAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#handleAlarmObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleAlarmObject(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, alarmsAssignee, alarmsDescription, alarmsSpecificProblem, alarmsState, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'handleAlarmObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmHandlingObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmHandlingObject(null, null, null, null, null, null, null, alarmsDeviceName, null, null, null, null, null, alarmsOrg, null, null, alarmsSpecificProblem, null, null, null, null, alarmsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alarmHandlings));
                assert.equal('SDWAN-Branch2', data.response.device);
                assert.equal('San-Jose-DG', data.response.deviceGroup);
                assert.equal('SDWAN-Branch2', data.response.deviceName);
                assert.equal('true', data.response.isCleared);
                assert.equal('CPU usage on appliance R2Controller exceeded hard limit', data.response.lastAlarmText);
                assert.equal('critical', data.response.lastPerceivedSeverity);
                assert.equal('2020-01-01 12:02:44', data.response.lastStatusChangeTimeStamp);
                assert.equal('vni-0/0', data.response.object);
                assert.equal('Tenant-2', data.response.org);
                assert.equal('string', data.response.serialNum);
                assert.equal('string', data.response.severity);
                assert.equal('string', data.response.specificProblem);
                assert.equal(true, Array.isArray(data.response.statusChanges));
                assert.equal('interface-down', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmHandlingObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgeAlarmObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.purgeAlarmObject(null, null, null, null, null, null, null, alarmsDeviceName, null, null, null, null, null, alarmsOrg, null, null, alarmsSpecificProblem, null, null, null, null, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'purgeAlarmObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusChangeObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusChangeObject(null, null, null, null, null, null, null, alarmsDeviceName, null, null, null, null, null, alarmsOrg, null, null, alarmsSpecificProblem, null, null, null, null, alarmsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alarmHandlings));
                assert.equal('SDWAN-Branch2', data.response.device);
                assert.equal('San-Jose-DG', data.response.deviceGroup);
                assert.equal('SDWAN-Branch2', data.response.deviceName);
                assert.equal('true', data.response.isCleared);
                assert.equal('CPU usage on appliance R2Controller exceeded hard limit', data.response.lastAlarmText);
                assert.equal('critical', data.response.lastPerceivedSeverity);
                assert.equal('2020-01-01 12:02:44', data.response.lastStatusChangeTimeStamp);
                assert.equal('vni-0/0', data.response.object);
                assert.equal('Tenant-2', data.response.org);
                assert.equal('string', data.response.serialNum);
                assert.equal('string', data.response.severity);
                assert.equal('string', data.response.specificProblem);
                assert.equal(true, Array.isArray(data.response.statusChanges));
                assert.equal('interface-down', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getStatusChangeObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsassignAllAlarmsBodyParam = {
      alarms: [
        {
          alarmHandlings: [
            {
              assignedBy: 'ConsoleOperator',
              description: 'Administrator',
              state: 'closed',
              time: '2020-04-13 09:16:02.986123',
              user: 'assigned'
            }
          ],
          device: 'SDWAN-Branch2',
          deviceGroup: 'San-Jose-DG',
          deviceName: 'SDWAN-Branch2',
          isCleared: 'true',
          lastAlarmText: 'CPU usage on appliance R2Controller exceeded hard limit',
          lastPerceivedSeverity: 'critical',
          lastStatusChangeTimeStamp: '2020-01-01 12:02:44',
          object: 'vni-0/0',
          org: 'Tenant-2',
          serialNum: 'string',
          severity: 'string',
          'specific-problem': 'string',
          statusChanges: [
            {
              alarmText: 'string',
              eventTime: '2020-04-08 00:28:42',
              receivedTime: '2020-04-08 00:29:12.8594',
              severity: 'major'
            }
          ],
          type: 'interface-down'
        }
      ],
      assignee: 'string',
      description: 'string',
      state: 'string'
    };
    describe('#assignAllAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignAllAlarms(alarmsassignAllAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'assignAllAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmsclearAllAlarmsBodyParam = {
      alarms: [
        {
          alarmHandlings: [
            {
              assignedBy: 'ConsoleOperator',
              description: 'Administrator',
              state: 'closed',
              time: '2020-04-13 09:16:02.986123',
              user: 'assigned'
            }
          ],
          device: 'SDWAN-Branch2',
          deviceGroup: 'San-Jose-DG',
          deviceName: 'SDWAN-Branch2',
          isCleared: 'true',
          lastAlarmText: 'CPU usage on appliance R2Controller exceeded hard limit',
          lastPerceivedSeverity: 'critical',
          lastStatusChangeTimeStamp: '2020-01-01 12:02:44',
          object: 'vni-0/0',
          org: 'Tenant-2',
          serialNum: 'string',
          severity: 'string',
          'specific-problem': 'string',
          statusChanges: [
            {
              alarmText: 'string',
              eventTime: '2020-04-08 00:28:42',
              receivedTime: '2020-04-08 00:29:12.8594',
              severity: 'major'
            }
          ],
          type: 'interface-down'
        }
      ],
      assignee: 'string',
      description: 'string',
      state: 'string'
    };
    describe('#clearAllAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clearAllAlarms(alarmsclearAllAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'clearAllAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmspurgeAllAlarmsBodyParam = {
      alarms: [
        {
          alarmHandlings: [
            {
              assignedBy: 'ConsoleOperator',
              description: 'Administrator',
              state: 'closed',
              time: '2020-04-13 09:16:02.986123',
              user: 'assigned'
            }
          ],
          device: 'SDWAN-Branch2',
          deviceGroup: 'San-Jose-DG',
          deviceName: 'SDWAN-Branch2',
          isCleared: 'true',
          lastAlarmText: 'CPU usage on appliance R2Controller exceeded hard limit',
          lastPerceivedSeverity: 'critical',
          lastStatusChangeTimeStamp: '2020-01-01 12:02:44',
          object: 'vni-0/0',
          org: 'Tenant-2',
          serialNum: 'string',
          severity: 'string',
          'specific-problem': 'string',
          statusChanges: [
            {
              alarmText: 'string',
              eventTime: '2020-04-08 00:28:42',
              receivedTime: '2020-04-08 00:29:12.8594',
              severity: 'major'
            }
          ],
          type: 'interface-down'
        }
      ],
      assignee: 'string',
      description: 'string',
      state: 'string'
    };
    describe('#purgeAllAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.purgeAllAlarms(alarmspurgeAllAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'purgeAllAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlarmAssignee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAlarmAssignee(alarmsAssignee, alarmsDescription, alarmsDeviceName, alarmsManagedObject, alarmsOrg, alarmsSpecificProblem, alarmsState, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'updateAlarmAssignee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHandleAlarm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateHandleAlarm(alarmsDescription, alarmsDeviceName, alarmsManagedObject, alarmsOrg, alarmsSpecificProblem, alarmsState, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'updateHandleAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmHandling - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmHandling(alarmsDeviceName, alarmsManagedObject, alarmsOrg, alarmsSpecificProblem, alarmsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alarmHandlings));
                assert.equal('SDWAN-Branch2', data.response.device);
                assert.equal('San-Jose-DG', data.response.deviceGroup);
                assert.equal('SDWAN-Branch2', data.response.deviceName);
                assert.equal('true', data.response.isCleared);
                assert.equal('CPU usage on appliance R2Controller exceeded hard limit', data.response.lastAlarmText);
                assert.equal('critical', data.response.lastPerceivedSeverity);
                assert.equal('2020-01-01 12:02:44', data.response.lastStatusChangeTimeStamp);
                assert.equal('vni-0/0', data.response.object);
                assert.equal('Tenant-2', data.response.org);
                assert.equal('string', data.response.serialNum);
                assert.equal('string', data.response.severity);
                assert.equal('string', data.response.specificProblem);
                assert.equal(true, Array.isArray(data.response.statusChanges));
                assert.equal('interface-down', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmHandling', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatusChange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatusChange(alarmsDeviceName, alarmsManagedObject, alarmsOrg, alarmsSpecificProblem, alarmsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alarmHandlings));
                assert.equal('SDWAN-Branch2', data.response.device);
                assert.equal('San-Jose-DG', data.response.deviceGroup);
                assert.equal('SDWAN-Branch2', data.response.deviceName);
                assert.equal('true', data.response.isCleared);
                assert.equal('CPU usage on appliance R2Controller exceeded hard limit', data.response.lastAlarmText);
                assert.equal('critical', data.response.lastPerceivedSeverity);
                assert.equal('2020-01-01 12:02:44', data.response.lastStatusChangeTimeStamp);
                assert.equal('vni-0/0', data.response.object);
                assert.equal('Tenant-2', data.response.org);
                assert.equal('string', data.response.serialNum);
                assert.equal('string', data.response.severity);
                assert.equal('string', data.response.specificProblem);
                assert.equal(true, Array.isArray(data.response.statusChanges));
                assert.equal('interface-down', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getStatusChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllFilteredAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllFilteredAlarms(alarmsDeviceName, null, null, null, null, null, null, alarmsOrg, alarmsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAllFilteredAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignAllFilteredAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignAllFilteredAlarms(alarmsAssignee, alarmsDescription, alarmsDeviceName, null, null, null, null, null, alarmsOrg, alarmsState, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'assignAllFilteredAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmshandleAllAlarmsBodyParam = {
      alarms: [
        {
          alarmHandlings: [
            {
              assignedBy: 'ConsoleOperator',
              description: 'Administrator',
              state: 'closed',
              time: '2020-04-13 09:16:02.986123',
              user: 'assigned'
            }
          ],
          device: 'SDWAN-Branch2',
          deviceGroup: 'San-Jose-DG',
          deviceName: 'SDWAN-Branch2',
          isCleared: 'true',
          lastAlarmText: 'CPU usage on appliance R2Controller exceeded hard limit',
          lastPerceivedSeverity: 'critical',
          lastStatusChangeTimeStamp: '2020-01-01 12:02:44',
          object: 'vni-0/0',
          org: 'Tenant-2',
          serialNum: 'string',
          severity: 'string',
          'specific-problem': 'string',
          statusChanges: [
            {
              alarmText: 'string',
              eventTime: '2020-04-08 00:28:42',
              receivedTime: '2020-04-08 00:29:12.8594',
              severity: 'major'
            }
          ],
          type: 'interface-down'
        }
      ],
      assignee: 'string',
      description: 'string',
      state: 'string'
    };
    describe('#handleAllAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleAllAlarms(alarmshandleAllAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'handleAllAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#handleAllFilteredAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.handleAllFilteredAlarms(alarmsDescription, alarmsDeviceName, null, null, null, null, null, alarmsOrg, alarmsState, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'handleAllFilteredAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterPaginateAlarm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterPaginateAlarm(alarmsDeviceName, null, null, null, null, null, null, null, null, null, null, alarmsOrg, null, null, null, alarmsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'filterPaginateAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceAlarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceAlarmSummary(alarmsDeviceName, alarmsOrg, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getDeviceAlarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmSummaryPerOrg - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmSummaryPerOrg(null, null, alarmsOrg, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmSummaryPerOrg', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectorAlarms(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getDirectorAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorAlarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectorAlarmSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getDirectorAlarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorFailOverAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectorFailOverAlarms((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getDirectorFailOverAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorHAAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectorHAAlarms((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getDirectorHAAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImpAlarms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImpAlarms((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getImpAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImpAlarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImpAlarmSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getImpAlarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'getAlarmTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgeAlarm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.purgeAlarm(alarmsDeviceName, alarmsManagedObject, alarmsOrg, alarmsSpecificProblem, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'purgeAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purgeAllFilteredAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.purgeAllFilteredAlarms(null, null, alarmsDeviceName, null, null, null, null, null, null, null, alarmsOrg, alarmsType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarms', 'purgeAllFilteredAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmNotifications(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'getAlarmNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmNotificationsaveAlarmNotificationBodyParam = {};
    describe('#saveAlarmNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.saveAlarmNotification(alarmNotificationsaveAlarmNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Result', data.response.name);
                assert.equal('Success', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'saveAlarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmNotificationeditAlarmNotificationBodyParam = {};
    describe('#editAlarmNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.editAlarmNotification(alarmNotificationeditAlarmNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'editAlarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAlarmNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAlarmNotification('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'deleteAlarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadAllAlarmNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loadAllAlarmNotifications((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.actionAttributes));
                assert.equal(true, data.response.alertOnClear);
                assert.equal(true, Array.isArray(data.response.conditions));
                assert.equal('[ "provider-org", "Tenant" ]', data.response.groupKeys);
                assert.equal('My-Notification-Rule', data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'loadAllAlarmNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkDeleteAlarmNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bulkDeleteAlarmNotification(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'bulkDeleteAlarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshAlarmNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.refreshAlarmNotification((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Result', data.response.name);
                assert.equal('Success', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'refreshAlarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmNotificationRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlarmNotificationRule('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.actionAttributes));
                assert.equal(true, data.response.alertOnClear);
                assert.equal(true, Array.isArray(data.response.conditions));
                assert.equal('[ "provider-org", "Tenant" ]', data.response.groupKeys);
                assert.equal('My-Notification-Rule', data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'getAlarmNotificationRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAlarmNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAlarmNotification('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.List);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlarmNotification', 'searchAlarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMQPEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAMQPEvents(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Amqp', 'getAMQPEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAMQPObjEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAMQPObjEvents(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Amqp', 'getAMQPObjEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplianceStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplianceStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceStatus', 'getAllApplianceStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceStatusById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplianceStatusById(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.applianceName);
                assert.equal('string', data.response.applianceOverallStatus);
                assert.equal('string', data.response.applianceUUID);
                assert.equal(false, data.response.isBranchInMaintenanceMode);
                assert.equal('string', data.response.maintenanceModeIP);
                assert.equal('string', data.response.ncsAndApplianceYangCompatible);
                assert.equal(true, Array.isArray(data.response.nodesStatusList));
                assert.equal('string', data.response.pingStatus);
                assert.equal('string', data.response.servicesStatus);
                assert.equal('string', data.response.syncStatus);
                assert.equal(true, Array.isArray(data.response.ucpeNodesStatusList));
                assert.equal('string', data.response.updateTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceStatus', 'getApplianceStatusById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceTemplateListing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplianceTemplateListing('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deviceGroup);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.generalTemplate);
                assert.equal('string', data.response.organization);
                assert.equal('string', data.response.postStagingTemplate);
                assert.equal(true, Array.isArray(data.response.serviceTemplates));
                assert.equal('string', data.response.stagingTemplate);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceStatus', 'getApplianceTemplateListing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliances(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.appliances));
                assert.equal('string', data.response.id);
                assert.equal(1, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliances2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliances2(20, 0, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.appliances));
                assert.equal('string', data.response.id);
                assert.equal(3, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliances2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesByTypeAndOrg - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancesByTypeAndOrg('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response['appliance-list'][0]['branch-maintenance-mode']);
                assert.equal('string', data.response['appliance-list'][0]['branch-maintenance-mode-ip']);
                assert.equal('string', data.response['appliance-list'][0]['controll-status']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliancesByTypeAndOrg', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesByType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancesByType('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliancesByType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForSecurityPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancesForSecurityPackage((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliancesForSecurityPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportApplianceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportApplianceConfiguration(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'exportApplianceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#filterAppliances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.filterAppliances(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.appliances));
                assert.equal('string', data.response.id);
                assert.equal(1, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'filterAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliancesForOrganization(null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.appliances));
                assert.equal('string', data.response.id);
                assert.equal(9, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliancesForOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsForAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTagsForAppliance((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getTagsForAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsForTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTagsForTenant('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getTagsForTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importApplianceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importApplianceConfiguration(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'importApplianceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appliancesViewSetApplianceOwnerBodyParam = {};
    describe('#setApplianceOwner - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setApplianceOwner(appliancesViewSetApplianceOwnerBodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'setApplianceOwner', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForTenantOrgs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancesForTenantOrgs((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliancesForTenantOrgs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAppliances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAppliances(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.appliances));
                assert.equal('string', data.response.id);
                assert.equal(5, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'searchAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancesSummary(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAppliancesSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#doSyncFromAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.doSyncFromAppliance('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'doSyncFromAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#doSyncToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.doSyncToAppliance('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'doSyncToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appliancesViewDeleteTagsFromApplianceBodyParam = {};
    describe('#deleteTagsFromAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTagsFromAppliance('fakedata', appliancesViewDeleteTagsFromApplianceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'deleteTagsFromAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendorForAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVendorForAppliance('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getVendorForAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplianceRoutingInstanceInformation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllApplianceRoutingInstanceInformation('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getAllApplianceRoutingInstanceInformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceRoutingInstanceInformationById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplianceRoutingInstanceInformationById('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'getApplianceRoutingInstanceInformationById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appliancesViewSetTagsForApplianceBodyParam = {};
    describe('#setTagsForAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setTagsForAppliance('fakedata', appliancesViewSetTagsForApplianceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.message);
                assert.equal(false, data.response.result);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesView', 'setTagsForAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationServiceTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationServiceTemplates(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'getAllApplicationServiceTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationServiceTemplateCreateApplicationServiceTemplateBodyParam = {};
    describe('#createApplicationServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApplicationServiceTemplate(applicationServiceTemplateCreateApplicationServiceTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'createApplicationServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplicationServiceTemplateSamples - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplicationServiceTemplateSamples((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'getAllApplicationServiceTemplateSamples', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneApplicationServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cloneApplicationServiceTemplate(null, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'cloneApplicationServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployApplicationServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployApplicationServiceTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'deployApplicationServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationServiceTemplateUpdateApplicationServiceTemplateBodyParam = {};
    describe('#updateApplicationServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateApplicationServiceTemplate('fakedata', applicationServiceTemplateUpdateApplicationServiceTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'updateApplicationServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplicationServiceTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'deleteApplicationServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationServiceTemplateById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplicationServiceTemplateById('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationServiceTemplate', 'getApplicationServiceTemplateById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommunityStrings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCommunityStrings(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Community', 'getAllCommunityStrings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const communityCreateCommunityBodyParam = {
      communityId: 5,
      id: 4,
      name: 'community-ref-1',
      org: 'Tenant1'
    };
    describe('#createCommunity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCommunity(communityCreateCommunityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.communityId);
                assert.equal(7, data.response.id);
                assert.equal('community-ref-1', data.response.name);
                assert.equal('Tenant1', data.response.org);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Community', 'createCommunity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const communityCreateBulkCommunitiesBodyParam = {
      communityBeans: [
        {
          communityId: 7,
          name: 'community-ref-1'
        }
      ],
      orgName: 'Tenant1'
    };
    describe('#createBulkCommunities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkCommunities(communityCreateBulkCommunitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Community', 'createBulkCommunities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextAvailableCommunityId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNextAvailableCommunityId(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.nextAvailableCommunityId);
                assert.equal('string', data.response.orgName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Community', 'getNextAvailableCommunityId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getControllerWorkflowList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getControllerWorkflowList('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['versanms.sdwan-controller-list']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControllerWorkflow', 'getControllerWorkflowList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controllerWorkflowCreateJsonControllerWorkflowBodyParam = {
      'versanms.sdwan-controller-workflow': {
        analyticsCluster: 'string',
        baremetalController: {
          cmsOrg: 'string',
          controllerInterface: {
            interfaceName: 'string',
            unitInfoList: [
              {
                ipv4address: [
                  'string'
                ],
                ipv4dhcp: false,
                ipv4gateway: 'string',
                ipv6address: [
                  'string'
                ],
                ipv6dhcp: false,
                ipv6gateway: 'string',
                networkName: 'string',
                poolSize: 6,
                vlanId: 1,
                wanStaging: false
              }
            ]
          },
          serverIP: 'string',
          wanInterfaces: [
            {
              interfaceName: 'string',
              unitInfoList: [
                {
                  ipv4address: [
                    'string'
                  ],
                  ipv4dhcp: false,
                  ipv4fqdn: 'string',
                  ipv4gateway: 'string',
                  ipv6InterfaceMode: 'string',
                  ipv6address: [
                    'string'
                  ],
                  ipv6dhcp: false,
                  ipv6fqdn: 'string',
                  ipv6gateway: 'string',
                  networkName: 'string',
                  poolSize: 7,
                  publicIPAddress: 'string',
                  transportDomainList: [
                    'string'
                  ],
                  vlanId: 7,
                  wanStaging: true
                }
              ]
            }
          ]
        },
        bgp: {
          peerIP: 'string',
          peerIPAs: 'string'
        },
        controllerName: 'string',
        locationInfo: {
          address1: '123 Main Street',
          address2: 'Suite 400',
          city: 'San Jose',
          country: 'USA',
          latitude: 37.242286,
          longitude: -121.730945,
          state: 'CA',
          zip: '95138'
        },
        orgName: 'string',
        ospf: {
          areaId: 'string'
        },
        peerControllers: [
          'string'
        ],
        postStagingController: false,
        resourceType: 'string',
        siteId: 9,
        stagingController: false,
        staticRoutes: {
          areaId: [
            {
              nexthop: 'string',
              prefix: 'string'
            }
          ]
        },
        subOrgs: [
          'string'
        ],
        vcpeController: {
          availabilityZone: 'string',
          cmsOrg: 'string',
          controllerInterface: {
            ipAssignment: 'string',
            networkName: 'string'
          },
          image: 'string',
          vdcOrg: 'string',
          wanInterfaces: [
            {
              fqdnIPV4: 'string',
              ipAssignment: 'string',
              networkName: 'string',
              publicIPAddress: 'string',
              transportDomainList: [
                'string'
              ]
            }
          ]
        }
      }
    };
    describe('#createJsonControllerWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createJsonControllerWorkflow(controllerWorkflowCreateJsonControllerWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.body);
                assert.equal('415 UNSUPPORTED_MEDIA_TYPE', data.response.statusCode);
                assert.equal(7, data.response.statusCodeValue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControllerWorkflow', 'createJsonControllerWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteControllerWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteControllerWorkflows([], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControllerWorkflow', 'deleteControllerWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controllerWorkflowDeployControllerWorkflowBodyParam = {};
    describe('#deployControllerWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployControllerWorkflow('fakedata', 'fakedata', controllerWorkflowDeployControllerWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControllerWorkflow', 'deployControllerWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJsonControllerWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJsonControllerWorkflow('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.analyticsCluster);
                assert.equal('object', typeof data.response.baremetalController);
                assert.equal('object', typeof data.response.bgp);
                assert.equal('string', data.response.controllerName);
                assert.equal('object', typeof data.response.locationInfo);
                assert.equal('string', data.response.orgName);
                assert.equal('object', typeof data.response.ospf);
                assert.equal(true, Array.isArray(data.response.peerControllers));
                assert.equal(false, data.response.postStagingController);
                assert.equal('string', data.response.resourceType);
                assert.equal(9, data.response.siteId);
                assert.equal(true, data.response.stagingController);
                assert.equal('object', typeof data.response.staticRoutes);
                assert.equal(true, Array.isArray(data.response.subOrgs));
                assert.equal('object', typeof data.response.vcpeController);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControllerWorkflow', 'getJsonControllerWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const controllerWorkflowUpdateJsonControllerWorkflowBodyParam = {
      'versanms.sdwan-controller-workflow': {
        analyticsCluster: 'string',
        baremetalController: {
          cmsOrg: 'string',
          controllerInterface: {
            interfaceName: 'string',
            unitInfoList: [
              {
                ipv4address: [
                  'string'
                ],
                ipv4dhcp: false,
                ipv4gateway: 'string',
                ipv6address: [
                  'string'
                ],
                ipv6dhcp: false,
                ipv6gateway: 'string',
                networkName: 'string',
                poolSize: 4,
                vlanId: 10,
                wanStaging: false
              }
            ]
          },
          serverIP: 'string',
          wanInterfaces: [
            {
              interfaceName: 'string',
              unitInfoList: [
                {
                  ipv4address: [
                    'string'
                  ],
                  ipv4dhcp: true,
                  ipv4fqdn: 'string',
                  ipv4gateway: 'string',
                  ipv6InterfaceMode: 'string',
                  ipv6address: [
                    'string'
                  ],
                  ipv6dhcp: false,
                  ipv6fqdn: 'string',
                  ipv6gateway: 'string',
                  networkName: 'string',
                  poolSize: 2,
                  publicIPAddress: 'string',
                  transportDomainList: [
                    'string'
                  ],
                  vlanId: 10,
                  wanStaging: false
                }
              ]
            }
          ]
        },
        bgp: {
          peerIP: 'string',
          peerIPAs: 'string'
        },
        controllerName: 'string',
        locationInfo: {
          address1: '123 Main Street',
          address2: 'Suite 400',
          city: 'San Jose',
          country: 'USA',
          latitude: 37.242286,
          longitude: -121.730945,
          state: 'CA',
          zip: '95138'
        },
        orgName: 'string',
        ospf: {
          areaId: 'string'
        },
        peerControllers: [
          'string'
        ],
        postStagingController: true,
        resourceType: 'string',
        siteId: 2,
        stagingController: false,
        staticRoutes: {
          areaId: [
            {
              nexthop: 'string',
              prefix: 'string'
            }
          ]
        },
        subOrgs: [
          'string'
        ],
        vcpeController: {
          availabilityZone: 'string',
          cmsOrg: 'string',
          controllerInterface: {
            ipAssignment: 'string',
            networkName: 'string'
          },
          image: 'string',
          vdcOrg: 'string',
          wanInterfaces: [
            {
              fqdnIPV4: 'string',
              ipAssignment: 'string',
              networkName: 'string',
              publicIPAddress: 'string',
              transportDomainList: [
                'string'
              ]
            }
          ]
        }
      }
    };
    describe('#updateJsonControllerWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateJsonControllerWorkflow('fakedata', controllerWorkflowUpdateJsonControllerWorkflowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControllerWorkflow', 'updateJsonControllerWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteControllerWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteControllerWorkflow('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ControllerWorkflow', 'deleteControllerWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDeviceWorkflows(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(87, data.response.totalCount);
                assert.equal(true, Array.isArray(data.response['versanms.sdwan-device-list']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'getAllDeviceWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceWorkflowCreateDevicefromJsonBodyParam = {
      'versanms.sdwan-device-workflow': {
        applianceExists: true,
        bandwidth: 100,
        deploymentType: 'physical',
        deviceGroup: 'MultiTenant_Url_Group1',
        deviceName: 'Branch1',
        deviceSpecificServiceTemplates: [
          {
            category: 'general',
            name: 'general1',
            organization: 'Tenant1'
          }
        ],
        email: 'admin@yourcompany.com',
        instanceInfo: {
          acceleratedNetworking: false,
          availabilitySet: 'string',
          availabilityZone: 'us-west-2a',
          connector: 'awsgdversaconnector',
          image: 'ami-9b7d1bfb',
          instanceProfile: 'string',
          instanceType: 'r3.xlarge',
          keyPair: 'gdmuraliawsversadev',
          networkData: [
            {
              assignPublicIp: true,
              deviceNetworkName: 'MGMT',
              id: 'subnet-095b016e',
              interfaceType: 'mgmt',
              networkType: 'mgmt',
              securityGroup: 'sg-7699ec0d'
            }
          ],
          region: 'us-west-2',
          resourceGroup: 'string',
          vpcId: 'vpc-3d3c3c5a'
        },
        lastModifiedTime: '2019-09-18 14:39:45',
        lastUpdatedBy: 'tenant1SuperAdmin1',
        licensePeriod: 2,
        locationInfo: {
          address1: '123 Main Street',
          address2: 'Suite 400',
          city: 'San Jose',
          country: 'USA',
          latitude: 37.242286,
          longitude: -121.730945,
          state: 'CA',
          zip: '95138'
        },
        orgName: 'Tenant1',
        'phone-number': '800-800-8000',
        postStagingTemplateInfo: {
          templateData: {
            count: 9,
            'device-template-variable': [
              {}
            ],
            id: 'string',
            variableMetadata: [
              {
                group: 'string',
                overlay: true,
                type: 'string',
                variable: 'string'
              }
            ]
          },
          templateName: 'PostStaging-California'
        },
        serialNumber: '702ebf22-a682-44c8-9abd-00bba7a3728a',
        siteId: '186',
        siteToSiteTunnelsInfo: {
          tunnels: [
            {
              bgpAsNo: 5,
              bgpEnabled: true,
              connector: 'string',
              lanAddressSpace: 'string',
              name: 'string',
              peerType: 'PaloAlto',
              psk: 'string',
              region: 'string',
              resourceGroup: 'string',
              virtualWanId: 'string'
            }
          ]
        },
        stagingTemplateInfo: {
          templateData: {
            count: 3,
            'device-template-variable': [
              {}
            ],
            id: 'string',
            variableMetadata: [
              {
                group: 'string',
                overlay: false,
                type: 'string',
                variable: 'string'
              }
            ]
          },
          templateName: 'PostStaging-California'
        },
        urlztpBasedInfo: {
          authType: 'psk',
          branchId: 'SDWAN-Branch2@provider-org.com',
          branchKey: 'DtAlOCObfjktnFPf',
          caEmail: 'admin1@versa.com',
          caPassword: '**********',
          caUsername: 'admin1',
          dnsServer: '8.8.8.8',
          enableIpv4: true,
          enableIpv4Dhcp: true,
          enableIpv6: false,
          enableIpv6Dhcp: true,
          enablePPPoE: true,
          pppoeAccessConcentrator: 'string',
          pppoePassword: samProps.authentication.password,
          pppoeServiceName: 'service1',
          pppoeUsername: 'user1',
          wanMTU: 'string',
          wanVlan: 'string',
          wanv4IpAddress: '10.0.48.52',
          wanv4gateway: '0.255.255.255',
          wanv6IpAddress: '2001:0db8:85a3:0000:0000:8a2e:0370:7334',
          wanv6gateway: '2001:db8:a0b:12f0::1'
        },
        workflowStatus: 'deployed'
      }
    };
    describe('#createDevicefromJson - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createDevicefromJson(deviceWorkflowCreateDevicefromJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'createDevicefromJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceWorkflowDeploybulkDevicesJsonBodyParam = {
      'versanms.sdwan-device-workflow-list': {
        deviceNameList: [
          'string'
        ]
      }
    };
    describe('#deploybulkDevicesJson - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deploybulkDevicesJson(deviceWorkflowDeploybulkDevicesJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'deploybulkDevicesJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'deployDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceWorkflowGetDeviceWorkflowTemplateDataInJsonBodyParam = {
      'versanms.sdwan-device-workflow': {
        applianceExists: true,
        bandwidth: 100,
        deploymentType: 'physical',
        deviceGroup: 'MultiTenant_Url_Group1',
        deviceName: 'Branch1',
        deviceSpecificServiceTemplates: [
          {
            category: 'general',
            name: 'general1',
            organization: 'Tenant1'
          }
        ],
        email: 'admin@yourcompany.com',
        instanceInfo: {
          acceleratedNetworking: false,
          availabilitySet: 'string',
          availabilityZone: 'us-west-2a',
          connector: 'awsgdversaconnector',
          image: 'ami-9b7d1bfb',
          instanceProfile: 'string',
          instanceType: 'r3.xlarge',
          keyPair: 'gdmuraliawsversadev',
          networkData: [
            {
              assignPublicIp: true,
              deviceNetworkName: 'MGMT',
              id: 'subnet-095b016e',
              interfaceType: 'mgmt',
              networkType: 'mgmt',
              securityGroup: 'sg-7699ec0d'
            }
          ],
          region: 'us-west-2',
          resourceGroup: 'string',
          vpcId: 'vpc-3d3c3c5a'
        },
        lastModifiedTime: '2019-09-18 14:39:45',
        lastUpdatedBy: 'tenant1SuperAdmin1',
        licensePeriod: 2,
        locationInfo: {
          address1: '123 Main Street',
          address2: 'Suite 400',
          city: 'San Jose',
          country: 'USA',
          latitude: 37.242286,
          longitude: -121.730945,
          state: 'CA',
          zip: '95138'
        },
        orgName: 'Tenant1',
        'phone-number': '800-800-8000',
        postStagingTemplateInfo: {
          templateData: {
            count: 4,
            'device-template-variable': [
              {}
            ],
            id: 'string',
            variableMetadata: [
              {
                group: 'string',
                overlay: true,
                type: 'string',
                variable: 'string'
              }
            ]
          },
          templateName: 'PostStaging-California'
        },
        serialNumber: '702ebf22-a682-44c8-9abd-00bba7a3728a',
        siteId: '186',
        siteToSiteTunnelsInfo: {
          tunnels: [
            {
              bgpAsNo: 7,
              bgpEnabled: true,
              connector: 'string',
              lanAddressSpace: 'string',
              name: 'string',
              peerType: 'AWSTransitGW',
              psk: 'string',
              region: 'string',
              resourceGroup: 'string',
              virtualWanId: 'string'
            }
          ]
        },
        stagingTemplateInfo: {
          templateData: {
            count: 5,
            'device-template-variable': [
              {}
            ],
            id: 'string',
            variableMetadata: [
              {
                group: 'string',
                overlay: false,
                type: 'string',
                variable: 'string'
              }
            ]
          },
          templateName: 'PostStaging-California'
        },
        urlztpBasedInfo: {
          authType: 'psk',
          branchId: 'SDWAN-Branch2@provider-org.com',
          branchKey: 'DtAlOCObfjktnFPf',
          caEmail: 'admin1@versa.com',
          caPassword: '**********',
          caUsername: 'admin1',
          dnsServer: '8.8.8.8',
          enableIpv4: true,
          enableIpv4Dhcp: true,
          enableIpv6: false,
          enableIpv6Dhcp: false,
          enablePPPoE: true,
          pppoeAccessConcentrator: 'string',
          pppoePassword: samProps.authentication.password,
          pppoeServiceName: 'service1',
          pppoeUsername: 'user1',
          wanMTU: 'string',
          wanVlan: 'string',
          wanv4IpAddress: '10.0.48.52',
          wanv4gateway: '0.255.255.255',
          wanv6IpAddress: '2001:0db8:85a3:0000:0000:8a2e:0370:7334',
          wanv6gateway: '2001:db8:a0b:12f0::1'
        },
        workflowStatus: 'deployed'
      }
    };
    describe('#getDeviceWorkflowTemplateDataInJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceWorkflowTemplateDataInJson(deviceWorkflowGetDeviceWorkflowTemplateDataInJsonBodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.sdwan-device-workflow']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'getDeviceWorkflowTemplateDataInJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHaPairedSiteLocationIdMapByTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHaPairedSiteLocationIdMapByTemplate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.multiOptionsVOWrapper));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'getHaPairedSiteLocationIdMapByTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceJsonWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceJsonWorkflow('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.sdwan-device-workflow']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'getDeviceJsonWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceWorkflowUpdateDevicefromJsonUsingPUT1BodyParam = {
      'versanms.sdwan-device-workflow': {
        applianceExists: true,
        bandwidth: 100,
        deploymentType: 'physical',
        deviceGroup: 'MultiTenant_Url_Group1',
        deviceName: 'Branch1',
        deviceSpecificServiceTemplates: [
          {
            category: 'general',
            name: 'general1',
            organization: 'Tenant1'
          }
        ],
        email: 'admin@yourcompany.com',
        instanceInfo: {
          acceleratedNetworking: true,
          availabilitySet: 'string',
          availabilityZone: 'us-west-2a',
          connector: 'awsgdversaconnector',
          image: 'ami-9b7d1bfb',
          instanceProfile: 'string',
          instanceType: 'r3.xlarge',
          keyPair: 'gdmuraliawsversadev',
          networkData: [
            {
              assignPublicIp: true,
              deviceNetworkName: 'MGMT',
              id: 'subnet-095b016e',
              interfaceType: 'mgmt',
              networkType: 'mgmt',
              securityGroup: 'sg-7699ec0d'
            }
          ],
          region: 'us-west-2',
          resourceGroup: 'string',
          vpcId: 'vpc-3d3c3c5a'
        },
        lastModifiedTime: '2019-09-18 14:39:45',
        lastUpdatedBy: 'tenant1SuperAdmin1',
        licensePeriod: 2,
        locationInfo: {
          address1: '123 Main Street',
          address2: 'Suite 400',
          city: 'San Jose',
          country: 'USA',
          latitude: 37.242286,
          longitude: -121.730945,
          state: 'CA',
          zip: '95138'
        },
        orgName: 'Tenant1',
        'phone-number': '800-800-8000',
        postStagingTemplateInfo: {
          templateData: {
            count: 7,
            'device-template-variable': [
              {}
            ],
            id: 'string',
            variableMetadata: [
              {
                group: 'string',
                overlay: false,
                type: 'string',
                variable: 'string'
              }
            ]
          },
          templateName: 'PostStaging-California'
        },
        serialNumber: '702ebf22-a682-44c8-9abd-00bba7a3728a',
        siteId: '186',
        siteToSiteTunnelsInfo: {
          tunnels: [
            {
              bgpAsNo: 7,
              bgpEnabled: true,
              connector: 'string',
              lanAddressSpace: 'string',
              name: 'string',
              peerType: 'ZscalerUnmanaged',
              psk: 'string',
              region: 'string',
              resourceGroup: 'string',
              virtualWanId: 'string'
            }
          ]
        },
        stagingTemplateInfo: {
          templateData: {
            count: 2,
            'device-template-variable': [
              {}
            ],
            id: 'string',
            variableMetadata: [
              {
                group: 'string',
                overlay: true,
                type: 'string',
                variable: 'string'
              }
            ]
          },
          templateName: 'PostStaging-California'
        },
        urlztpBasedInfo: {
          authType: 'psk',
          branchId: 'SDWAN-Branch2@provider-org.com',
          branchKey: 'DtAlOCObfjktnFPf',
          caEmail: 'admin1@versa.com',
          caPassword: '**********',
          caUsername: 'admin1',
          dnsServer: '8.8.8.8',
          enableIpv4: true,
          enableIpv4Dhcp: true,
          enableIpv6: false,
          enableIpv6Dhcp: true,
          enablePPPoE: true,
          pppoeAccessConcentrator: 'string',
          pppoePassword: samProps.authentication.password,
          pppoeServiceName: 'service1',
          pppoeUsername: 'user1',
          wanMTU: 'string',
          wanVlan: 'string',
          wanv4IpAddress: '10.0.48.52',
          wanv4gateway: '0.255.255.255',
          wanv6IpAddress: '2001:0db8:85a3:0000:0000:8a2e:0370:7334',
          wanv6gateway: '2001:db8:a0b:12f0::1'
        },
        workflowStatus: 'deployed'
      }
    };
    describe('#updateDevicefromJson - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDevicefromJson(deviceWorkflowUpdateDevicefromJsonUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'updateDevicefromJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deCommisionDeleteWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deCommisionDeleteWorkflow(null, 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'deCommisionDeleteWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportDeviceWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportDeviceWorkflow('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'exportDeviceWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDeviceWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importDeviceWorkflows('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.sdwan-device-import-workflow']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'importDeviceWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDeviceWorkflowProgressStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importDeviceWorkflowProgressStatus('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.sdwan-device-import-status']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'importDeviceWorkflowProgressStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDeviceWorkflowResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importDeviceWorkflowResult('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.sdwan-device-import-result']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'importDeviceWorkflowResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceWorkflowGetDeviceOwnerStatusInJsonUsingPOST1BodyParam = {
      list: [
        'string'
      ]
    };
    describe('#getDeviceOwnerStatusInJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceOwnerStatusInJson(deviceWorkflowGetDeviceOwnerStatusInJsonUsingPOST1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'getDeviceOwnerStatusInJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceteWorkflows - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceteWorkflows(null, 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceWorkflow', 'deleteDeviceteWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#migrateSDWANWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.migrateSDWANWorkflow((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.numberDevicesAlreadyMigrated);
                assert.equal(5, data.response.numberDevicesCouldNotBeMigrated);
                assert.equal(4, data.response.numberDevicesMigrated);
                assert.equal(3, data.response.numberHaPairAppliancesCouldNotBeMigrated);
                assert.equal(3, data.response.numberHaPairAppliancesMigrated);
                assert.equal(7, data.response.numberHaPairDevicesCouldNotBeMigrated);
                assert.equal(8, data.response.numberHaPairDevicesMigrated);
                assert.equal(1, data.response.numberTemplatesAlreadyMigrated);
                assert.equal(8, data.response.numberTemplatesCouldNotBeMigrated);
                assert.equal(4, data.response.numberTemplatesMigrated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkFlowMigrationController', 'migrateSDWANWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgWorkflowList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgWorkflowList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.totalCount);
                assert.equal(true, Array.isArray(data.response['versanms.sdwan-org-list']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrganizationWorkflow', 'getOrgWorkflowList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationWorkflowCreateOrgFromJsonUsingPOST1BodyParam = {
      'versanms.sdwan-org-workflow': {
        analyticsCluster: 'string',
        analyticsClusters: [
          'string'
        ],
        bandwidth: 9,
        cmsConnectors: [
          'string'
        ],
        controllers: [
          'string'
        ],
        cpeDeploymentType: 'string',
        crossAccountRoles: [
          {
            connector: 'string',
            role: 'string'
          }
        ],
        globalId: 'string',
        ikeAuthType: 'PSK',
        orgName: 'string',
        parentOrg: 'string',
        sharedControlPlane: true,
        solutionTier: 'string',
        supportedRoles: [
          'string'
        ],
        vrfs: [
          {
            description: 'string',
            enableVPN: true,
            id: 6,
            name: 'string'
          }
        ]
      }
    };
    describe('#createOrgWorkflowFromJson - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrgWorkflowFromJson(organizationWorkflowCreateOrgFromJsonUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrganizationWorkflow', 'createOrgWorkflowFromJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationWorkflowDeployOrgUsingPOST1BodyParam = {};
    describe('#deployOrgWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployOrgWorkflow('fakedata', organizationWorkflowDeployOrgUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.TaskResponse['task-id']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrganizationWorkflow', 'deployOrgWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgWorkflowFromJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgWorkflowFromJson('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.sdwan-org-workflow']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrganizationWorkflow', 'getOrgWorkflowFromJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationWorkflowUpdateOrgFromJsonUsingPUT1BodyParam = {
      'versanms.sdwan-org-workflow': {
        analyticsCluster: 'string',
        analyticsClusters: [
          'string'
        ],
        bandwidth: 1,
        cmsConnectors: [
          'string'
        ],
        controllers: [
          'string'
        ],
        cpeDeploymentType: 'string',
        crossAccountRoles: [
          {
            connector: 'string',
            role: 'string'
          }
        ],
        globalId: 'string',
        ikeAuthType: 'PSK',
        orgName: 'string',
        parentOrg: 'string',
        sharedControlPlane: true,
        solutionTier: 'string',
        supportedRoles: [
          'string'
        ],
        vrfs: [
          {
            description: 'string',
            enableVPN: false,
            id: 3,
            name: 'string'
          }
        ]
      }
    };
    describe('#updateOrgWorkflowFromJson - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOrgWorkflowFromJson('fakedata', organizationWorkflowUpdateOrgFromJsonUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrganizationWorkflow', 'updateOrgWorkflowFromJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationWorkflowDecommissionOrgWorkflowUsingDELETE1BodyParam = {};
    describe('#decommissionOrgWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.decommissionOrgWorkflow('fakedata', organizationWorkflowDecommissionOrgWorkflowUsingDELETE1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrganizationWorkflow', 'decommissionOrgWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgWorkflows - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrgWorkflows('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OrganizationWorkflow', 'deleteOrgWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUCPEServiceChains - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUCPEServiceChains('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceChainWorkflow', 'getUCPEServiceChains', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceChainWorkflowCreateUCPEServiceChainUsingPOST1BodyParam = {};
    describe('#createUCPEServiceChain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUCPEServiceChain(serviceChainWorkflowCreateUCPEServiceChainUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceChainWorkflow', 'createUCPEServiceChain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceChainWorkflowDeployUCPEServiceChainUsingPOST1BodyParam = {};
    describe('#deployUCPEServiceChain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployUCPEServiceChain(serviceChainWorkflowDeployUCPEServiceChainUsingPOST1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceChainWorkflow', 'deployUCPEServiceChain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUCPEServiceChainById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUCPEServiceChainById('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceChainWorkflow', 'getUCPEServiceChainById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceChainWorkflowUpdateUCPEServiceChainUsingPUT1BodyParam = {};
    describe('#updateUCPEServiceChain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUCPEServiceChain('fakedata', serviceChainWorkflowUpdateUCPEServiceChainUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceChainWorkflow', 'updateUCPEServiceChain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUCPEServiceChain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUCPEServiceChain('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceChainWorkflow', 'deleteUCPEServiceChain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTemplatesWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTemplatesWorkflows(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.totalCount);
                assert.equal(true, Array.isArray(data.response['versanms.sdwan-template-list']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'getAllTemplatesWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateWorkflowCreateTemplatefromXmlUsingPOST1BodyParam = {};
    describe('#createTemplateWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTemplateWorkflow(templateWorkflowCreateTemplatefromXmlUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'createTemplateWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployTemplateWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployTemplateWorkflow('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'deployTemplateWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateWorkflowById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateWorkflowById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.sdwan-template-workflow']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'getTemplateWorkflowById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateWorkflowUpdateTemplatefromJsonUsingPUT1BodyParam = {
      'versanms.sdwan-template-workflow': {
        LDAPServers: [
          {
            base: 'DC=example-domain,DC=com',
            bindDN: 'dc=mydomain',
            bindPassword: 'xxxxx',
            domainName: 'abc.com',
            networkName: 'network_2',
            server: '10.26.14.32'
          }
        ],
        NTPServers: [
          {
            actions: 'wifi-authentication',
            authKey: samProps.authentication.password,
            networkName: 'network_1',
            port: '22',
            server: '20.21.34.22'
          }
        ],
        RadiusServers: [
          {
            actions: 'wifi-authentication',
            authKey: samProps.authentication.password,
            networkName: 'network_1',
            port: '22',
            server: '20.21.34.22'
          }
        ],
        SNMPServers: [
          {
            actions: 'wifi-authentication',
            authKey: samProps.authentication.password,
            networkName: 'network_1',
            port: '22',
            server: '20.21.34.22'
          }
        ],
        SyslogServers: [
          {
            actions: 'wifi-authentication',
            authKey: samProps.authentication.password,
            networkName: 'network_1',
            port: '22',
            server: '20.21.34.22'
          }
        ],
        TacacsPlusServers: [
          {
            actions: 'wifi-authentication',
            authKey: samProps.authentication.password,
            networkName: 'network_1',
            port: '22',
            server: '20.21.34.22'
          }
        ],
        analyticsCluster: 'Analytics-1',
        bandwidth: 5,
        controllers: '[controller-1, controller-2]',
        cpeDeploymentType: 'baremetal',
        crossConnectport: 'vni-0/4',
        customParams: [
          {
            name: 'Result',
            value: 'Success'
          }
        ],
        deviceFirmfactor: 6,
        deviceType: 'full-mesh, spoke, hub ',
        diaConfig: {
          dia: true,
          loadBalance: true
        },
        inBoundNats: [
          {
            LANRoutingInstance: 'Tenant1-LAN-VR',
            externalAddress: '10.20.21.23',
            externalPort: '22',
            internalAddress: '10.18.25.11',
            internalPort: '26',
            lanroutingInstance: 'string',
            name: 'NAT_1',
            protocol: 'TCP',
            wanNetworkName: 'string'
          }
        ],
        isAnalyticsEnabled: true,
        isPrimary: true,
        isStaging: true,
        lanInterfaces: [
          {
            interfaceName: 'vni-0/1',
            unitInfo: [
              {
                allowSSH: true,
                dhcpV4Relay: true,
                dhcpV4RelayAddress: '{$v_Tenant1-LAN_DHCPv4_Relay_Address__dhcpRelayAddress}',
                dhcpV6Relay: true,
                dhcpV6RelayAddress: '{$v_Tenant1-LAN_DHCPv6_Relay_Address__dhcpRelayAddress}',
                dhcpv4Profile: 'dhcpv4_Profile_1',
                dhcpv6Profile: 'dhcpv6_Profile_1',
                dia: false,
                ip6Static: true,
                ipv4Dhcp: true,
                ipv4DhcpServer: true,
                ipv4Static: true,
                ipv6Dhcp: false,
                ipv6DhcpServer: true,
                ipv6ra: true,
                networkName: 'Tenant1-LAN',
                routing: {
                  bgp: {
                    localAS: 'AS 65101',
                    neighbor: 'AS 89101',
                    neighbors: [
                      {
                        bfd: true,
                        ibgp: true,
                        neighbor: 'AS 89101',
                        peerAS: 'AS 54301'
                      }
                    ],
                    peerAS: 'AS 54301'
                  },
                  igmp: {
                    groups: '[group1, group2]'
                  },
                  ospf: {
                    area: 'Area 7',
                    bfd: true
                  },
                  pim: true
                },
                subOrganization: 'Tenant-1',
                subUnit: '22',
                vlanId: '22',
                vrfName: 'Tenant-1-LAN-VR',
                zoneName: 'Zone1'
              }
            ]
          }
        ],
        licensePeriod: 4,
        minimumImageVersion: '20.2',
        providerOrg: {
          name: 'provider-org',
          nextGenFW: true,
          spokeGroup: 'spokegroup_1',
          statefulFW: true
        },
        redundantPair: {
          cloudCPE: false,
          enable: true,
          templateName: 'red_template_1',
          vrrp: true
        },
        redundantWanInterfaces: [
          {
            cloudConnect: false,
            interfaceName: 'vni-0/0',
            pppoe: false,
            unitInfo: [
              {
                DIA: true,
                allowSSH: true,
                ip6Static: true,
                ipv4Dhcp: true,
                ipv4Static: false,
                ipv6Dhcp: false,
                ipv6rs: true,
                linkPriority: '1',
                monitor: {
                  monitorIPv4Address: '10.40.21.45',
                  monitorNexthop: true
                },
                networkName: 'WAN_Network-1',
                routing: {
                  bgp: {
                    localAS: 'AS 65101',
                    neighbor: 'AS 89101',
                    neighbors: [
                      {
                        bfd: true,
                        ibgp: true,
                        neighbor: 'AS 89101',
                        peerAS: 'AS 54301'
                      }
                    ],
                    peerAS: 'AS 54301'
                  },
                  igmp: {
                    groups: '[group1, group2]'
                  },
                  ospf: {
                    area: 'Area 7',
                    bfd: true
                  },
                  pim: false
                },
                subUnit: '21',
                transportDomains: '[Internet, MPLS, TD_Mpls]',
                vlanId: '21'
              }
            ]
          }
        ],
        region: 'region_1',
        routingInstances: [
          {
            pimRPaddresses: [
              {
                address: '10.20.22.21',
                groupPrefixes: '[225.1.0.0/16,225.1.0.10/16]'
              }
            ],
            prefixes: [
              {
                nexthop: '10.20.11.21',
                prefix: '21'
              }
            ],
            routingInstance: 'Route_Instance_test'
          }
        ],
        siteToSiteTunnels: [
          {
            bgpEnabled: true,
            lanVrf: 'Tenant-1-LAN-VR',
            name: 'tunnel_1',
            networkName: 'Control_network',
            peerType: 'AzurevWAN',
            tunnelProtocol: 'GRE',
            vpnProfileName: 'vpn_profile_test',
            wanNetwork: 'WAN-1'
          }
        ],
        snmp: {
          community: 'community_1',
          password: samProps.authentication.password,
          snmpV1: true,
          snmpV2: true,
          snmpV3: true,
          userName: 'admin'
        },
        solutionTier: 'Prime-SDWAN',
        splitTunnels: [
          {
            dia: true,
            gateway: false,
            vrfName: 'Tenant-1-LAN-VR',
            wanNetworkName: 'WAN-1'
          }
        ],
        spokeGroup: 'spoke_group_1',
        subOrgs: [
          {
            name: 'provider-org',
            nextGenFW: true,
            spokeGroup: 'spokegroup_1',
            statefulFW: true
          }
        ],
        templateName: 'template_1',
        templateType: 'staging, post-staging ',
        transportDomains: [
          {
            description: 'User-defined domain',
            id: 6,
            name: 'Internet'
          }
        ],
        vrfName: 'Tenant-1-LAN-VR',
        wanInterfaces: [
          {
            cloudConnect: false,
            interfaceName: 'vni-0/0',
            pppoe: true,
            unitInfo: [
              {
                DIA: true,
                allowSSH: true,
                ip6Static: true,
                ipv4Dhcp: true,
                ipv4Static: false,
                ipv6Dhcp: false,
                ipv6rs: false,
                linkPriority: '1',
                monitor: {
                  monitorIPv4Address: '10.40.21.45',
                  monitorNexthop: true
                },
                networkName: 'WAN_Network-1',
                routing: {
                  bgp: {
                    localAS: 'AS 65101',
                    neighbor: 'AS 89101',
                    neighbors: [
                      {
                        bfd: true,
                        ibgp: false,
                        neighbor: 'AS 89101',
                        peerAS: 'AS 54301'
                      }
                    ],
                    peerAS: 'AS 54301'
                  },
                  igmp: {
                    groups: '[group1, group2]'
                  },
                  ospf: {
                    area: 'Area 7',
                    bfd: true
                  },
                  pim: false
                },
                subUnit: '21',
                transportDomains: '[Internet, MPLS, TD_Mpls]',
                vlanId: '21'
              }
            ]
          }
        ],
        wifiSettings: {
          ssid: [
            {
              broadcastSsid: 'true',
              encryptionType: 'ascii-64-bit-key',
              frequency: '2.4-GHz',
              networkName: 'network_2',
              password: samProps.authentication.password,
              radiusAuthKey: samProps.authentication.password,
              radiusServerAddress: '10.21.33.21',
              securityMode: 'wep-auto',
              ssidName: 'ssid_1'
            }
          ],
          ssids: [
            {
              broadcastSsid: 'true',
              encryptionType: 'ascii-64-bit-key',
              frequency: '2.4-GHz',
              networkName: 'network_2',
              password: samProps.authentication.password,
              radiusAuthKey: samProps.authentication.password,
              radiusServerAddress: '10.21.33.21',
              securityMode: 'wep-auto',
              ssidName: 'ssid_1'
            }
          ],
          wifi2d4GhzInfo: {
            channel: '10',
            channelWidth: '40MHz',
            countryName: 'US-United States',
            enable: true,
            protocol: 'b-2.4Ghz'
          },
          wifi5GhzInfo: {
            channel: '10',
            channelWidth: '40MHz',
            countryName: 'US-United States',
            enable: true,
            protocol: 'b-2.4Ghz'
          }
        }
      }
    };
    describe('#updateTemplateWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTemplateWorkflow(templateWorkflowUpdateTemplatefromJsonUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'updateTemplateWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#decommisionTemplateWorkflow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.decommisionTemplateWorkflow('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'decommisionTemplateWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateWorkflows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTemplateWorkflows('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'deleteTemplateWorkflows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateWorkflowAddPortfromXmlUsingPUT1BodyParam = {
      'versanms.sdwan-port-template-workflow': {
        diaConfig: {
          dia: true,
          loadBalance: true
        },
        lanInterfaces: [
          {
            interfaceName: 'vni-0/1',
            unitInfo: [
              {
                allowSSH: true,
                dhcpV4Relay: false,
                dhcpV4RelayAddress: '{$v_Tenant1-LAN_DHCPv4_Relay_Address__dhcpRelayAddress}',
                dhcpV6Relay: false,
                dhcpV6RelayAddress: '{$v_Tenant1-LAN_DHCPv6_Relay_Address__dhcpRelayAddress}',
                dhcpv4Profile: 'dhcpv4_Profile_1',
                dhcpv6Profile: 'dhcpv6_Profile_1',
                dia: false,
                ip6Static: true,
                ipv4Dhcp: true,
                ipv4DhcpServer: true,
                ipv4Static: false,
                ipv6Dhcp: false,
                ipv6DhcpServer: false,
                ipv6ra: true,
                networkName: 'Tenant1-LAN',
                routing: {
                  bgp: {
                    localAS: 'AS 65101',
                    neighbor: 'AS 89101',
                    neighbors: [
                      {
                        bfd: true,
                        ibgp: false,
                        neighbor: 'AS 89101',
                        peerAS: 'AS 54301'
                      }
                    ],
                    peerAS: 'AS 54301'
                  },
                  igmp: {
                    groups: '[group1, group2]'
                  },
                  ospf: {
                    area: 'Area 7',
                    bfd: true
                  },
                  pim: false
                },
                subOrganization: 'Tenant-1',
                subUnit: '22',
                vlanId: '22',
                vrfName: 'Tenant-1-LAN-VR',
                zoneName: 'Zone1'
              }
            ]
          }
        ],
        wanInterfaces: [
          {
            cloudConnect: false,
            interfaceName: 'vni-0/0',
            pppoe: true,
            unitInfo: [
              {
                DIA: true,
                allowSSH: true,
                ip6Static: true,
                ipv4Dhcp: true,
                ipv4Static: true,
                ipv6Dhcp: false,
                ipv6rs: false,
                linkPriority: '1',
                monitor: {
                  monitorIPv4Address: '10.40.21.45',
                  monitorNexthop: true
                },
                networkName: 'WAN_Network-1',
                routing: {
                  bgp: {
                    localAS: 'AS 65101',
                    neighbor: 'AS 89101',
                    neighbors: [
                      {
                        bfd: true,
                        ibgp: false,
                        neighbor: 'AS 89101',
                        peerAS: 'AS 54301'
                      }
                    ],
                    peerAS: 'AS 54301'
                  },
                  igmp: {
                    groups: '[group1, group2]'
                  },
                  ospf: {
                    area: 'Area 7',
                    bfd: true
                  },
                  pim: false
                },
                subUnit: '21',
                transportDomains: '[Internet, MPLS, TD_Mpls]',
                vlanId: '21'
              }
            ]
          }
        ]
      }
    };
    describe('#addPortToTemplateWorkflow - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPortToTemplateWorkflow(templateWorkflowAddPortfromXmlUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateWorkflow', 'addPortToTemplateWorkflow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRbacPrivilege - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRbacPrivilege((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'getAllRbacPrivilege', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshRbacRolesMetaInMemory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refreshRbacRolesMetaInMemory((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'refreshRbacRolesMetaInMemory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshRbacRolesMetaInMemoryById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refreshRbacRolesMetaInMemoryById('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'refreshRbacRolesMetaInMemoryById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customRBACCreateRbacOrgRoleBodyParam = {
      orgName: 'string',
      roleNames: [
        'string'
      ]
    };
    describe('#createRbacOrgRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRbacOrgRole(customRBACCreateRbacOrgRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'createRbacOrgRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableRbacOrgRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAvailableRbacOrgRoles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.orgName);
                assert.equal(true, Array.isArray(data.response.roleNames));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'getAvailableRbacOrgRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacOrgRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRbacOrgRoles('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.orgName);
                assert.equal(true, Array.isArray(data.response.roleNames));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'getRbacOrgRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacProviderRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRbacProviderRoles(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'getRbacProviderRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customRBACCreateRbacRoleBodyParam = {
      defaultLandingPages: [
        'string'
      ],
      definitionInProgress: true,
      description: 'string',
      help: 'string',
      label: 'string',
      permitAllAtEnd: false,
      privileges: [
        {
          actions: [
            'string'
          ],
          contextActions: [
            {
              action: 'string',
              contextList: [
                'APPLIANCE_OWNER'
              ]
            }
          ],
          customRules: [
            {
              actions: [
                'string'
              ],
              criterias: [
                {
                  key: 'string',
                  values: [
                    'string'
                  ]
                }
              ],
              ruleName: 'string'
            }
          ],
          name: 'string'
        }
      ],
      roleName: 'string',
      type: 'PROVIDER',
      versaRole: false
    };
    describe('#createRbacRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRbacRole(customRBACCreateRbacRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'createRbacRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customRBACUpdateRbacRoleBodyParam = {
      defaultLandingPages: [
        'string'
      ],
      definitionInProgress: false,
      description: 'string',
      help: 'string',
      label: 'string',
      permitAllAtEnd: false,
      privileges: [
        {
          actions: [
            'string'
          ],
          contextActions: [
            {
              action: 'string',
              contextList: [
                'TEMPLATE_OWNER'
              ]
            }
          ],
          customRules: [
            {
              actions: [
                'string'
              ],
              criterias: [
                {
                  key: 'string',
                  values: [
                    'string'
                  ]
                }
              ],
              ruleName: 'string'
            }
          ],
          name: 'string'
        }
      ],
      roleName: 'string',
      type: 'PROVIDER',
      versaRole: true
    };
    describe('#updateRbacRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRbacRole(customRBACUpdateRbacRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'updateRbacRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRbacRoleById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.defaultLandingPages));
                assert.equal(true, data.response.definitionInProgress);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.help);
                assert.equal('string', data.response.label);
                assert.equal(true, data.response.permitAllAtEnd);
                assert.equal(true, Array.isArray(data.response.privileges));
                assert.equal('string', data.response.roleName);
                assert.equal('TENANT', data.response.type);
                assert.equal(true, data.response.versaRole);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'getRbacRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRbacRole('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'deleteRbacRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deployRbacRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployRbacRole('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'deployRbacRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customRBACDeployRbacRoleAsyncBodyParam = {};
    describe('#deployRbacRoleAsync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deployRbacRoleAsync('fakedata', customRBACDeployRbacRoleAsyncBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'deployRbacRoleAsync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacTenantRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRbacTenantRoles(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomRBAC', 'getRbacTenantRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVersaDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceController', 'getDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDeviceGroups(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['device-group']));
                assert.equal(10, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getAllDeviceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupCreateDeviceGroupBodyParam = {
      'device-group': {
        'dg:ca-config-on-branch-notification': true,
        'dg:description': 'Device Group having multi tenant branches',
        'dg:e-mail': 'admin@versa-networks.com',
        'dg:enable-2factor-auth': true,
        'dg:enable-one-time-password': true,
        'dg:enable-staging-url': true,
        'dg:general-template': 'gen1template',
        'dg:organization': 'Provider1',
        'dg:phone': '408-480-4800',
        'dg:port': 'string',
        'dg:poststaging-template': 'controller1poststaging',
        'dg:staging-controller': 'Controller1',
        'dg:staging-template': 'globalStaging',
        'dg:staging-type': 'pre_statging',
        'dg:tags': 'multitenant,controller1',
        'dg:vpn-profile-name': 'wan1-staging1-StagingIpSec',
        'inventory-name': 'Branch1,Multi_Tenant_branch2',
        name: 'Single_Tenant_DeviceGroup',
        'template-association': [
          {
            category: 'nextgen-firewall',
            name: 'NGFW1',
            organization: 'Tenant2'
          }
        ]
      }
    };
    describe('#createDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceGroup(deviceGroupCreateDeviceGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response['ca-config-on-branch-notification']);
                assert.equal('2019-09-18 14:39:45', data.response.createDate);
                assert.equal('Device Group having multi tenant branches', data.response.description);
                assert.equal('admin@versa-networks.com', data.response.email);
                assert.equal(true, data.response['enable-2factor-auth']);
                assert.equal(true, data.response['enable-staging-url']);
                assert.equal('gen1template', data.response['general-template']);
                assert.equal('Branch1,Multi_Tenant_branch2', data.response['inventory-name']);
                assert.equal('Administrator', data.response.lastUpdatedBy);
                assert.equal('2019-11-11 08:19:45', data.response.modifyDate);
                assert.equal('Multi_Tenant_DeviceGroup', data.response.name);
                assert.equal(true, data.response.oneTimePassword);
                assert.equal('Tenant1', data.response.organization);
                assert.equal('408-480-4800', data.response.phone);
                assert.equal('string', data.response.port);
                assert.equal('controller1poststaging', data.response['poststaging-template']);
                assert.equal(2, data.response.poststagingTemplatePriority);
                assert.equal('Controller1', data.response['staging-controller']);
                assert.equal('globalStaging', data.response['staging-template']);
                assert.equal('pre_statging', data.response['staging-type']);
                assert.equal('multitenant,controller1', data.response.tags);
                assert.equal(true, Array.isArray(data.response['template-association']));
                assert.equal('wan1-staging1-StagingIpSec', data.response['vpn-profile-name']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'createDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkDeleteDeviceGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bulkDeleteDeviceGroup('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'bulkDeleteDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroupsNames - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDeviceGroupsNames('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['device-group']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getAllDeviceGroupsNames', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroupsByAnyTemplateAndOrg - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDeviceGroupsByAnyTemplateAndOrg(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['devicegroup-template-mapping']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getAllDeviceGroupsByAnyTemplateAndOrg', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDeviceGroupsByTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDeviceGroupsByTemplate(null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['devicegroup-template-mapping']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getAllDeviceGroupsByTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response['ca-config-on-branch-notification']);
                assert.equal('2019-09-18 14:39:45', data.response.createDate);
                assert.equal('Device Group having multi tenant branches', data.response.description);
                assert.equal('admin@versa-networks.com', data.response.email);
                assert.equal(true, data.response['enable-2factor-auth']);
                assert.equal(true, data.response['enable-staging-url']);
                assert.equal('gen1template', data.response['general-template']);
                assert.equal('Branch1,Multi_Tenant_branch2', data.response['inventory-name']);
                assert.equal('Administrator', data.response.lastUpdatedBy);
                assert.equal('2019-11-11 08:19:45', data.response.modifyDate);
                assert.equal('Multi_Tenant_DeviceGroup', data.response.name);
                assert.equal(true, data.response.oneTimePassword);
                assert.equal('Tenant1', data.response.organization);
                assert.equal('408-480-4800', data.response.phone);
                assert.equal('string', data.response.port);
                assert.equal('controller1poststaging', data.response['poststaging-template']);
                assert.equal(2, data.response.poststagingTemplatePriority);
                assert.equal('Controller1', data.response['staging-controller']);
                assert.equal('globalStaging', data.response['staging-template']);
                assert.equal('pre_statging', data.response['staging-type']);
                assert.equal('multitenant,controller1', data.response.tags);
                assert.equal(true, Array.isArray(data.response['template-association']));
                assert.equal('wan1-staging1-StagingIpSec', data.response['vpn-profile-name']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getDeviceGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupUpdateDeviceGroupBodyParam = {
      'device-group': {
        'dg:ca-config-on-branch-notification': true,
        'dg:description': 'Device Group having multi tenant branches',
        'dg:e-mail': 'admin@versa-networks.com',
        'dg:enable-2factor-auth': true,
        'dg:enable-one-time-password': true,
        'dg:enable-staging-url': true,
        'dg:general-template': 'gen1template',
        'dg:organization': 'Provider1',
        'dg:phone': '408-480-4800',
        'dg:port': 'string',
        'dg:poststaging-template': 'controller1poststaging',
        'dg:staging-controller': 'Controller1',
        'dg:staging-template': 'globalStaging',
        'dg:staging-type': 'pre_statging',
        'dg:tags': 'multitenant,controller1',
        'dg:vpn-profile-name': 'wan1-staging1-StagingIpSec',
        'inventory-name': 'Branch1,Multi_Tenant_branch2',
        name: 'Single_Tenant_DeviceGroup',
        'template-association': [
          {
            category: 'nextgen-firewall',
            name: 'NGFW1',
            organization: 'Tenant2'
          }
        ]
      }
    };
    describe('#updateDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceGroup(deviceGroupUpdateDeviceGroupBodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'updateDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceGroup('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'deleteDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupStandbyInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupStandbyInfo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Multi_Tenant_DeviceGroup', data.response.name);
                assert.equal(true, data.response.postStaging);
                assert.equal(true, data.response.primary);
                assert.equal(true, data.response.standbyAvailable);
                assert.equal('DgStandBy1, DG2Standby', data.response.standbyNames);
                assert.equal(true, data.response.standbyTemplateAvailable);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getDeviceGroupStandbyInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupTemplateAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupTemplateAssociation('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response['ca-config-on-branch-notification']);
                assert.equal('2019-09-18 14:39:45', data.response.createDate);
                assert.equal('Device Group having multi tenant branches', data.response.description);
                assert.equal('admin@versa-networks.com', data.response.email);
                assert.equal(true, data.response['enable-2factor-auth']);
                assert.equal(true, data.response['enable-staging-url']);
                assert.equal('gen1template', data.response['general-template']);
                assert.equal('Branch1,Multi_Tenant_branch2', data.response['inventory-name']);
                assert.equal('Administrator', data.response.lastUpdatedBy);
                assert.equal('2019-11-11 08:19:45', data.response.modifyDate);
                assert.equal('Multi_Tenant_DeviceGroup', data.response.name);
                assert.equal(true, data.response.oneTimePassword);
                assert.equal('Tenant1', data.response.organization);
                assert.equal('408-480-4800', data.response.phone);
                assert.equal('string', data.response.port);
                assert.equal('controller1poststaging', data.response['poststaging-template']);
                assert.equal(2, data.response.poststagingTemplatePriority);
                assert.equal('Controller1', data.response['staging-controller']);
                assert.equal('globalStaging', data.response['staging-template']);
                assert.equal('pre_statging', data.response['staging-type']);
                assert.equal('multitenant,controller1', data.response.tags);
                assert.equal(true, Array.isArray(data.response['template-association']));
                assert.equal('wan1-staging1-StagingIpSec', data.response['vpn-profile-name']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroup', 'getDeviceGroupTemplateAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.registerDevice(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceRegistration', 'registerDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegistrationDeviceInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRegistrationDeviceInfo('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['versanms.DeviceRegInfo']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceRegistration', 'getRegistrationDeviceInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerDeviceAsync - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerDeviceAsync('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.TaskResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceRegistration', 'registerDeviceAsync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDeviceEMAILSecureCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateDeviceEMAILSecureCode('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceRegistration', 'generateDeviceEMAILSecureCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateDeviceSMSSecureCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateDeviceSMSSecureCode('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceRegistration', 'generateDeviceSMSSecureCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyDeviceSecureCodeAndRegisterAsync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyDeviceSecureCodeAndRegisterAsync('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceRegistration', 'verifyDeviceSecureCodeAndRegisterAsync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportDocsSwagger - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportDocsSwagger(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ExportDocuments', 'exportDocsSwagger', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssets(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.assets));
                assert.equal(4, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getAssets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#availableAssets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.availableAssets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.assets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'availableAssets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAssetEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateAssetEmail('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'generateAssetEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryUpdateAssetUsingPUT2BodyParam = {};
    describe('#updateAsset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAsset(inventoryUpdateAssetUsingPUT2BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'updateAsset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelAssetSerialNumberReplacement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelAssetSerialNumberReplacement('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'cancelAssetSerialNumberReplacement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceAsset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.replaceAsset('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'replaceAsset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateBranchWithSerialNumber - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateBranchWithSerialNumber('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'associateBranchWithSerialNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUnknownDevices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getUnknownDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAllUnknownDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAllUnknownDevices('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'deleteAllUnknownDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnknownDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUnknownDeviceById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.branchId);
                assert.equal('2019-11-11 08:19:45', data.response.creationTime);
                assert.equal('br103.405', data.response.hwSerialNo);
                assert.equal('10.2.28.4', data.response.mgmtIp);
                assert.equal('branch-pre-staging', data.response.notificationType);
                assert.equal('702ebf22-a682-44c8-9abd-00bba7a3728a', data.response.serialNumber);
                assert.equal(true, Array.isArray(data.response.wanIps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'getUnknownDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnknownDeviceById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteUnknownDeviceById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'deleteUnknownDeviceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLdapGroups(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'getLdapGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapServerProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLdapServerProfile(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'getLdapServerProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLdapServerProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createLdapServerProfile(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'createLdapServerProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgToLDAPServerProfilesMap - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrgToLDAPServerProfilesMap((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'getOrgToLDAPServerProfilesMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapServerProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLdapServerProfiles(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'getLdapServerProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLdapUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLdapUsers(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'getLdapUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLdapServerProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateLdapServerProfile(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'updateLdapServerProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLdapServerProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLdapServerProfile(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LDAP', 'deleteLdapServerProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationTestSettingTestSmtpSettingResponseUsingPOST1BodyParam = {
      bccEmail: 'abc789@xxx.com',
      ccEmail: 'abc456@xxx.com',
      toEmail: 'abc123@xxx.com'
    };
    describe('#testSmtpSettingResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.testSmtpSettingResponse(notificationTestSettingTestSmtpSettingResponseUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.message);
                assert.equal(true, data.response.result);
                assert.equal('string', data.response.taskId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTestSetting', 'testSmtpSettingResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSOGenerateAndSaveSPAndIDPMetadataUsingPOST1BodyParam = {};
    describe('#generateAndSaveSPAndIDPMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateAndSaveSPAndIDPMetadata(sSOGenerateAndSaveSPAndIDPMetadataUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'generateAndSaveSPAndIDPMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSOUdpateSPAndIDPMetadataUsingPUT1BodyParam = {};
    describe('#udpateSPAndIDPMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.udpateSPAndIDPMetadata(sSOUdpateSPAndIDPMetadataUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'udpateSPAndIDPMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextgenOrganizations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNextgenOrganizations(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getNextgenOrganizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationCreateOrganizationUsingPOST1BodyParam = {};
    describe('#createOrganization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrganization(organizationCreateOrganizationUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.authType);
                assert.equal('string', data.response.postStagingAgent);
                assert.equal('string', data.response.stagingAgent);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'createOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationAgents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationAgents('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.authType);
                assert.equal('string', data.response.postStagingAgent);
                assert.equal('string', data.response.stagingAgent);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrganizationAgents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationGetOrgsDetailsUsingPOST1BodyParam = [
      'string'
    ];
    describe('#getOrgsDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgsDetails(organizationGetOrgsDetailsUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgsDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRootOrganizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRootOrganizations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getRootOrganizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDetailsForUUid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDetailsForUUid('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getDetailsForUUid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgVrfs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgVrfs('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrganizationDetails('fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrganizationDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationUpdateOrganizationUsingPUT1BodyParam = {};
    describe('#updateOrganization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOrganization(organizationUpdateOrganizationUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'updateOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'deleteOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgWanNetworkGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgWanNetworkGroups('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgWanNetworkGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationCreateWanNetworkGroupUsingPOST1BodyParam = {
      description: 'string',
      id: 1,
      name: 'string',
      'transport-domains': [
        'string'
      ]
    };
    describe('#createOrgWanNetworkGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrgWanNetworkGroup(organizationCreateWanNetworkGroupUsingPOST1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'createOrgWanNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgWanNetworkGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrgWanNetworkGroups([], 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'deleteOrgWanNetworkGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationUpdateWanNetworkGroupUsingPUT1BodyParam = {
      description: 'string',
      id: 7,
      name: 'string',
      'transport-domains': [
        'string'
      ]
    };
    describe('#updateOrgWanNetworkGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOrgWanNetworkGroup(organizationUpdateWanNetworkGroupUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'updateOrgWanNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrgWanNetworkGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrgWanNetworkGroup('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'deleteOrgWanNetworkGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizations(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.organizations));
                assert.equal(5, data.response.totalCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrganizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgsCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgsCount((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgsCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationSetWanPropagateUsingPOST1BodyParam = {};
    describe('#setOrgWanPropagate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setOrgWanPropagate(organizationSetWanPropagateUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'setOrgWanPropagate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZonesForOrganization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getZonesForOrganization([], (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getZonesForOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgChildren - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrgChildren(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.children));
                assert.equal('string', data.response.orgName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgChildren', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllOsspackPossibleCurrentVersionOnDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAllOsspackPossibleCurrentVersionOnDevice((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'listAllOsspackPossibleCurrentVersionOnDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOsspackDownloadsForDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOsspackDownloadsForDevice(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.checkSum);
                assert.equal('string', data.response.downloadTime);
                assert.equal('string', data.response.lastVersion);
                assert.equal('string', data.response.packageName);
                assert.equal(8, data.response.percentage);
                assert.equal('string', data.response.product);
                assert.equal(6, data.response.size);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.updateType);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'getAllOsspackDownloadsForDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDeviceOsspackUpdates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkDeviceOsspackUpdates(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'checkDeviceOsspackUpdates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDeviceOsspackUpdatesByDeviceName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkDeviceOsspackUpdatesByDeviceName('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'checkDeviceOsspackUpdatesByDeviceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOsspackFromDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOsspackFromDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'deleteOsspackFromDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceOsspackDownloadsToBeInstalled - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceOsspackDownloadsToBeInstalled(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.checkSum);
                assert.equal('string', data.response.downloadTime);
                assert.equal('string', data.response.lastVersion);
                assert.equal('string', data.response.packageName);
                assert.equal(3, data.response.percentage);
                assert.equal('string', data.response.product);
                assert.equal(7, data.response.size);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.updateType);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'getDeviceOsspackDownloadsToBeInstalled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oSSpackInstallDevicesUsingPOST1BodyParam = {
      devices: [
        'string'
      ],
      'update-type': 'string',
      version: 'string'
    };
    describe('#instalOsspackOnlDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instalOsspackOnlDevices(oSSpackInstallDevicesUsingPOST1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'instalOsspackOnlDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOsspackDownloadsForDirector - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOsspackDownloadsForDirector(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.checkSum);
                assert.equal('string', data.response.downloadTime);
                assert.equal('string', data.response.installTime);
                assert.equal('string', data.response.lastVersion);
                assert.equal('string', data.response.packageName);
                assert.equal(8, data.response.percentage);
                assert.equal('string', data.response.product);
                assert.equal(4, data.response.size);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.updateType);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'getAllOsspackDownloadsForDirector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDirectorOsspackUpdates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkDirectorOsspackUpdates('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'checkDirectorOsspackUpdates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOsspackFromDirector - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOsspackFromDirector('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'deleteOsspackFromDirector', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDirectorOsspackDownloadsToBeInstalled - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDirectorOsspackDownloadsToBeInstalled(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.checkSum);
                assert.equal('string', data.response.downloadTime);
                assert.equal('string', data.response.installTime);
                assert.equal('string', data.response.lastVersion);
                assert.equal('string', data.response.packageName);
                assert.equal(3, data.response.percentage);
                assert.equal('string', data.response.product);
                assert.equal(7, data.response.size);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.updateType);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'getDirectorOsspackDownloadsToBeInstalled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oSSpackInstallDirectorUsingPOST1BodyParam = {
      'update-type': 'string',
      version: 'string'
    };
    describe('#installDirectorOsspack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.installDirectorOsspack(oSSpackInstallDirectorUsingPOST1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'installDirectorOsspack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oSSpackDownloadUsingPOST2BodyParam = {};
    describe('#downloadOsspack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadOsspack('fakedata', oSSpackDownloadUsingPOST2BodyParam, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OSSpack', 'downloadOsspack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAdminAlerts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemOperations', 'getAdminAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificateRemainingDays - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCertificateRemainingDays((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.expired);
                assert.equal('2022-04-27 06:06:09', data.response.expiryDate);
                assert.equal('2020-01-23 06:06:09', data.response.startDate);
                assert.equal(749, data.response.totalRemDays);
                assert.equal(17977, data.response.totalRemHours);
                assert.equal(1078646, data.response.totalRemMinutes);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemOperations', 'getCertificateRemainingDays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadApplianceDebPackage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadApplianceDebPackage(null, 'fakedata', 'fakedata', 'fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemOperations', 'uploadApplianceDebPackage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemLogLevel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemLogLevel('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'getSystemLogLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSystemLogLevel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSystemLogLevel('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemSettings', 'updateSystemLogLevel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const upgradeCheckUpdateMinImageVersionUsingPOST1BodyParam = {};
    describe('#checkUpdateMinImageVersion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkUpdateMinImageVersion(upgradeCheckUpdateMinImageVersionUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Upgrade', 'checkUpdateMinImageVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRegions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRegions(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Region1', data.response.description);
                assert.equal('Region1', data.response.name);
                assert.equal(1, data.response.regionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Region', 'getAllRegions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const regionCreateRegionUsingPOST1BodyParam = {
      description: 'string',
      name: 'string',
      regionId: 5
    };
    describe('#createRegion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRegion(regionCreateRegionUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.regionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Region', 'createRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNextAvailableRegionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNextAvailableRegionId((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.nextAvailableRegionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Region', 'getNextAvailableRegionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRegionById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.regionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Region', 'getRegionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const regionUpdateRegionUsingPUT1BodyParam = {
      description: 'string',
      name: 'string',
      regionId: 3
    };
    describe('#updateRegion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRegion(regionUpdateRegionUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Region', 'updateRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRegion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRegion('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Region', 'deleteRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAndSaveIDPMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateAndSaveIDPMetadata(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'generateAndSaveIDPMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSOOverallStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSOOverallStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.isSSOEnabled);
                assert.equal('string', data.response.ssoInitiatedType);
                assert.equal('string', data.response.ssoType);
                assert.equal('string', data.response.ssoURL);
                assert.equal(false, data.response.ssoenabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'getSSOOverallStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSORolesMappingById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSORolesMappingById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['role-mapping']));
                assert.equal('string', data.response.tenantName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'getSSORolesMappingById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSOUpdateSSORolesMappingUsingPUT1BodyParam = {
      'role-mapping': [
        {
          customerRole: 'string',
          directorRole: 'string'
        }
      ],
      tenantName: 'string'
    };
    describe('#updateSSORolesMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSSORolesMapping(sSOUpdateSSORolesMappingUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'updateSSORolesMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSSORolesMapping - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSSORolesMapping('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'deleteSSORolesMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSOGenerateAndSaveSPMetadataUsingPOST1BodyParam = {};
    describe('#generateAndSaveSPMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateAndSaveSPMetadata(sSOGenerateAndSaveSPMetadataUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'generateAndSaveSPMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sSOUpdateSPMetadataUsingPUT1BodyParam = {};
    describe('#updateSPMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSPMetadata(sSOUpdateSPMetadataUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'updateSPMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSOStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSOStatus(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.isSSOEnabled);
                assert.equal('string', data.response.ssoInitiatedType);
                assert.equal('string', data.response.ssoType);
                assert.equal('string', data.response.ssoURL);
                assert.equal(true, data.response.ssoenabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'getSSOStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSSOStatusWithURL - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSSOStatusWithURL(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.isSSOEnabled);
                assert.equal('string', data.response.ssoInitiatedType);
                assert.equal('string', data.response.ssoType);
                assert.equal('string', data.response.ssoURL);
                assert.equal(false, data.response.ssoenabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSO', 'getSSOStatusWithURL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAssetsByOrgNameAndDeviceType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSDWANAssetsByOrgNameAndDeviceType('fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2', data.response.SdwanAssetList.assetList[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANAssetsByOrgNameAndDeviceType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANDeviceMappingURL - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDWANDeviceMappingURL(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.passcode);
                assert.equal('string', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANDeviceMappingURL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAvailableIds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDWANAvailableIds(1, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, Array.isArray(data.response.globalIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANAvailableIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANNextGlobalId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSDWANNextGlobalId('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANNextGlobalId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANNextGlobalIdWithSerialNumber - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDWANNextGlobalIdWithSerialNumber('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.branchId);
                assert.equal('string', data.response.serialNumbmer);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANNextGlobalIdWithSerialNumber', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#freeAllSDWANCachedValues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.freeAllSDWANCachedValues('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.globalIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'freeAllSDWANCachedValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#freeSDWANCachedAvailableGlobalId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.freeSDWANCachedAvailableGlobalId(555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.globalIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'freeSDWANCachedAvailableGlobalId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANStagingControllers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSDWANStagingControllers(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.stagingControllers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANStagingControllers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANStagingControllerVPNProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSDWANStagingControllerVPNProfiles('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('profile1', data.response.StagingControllerVPNProfile.vpnProfiles[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANStagingControllerVPNProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANURLBasedZTPInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSDWANURLBasedZTPInfo('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANURLBasedZTPInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteToSiteTunnelVPNProfileCreateSiteToSiteTunnelVPNProfileUsingPOST1BodyParam = {
      bgpEnabled: true,
      ikeTransform: 'AES256_SHA384',
      ikeVersion: 'string',
      ipsecTransform: 'ESP_3DES_MD5',
      name: 'string',
      orgUuid: 'string',
      primaryPeerPskId: 'string',
      primaryPeerPskKey: 'string',
      secondaryPeerPskId: 'string',
      secondaryPeerPskKey: 'string',
      siteToSiteTunnelPolicyBasedConfig: [
        {
          destinationIpAddress: 'string',
          destinationPort: 'string',
          ipAddressVersion: 'string',
          name: 'string',
          protocol: 'string',
          sourceIpAddress: 'string',
          sourcePort: 'string'
        }
      ],
      tunnelConfigType: 'POLICY',
      tunnelProtocol: 'GRE',
      tunnelsCount: 4
    };
    describe('#createSiteToSiteTunnelVPNProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSiteToSiteTunnelVPNProfile(siteToSiteTunnelVPNProfileCreateSiteToSiteTunnelVPNProfileUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteToSiteTunnelVPNProfile', 'createSiteToSiteTunnelVPNProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteToSiteTunnelVPNProfileUpdateSiteToSiteTunnelVPNProfileUsingPUT1BodyParam = {
      bgpEnabled: false,
      ikeTransform: 'DES3_MD5',
      ikeVersion: 'string',
      ipsecTransform: 'ESP_AES256_GCM',
      name: 'string',
      orgUuid: 'string',
      primaryPeerPskId: 'string',
      primaryPeerPskKey: 'string',
      secondaryPeerPskId: 'string',
      secondaryPeerPskKey: 'string',
      siteToSiteTunnelPolicyBasedConfig: [
        {
          destinationIpAddress: 'string',
          destinationPort: 'string',
          ipAddressVersion: 'string',
          name: 'string',
          protocol: 'string',
          sourceIpAddress: 'string',
          sourcePort: 'string'
        }
      ],
      tunnelConfigType: 'ROUTE',
      tunnelProtocol: 'IPSEC',
      tunnelsCount: 1
    };
    describe('#updateSiteToSiteTunnelVPNProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSiteToSiteTunnelVPNProfile(siteToSiteTunnelVPNProfileUpdateSiteToSiteTunnelVPNProfileUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteToSiteTunnelVPNProfile', 'updateSiteToSiteTunnelVPNProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteToSiteTunnelVPNProfileNames - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSiteToSiteTunnelVPNProfileNames(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteToSiteTunnelVPNProfile', 'getSiteToSiteTunnelVPNProfileNames', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteToSiteTunnelVPNProfile', 'getSiteToSiteTunnelVPNProfileNamesByTunnelProtocol', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteToSiteTunnelVPNProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteToSiteTunnelVPNProfiles(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.bgpEnabled);
                assert.equal('AES256_SHA384', data.response.ikeTransform);
                assert.equal('string', data.response.ikeVersion);
                assert.equal('ESP_AES128_CTR_XCBC', data.response.ipsecTransform);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.orgUuid);
                assert.equal('string', data.response.primaryPeerPskId);
                assert.equal('string', data.response.primaryPeerPskKey);
                assert.equal('string', data.response.secondaryPeerPskId);
                assert.equal('string', data.response.secondaryPeerPskKey);
                assert.equal(true, Array.isArray(data.response.siteToSiteTunnelPolicyBasedConfig));
                assert.equal('POLICY', data.response.tunnelConfigType);
                assert.equal('GRE', data.response.tunnelProtocol);
                assert.equal(3, data.response.tunnelsCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteToSiteTunnelVPNProfile', 'getSiteToSiteTunnelVPNProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteToSiteTunnelVPNProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSiteToSiteTunnelVPNProfiles(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SiteToSiteTunnelVPNProfile', 'deleteSiteToSiteTunnelVPNProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelSpackDownload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelSpackDownload(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'cancelSpackDownload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkAvailableSpackUpdates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkAvailableSpackUpdates(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'checkAvailableSpackUpdates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkSpackUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkSpackUpdate(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'checkSpackUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spackDeletePackagesUsingDELETE1BodyParam = {};
    describe('#deleteSpackPackages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSpackPackages(spackDeletePackagesUsingDELETE1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'deleteSpackPackages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spackDownloadUsingPOST3BodyParam = {};
    describe('#downloadSpack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadSpack(spackDownloadUsingPOST3BodyParam, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'downloadSpack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultiPredefinedSpack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMultiPredefinedSpack('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'getMultiPredefinedSpack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPredefinedSpack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPredefinedSpack(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'getPredefinedSpack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spackUpdateToApplianceUsingPOST1BodyParam = {};
    describe('#updateToSpackAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateToSpackAppliance(null, 'fakedata', null, spackUpdateToApplianceUsingPOST1BodyParam, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'updateToSpackAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spackUploadUsingPOST1BodyParam = {};
    describe('#uploadSpack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadSpack(null, 'fakedata', 'fakedata', spackUploadUsingPOST1BodyParam, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Spack', 'uploadSpack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpackCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSpackCount(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpackDownload', 'getSpackCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpackLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpackLogs(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.downloadTime);
                assert.equal('string', data.response.flavour);
                assert.equal('string', data.response.info);
                assert.equal('string', data.response.packageName);
                assert.equal('string', data.response.packageType);
                assert.equal('string', data.response.packageVersion);
                assert.equal('string', data.response.releaseDate);
                assert.equal(9, data.response.size);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpackDownload', 'getSpackLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpokeGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpokeGroups(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpokeGroup', 'getSpokeGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spokeGroupAddSpokeGroupUsingPOST1BodyParam = {
      hubType: 'hub',
      name: 'SpokeGroup1',
      org: 'Tenant1',
      region: 'west',
      status: 'CREATED',
      vrfs: [
        {
          communityRef: 5,
          groupType: 'HubAndSpoke',
          hubs: [
            {
              name: 'hub34',
              priority: 3
            }
          ],
          name: 'provider-org-vr'
        }
      ]
    };
    describe('#createSpokeGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSpokeGroup(spokeGroupAddSpokeGroupUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('hub', data.response.hubType);
                assert.equal('SpokeGroup1', data.response.name);
                assert.equal('Tenant1', data.response.org);
                assert.equal('west', data.response.region);
                assert.equal('CREATED', data.response.status);
                assert.equal(true, Array.isArray(data.response.vrfs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpokeGroup', 'createSpokeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpokeGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpokeGroupById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('hub', data.response.hubType);
                assert.equal('SpokeGroup1', data.response.name);
                assert.equal('Tenant1', data.response.org);
                assert.equal('west', data.response.region);
                assert.equal('CREATED', data.response.status);
                assert.equal(true, Array.isArray(data.response.vrfs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpokeGroup', 'getSpokeGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spokeGroupUpdateSpokeGroupUsingPUT1BodyParam = {
      hubType: 'hub',
      name: 'SpokeGroup1',
      org: 'Tenant1',
      region: 'west',
      status: 'CREATED',
      vrfs: [
        {
          communityRef: 5,
          groupType: 'HubAndSpoke',
          hubs: [
            {
              name: 'hub34',
              priority: 3
            }
          ],
          name: 'provider-org-vr'
        }
      ]
    };
    describe('#updateSpokeGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSpokeGroup(spokeGroupUpdateSpokeGroupUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpokeGroup', 'updateSpokeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSpokeGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSpokeGroup('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpokeGroup', 'deleteSpokeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTasks(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response['versanms.task']));
                assert.equal(100, data.response['versa-tasks.count']);
                assert.equal(5, data.response['versa-tasks.failedCount']);
                assert.equal(90, data.response['versa-tasks.completedCount']);
                assert.equal(5, data.response['versa-tasks.inProgressCount']);
                assert.equal(2, data.response['versa-tasks.pendingCount']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tasks', 'getTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBulkTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteBulkTasks([], (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tasks', 'deleteBulkTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskSummary(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(100, data.response['versa-tasks.count']);
                assert.equal(5, data.response['versa-tasks.failedCount']);
                assert.equal(90, data.response['versa-tasks.completedCount']);
                assert.equal(5, data.response['versa-tasks.inProgressCount']);
                assert.equal(5, data.response['versa-tasks.pendingCount']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tasks', 'getTaskSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tasks', 'getTaskById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTask(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tasks', 'deleteTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateReferences - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateReferences('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateAssociation', 'getTemplateReferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTemplateReferences - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTemplateReferences('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateAssociation', 'getAllTemplateReferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateBinddataCreateBindDataUsingPOST1BodyParam = {
      deviceName: 'NewYork-Branch1',
      id: 1,
      serialNumber: '7b08d5c8-7c35-46a7-8f09-56087715b38a',
      templateDeviceVariables: [
        {
          id: {
            name: '{$v_vni-0-0_-_Unit_0_Static_address__staticaddress}'
          },
          prevValue: 'string',
          val: '70.70.70.101/24'
        }
      ],
      templateName: 'MultiTenant-PostStaging-1'
    };
    describe('#createBindData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBindData(templateBinddataCreateBindDataUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateBinddata', 'createBindData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBinddataHeaderAndCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBinddataHeaderAndCount('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal(true, Array.isArray(data.response['device-template-variable']));
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.variableMetadata));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateBinddata', 'getBinddataHeaderAndCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBinddataForTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBinddataForTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response['device-template-variable']));
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.variableMetadata));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateBinddata', 'getBinddataForTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateBindata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateBindata(null, null, 'fakedata', null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal(true, Array.isArray(data.response.deviceTemplateVariable));
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.variableMetadata));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateBinddata', 'getTemplateBindata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateBinddataSaveTemplateBindataUsingPUT1BodyParam = {
      count: 3,
      deviceTemplateVariable: [
        {}
      ],
      id: 'string',
      variableMetadata: [
        {
          group: 'string',
          overlay: true,
          type: 'string',
          variable: 'string'
        }
      ]
    };
    describe('#saveTemplateBindata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveTemplateBindata('fakedata', null, 'fakedata', templateBinddataSaveTemplateBindataUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateBinddata', 'saveTemplateBindata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateBinddataDeleteTemplateBindataUsingDELETE1BodyParam = {
      count: 9,
      'device-template-variable': [
        {}
      ],
      id: 'string',
      variableMetadata: [
        {
          group: 'string',
          overlay: true,
          type: 'string',
          variable: 'string'
        }
      ]
    };
    describe('#deleteTemplateBindata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateBindata('fakedata', 'fakedata', templateBinddataDeleteTemplateBindataUsingDELETE1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateBinddata', 'deleteTemplateBindata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBindDataById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBindDataById(555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('NewYork-Branch1', data.response.deviceName);
                assert.equal(6, data.response.id);
                assert.equal('7b08d5c8-7c35-46a7-8f09-56087715b38a', data.response.serialNumber);
                assert.equal(true, Array.isArray(data.response.templateDeviceVariables));
                assert.equal('MultiTenant-PostStaging-1', data.response.templateName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateBinddata', 'getBindDataById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateCategories((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateCategory', 'getTemplateCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateByCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateByCategory('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateCategory', 'getTemplateByCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateCategoriesForOrg - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateCategoriesForOrg('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateCategory', 'getTemplateCategoriesForOrg', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateShareCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateShareCategories('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateCategory', 'getTemplateShareCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateMetadataById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateMetadataById(null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response['template-metadata']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMetadata', 'getTemplateMetadataById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMetadataSaveTemplateMetadataUsingPUT1BodyParam = {
      'template-metadata': {
        composite_or_partial: 'composite',
        'device-type': 'full-mesh,hub-controller,spoke',
        orgs: '[ Versa , HR Department]',
        'provider-tenant': 'Versa',
        subscription: {
          bandwidth: 100,
          'is-analytics-enabled': false,
          'is-primary': true,
          'license-period': 6,
          'solution-tier': 'base-sdwanutm,base-sdwan,standard-sdwan,advanced-sdwan,vcpe-plus-advanced-sdwan,advanced-sdwan-plus-ngfw,standard-sdwan-plus-ngfw,standard-sdwan-plus-utm,advanced-secure-sdwan'
        },
        'template-name': 'string',
        type: 'sdwan-staging,sdwan-post-staging'
      }
    };
    describe('#saveTemplateMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveTemplateMetadata(templateMetadataSaveTemplateMetadataUsingPUT1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMetadata', 'saveTemplateMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllImages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllImages(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.imagedetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'getAllImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadImageDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadImageDetails(555, 'fakedata', 555, 'fakedata', 'fakedata', true, true, 555, 'fakedata', 555, null, 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'uploadImageDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteImage('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'deleteImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateImage('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'updateImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageLogo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImageLogo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'getImageLogo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateImageLogoforDoc - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateImageLogoforDoc('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'updateImageLogoforDoc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateImageMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateImageMetadata(555, 'fakedata', 555, true, true, 555, 555, [], 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'updateImageMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageByName('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.compressed);
                assert.equal('object', typeof data.response.defaultVmSpec);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.fileSize);
                assert.equal('string', data.response.fileType);
                assert.equal('string', data.response.imageLogoPath);
                assert.equal(true, data.response.isAuxiliaryInterface);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.packagePath);
                assert.equal('string', data.response.productType);
                assert.equal(true, Array.isArray(data.response.serviceFunctions));
                assert.equal('string', data.response.uploadedTime);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.vendor);
                assert.equal('string', data.response.vendorProductType);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'getImageByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uCPEImagesUrlUploadUsingPOST1BodyParam = {
      cpuCount: 4,
      description: 'string',
      diskSize: 2,
      fileType: 'string',
      isAuxiliaryInterface: false,
      isSecondaryDiskNeeded: true,
      memory: 3,
      name: 'string',
      secondaryDiskSize: 5,
      serviceFunctions: [
        'string'
      ],
      urlString: 'string',
      vendor: 'string',
      vendorProductType: 'string',
      version: 'string'
    };
    describe('#urlImageUpload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.urlImageUpload(uCPEImagesUrlUploadUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'urlImageUpload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageByUuid('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.compressed);
                assert.equal('object', typeof data.response.defaultVmSpec);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.fileSize);
                assert.equal('string', data.response.fileType);
                assert.equal('string', data.response.imageLogoPath);
                assert.equal(false, data.response.isAuxiliaryInterface);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.packagePath);
                assert.equal('string', data.response.productType);
                assert.equal(true, Array.isArray(data.response.serviceFunctions));
                assert.equal('string', data.response.uploadedTime);
                assert.equal('string', data.response.url);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.vendor);
                assert.equal('string', data.response.vendorProductType);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'getImageByUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByVendor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageByVendor(555, 555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.imagedetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'getImageByVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageList(555, 555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.imagedetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'getImageList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImageByVendorProductType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImageByVendorProductType(555, 555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.imagedetails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEImages', 'getImageByVendorProductType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCaptivePortalToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadCaptivePortalToAppliance(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadCaptivePortalFile', 'uploadCaptivePortalToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCaptivePortalFromAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCaptivePortalFromAppliance(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadCaptivePortalFile', 'deleteCaptivePortalFromAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListOfPortalPages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getListOfPortalPages(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadCaptivePortalFile', 'getListOfPortalPages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCaptivePortalToVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadCaptivePortalToVD(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadCaptivePortalFile', 'uploadCaptivePortalToVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCaptivePortalFromVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCaptivePortalFromVD(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadCaptivePortalFile', 'deleteCaptivePortalFromVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSFilterTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPSFilterTypes('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.list));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'getIPSFilterTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadIPSVulnerabilityRuleFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadIPSVulnerabilityRuleFile(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'uploadIPSVulnerabilityRuleFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getListOfIPSUnzippedRuleFiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getListOfIPSUnzippedRuleFiles(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'getListOfIPSUnzippedRuleFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadIPSCustomRuleFileToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadIPSCustomRuleFileToAppliance(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'uploadIPSCustomRuleFileToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceIPSRuleFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplianceIPSRuleFile(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'deleteApplianceIPSRuleFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#configureIPSVulnerabilityRuleFiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.configureIPSVulnerabilityRuleFiles(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'configureIPSVulnerabilityRuleFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDIPSRuleFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVDIPSRuleFile(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'deleteVDIPSRuleFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSSignatures - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPSSignatures(null, null, 555, 555, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.map);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadIPSRuleFile', 'getIPSSignatures', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadKeyTabToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadKeyTabToAppliance(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadKeyTabFile', 'uploadKeyTabToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyTabFromAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteKeyTabFromAppliance(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadKeyTabFile', 'deleteKeyTabFromAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadKeyTabToVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadKeyTabToVD(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadKeyTabFile', 'uploadKeyTabToVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyTabFromVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteKeyTabFromVD(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadKeyTabFile', 'deleteKeyTabFromVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadPACToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadPACToAppliance(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadPACFile', 'uploadPACToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePACFromAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePACFromAppliance(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadPACFile', 'deletePACFromAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadPACToVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadPACToVD(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadPACFile', 'uploadPACToVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePACFromVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePACFromVD(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadPACFile', 'deletePACFromVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceSecurityFileInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplianceSecurityFileInfo(null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadSecurityFile', 'getApplianceSecurityFileInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadSecuityFileToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadSecuityFileToAppliance(null, null, null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadSecurityFile', 'uploadSecuityFileToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityFileFromAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityFileFromAppliance(null, null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadSecurityFile', 'deleteSecurityFileFromAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVDSecurityFileInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVDSecurityFileInfo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadSecurityFile', 'getVDSecurityFileInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadSecurityFileToVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadSecurityFileToVD(null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadSecurityFile', 'uploadSecurityFileToVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityFileFromVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityFileFromVD(null, null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadSecurityFile', 'deleteSecurityFileFromVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCertificateToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadCertificateToAppliance(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadTrustedCertificateFile', 'uploadCertificateToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplianceCertificate(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadTrustedCertificateFile', 'deleteApplianceCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCertificateToVD - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uploadCertificateToVD(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadTrustedCertificateFile', 'uploadCertificateToVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVDCertificate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVDCertificate(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UploadTrustedCertificateFile', 'deleteVDCertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findActiveLoginUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findActiveLoginUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'findActiveLoginUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportAllUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportAllUsers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'exportAllUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findAllLockedUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findAllLockedUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'findAllLockedUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userTrackingGetPasswordScoreUsingPOST1BodyParam = {};
    describe('#getPasswordScore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPasswordScore(userTrackingGetPasswordScoreUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.passwordScore);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'getPasswordScore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userTrackingUnlockUsersAccountUsingPOST1BodyParam = [
      'string'
    ];
    describe('#unlockUsersAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unlockUsersAccount(userTrackingUnlockUsersAccountUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'unlockUsersAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUserById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findUserById('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('LOCKED', data.response.accountStatus);
                assert.equal('string', data.response.creationDate);
                assert.equal(false, data.response.currentlyLogged);
                assert.equal('string', data.response.lastResetPasswordChangedDate);
                assert.equal('string', data.response.lastResetPasswordRequest);
                assert.equal('string', data.response.lastSuccessfulLogin);
                assert.equal('string', data.response.lockedTime);
                assert.equal(9, data.response.loginFailCount);
                assert.equal('string', data.response.remoteAddress);
                assert.equal(true, data.response.resetPassword);
                assert.equal('string', data.response.userName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'findUserById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAccountLocked - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userAccountLocked('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'userAccountLocked', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lockUserAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lockUserAccount('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'lockUserAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlockUserAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unlockUserAccount('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'unlockUserAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserGlobalSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserGlobalSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.defaultUnlockTime);
                assert.equal(false, data.response.expirePassword);
                assert.equal(8, data.response.maxLoginFailCount);
                assert.equal(5, data.response.noOfDaysToExpirePassword);
                assert.equal(6, data.response.passwordHistorySize);
                assert.equal(true, data.response.passwordPolicyDictionaryWords);
                assert.equal(true, data.response.passwordPolicyLowercase);
                assert.equal(false, data.response.passwordPolicyNumber);
                assert.equal(true, data.response.passwordPolicyPasswordHistory);
                assert.equal(false, data.response.passwordPolicySpecialchar);
                assert.equal(true, data.response.passwordPolicyUppercase);
                assert.equal(true, data.response.resetPasswordForFirstLogin);
                assert.equal(6, data.response.resetPasswordRequestTimeIntervalInSeconds);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'getUserGlobalSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userTrackingSaveUserGlobalSettingsUsingPOST1BodyParam = {
      defaultUnlockTime: 3,
      expirePassword: false,
      maxLoginFailCount: 1,
      noOfDaysToExpirePassword: 1,
      passwordHistorySize: 1,
      passwordPolicyDictionaryWords: true,
      passwordPolicyLowercase: true,
      passwordPolicyNumber: true,
      passwordPolicyPasswordHistory: false,
      passwordPolicySpecialchar: true,
      passwordPolicyUppercase: true,
      resetPasswordForFirstLogin: false,
      resetPasswordRequestTimeIntervalInSeconds: 1
    };
    describe('#saveUserGlobalSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveUserGlobalSettings(userTrackingSaveUserGlobalSettingsUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserTracking', 'saveUserGlobalSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadCommonFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadCommonFile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VersaCommonFileUpload', 'downloadCommonFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadCommonFileToVD - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadCommonFileToVD('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.customData);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.moduleType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.orgName);
                assert.equal('string', data.response.originalFileName);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VersaCommonFileUpload', 'uploadCommonFileToVD', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommonFileMiniSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCommonFileMiniSummary('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VersaCommonFileUpload', 'getCommonFileMiniSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommonFilesByType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCommonFilesByType('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VersaCommonFileUpload', 'getAllCommonFilesByType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCommonFilesByMultipleTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCommonFilesByMultipleTypes('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.customData);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.moduleType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.orgName);
                assert.equal('string', data.response.originalFileName);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VersaCommonFileUpload', 'getAllCommonFilesByMultipleTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const versaCommonFileUploadUpdateFileMetaUsingPUT1BodyParam = {
      customData: {},
      description: 'string',
      name: 'string',
      orgName: 'string',
      originalFileName: 'string'
    };
    describe('#updateCommonFileMeta - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCommonFileMeta(versaCommonFileUploadUpdateFileMetaUsingPUT1BodyParam, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VersaCommonFileUpload', 'updateCommonFileMeta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCommonFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCommonFile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VersaCommonFileUpload', 'deleteCommonFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuthRegisterClientByAdminUsingPOST1BodyParam = {
      access_token_validity: 3,
      allowed_grant_types: [
        'string'
      ],
      allowed_source_client_address: {
        ip_address_list: [
          'string'
        ],
        source_type: 'ANYWHERE'
      },
      client_secret_expires_at: 'string',
      client_uri: 'string',
      contacts: [
        {
          email: 'string',
          phone: 'string'
        }
      ],
      description: 'string',
      enabled: true,
      expires_at: 'string',
      icon_uri: 'string',
      max_access_tokens: 9,
      max_access_tokens_per_user: 7,
      name: 'string',
      redirect_uris: [
        'string'
      ],
      refresh_token_validity: 3,
      scope: [
        'string'
      ],
      software_id: 'string',
      software_version: 'string'
    };
    describe('#registerClientByAdmin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerClientByAdmin(oAuthRegisterClientByAdminUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.client_id);
                assert.equal('string', data.response.client_secret);
                assert.equal('string', data.response.client_secret_expires_at);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'registerClientByAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuthUpdateClientByAdminUsingPOST1BodyParam = {
      access_token_validity: 2,
      allowed_grant_types: [
        'string'
      ],
      allowed_source_client_address: {
        ip_address_list: [
          'string'
        ],
        source_type: 'CUSTOMIP'
      },
      client_secret_expires_at: 'string',
      client_uri: 'string',
      contacts: [
        {
          email: 'string',
          phone: 'string'
        }
      ],
      description: 'string',
      enabled: false,
      expires_at: 'string',
      icon_uri: 'string',
      max_access_tokens: 9,
      max_access_tokens_per_user: 3,
      name: 'string',
      redirect_uris: [
        'string'
      ],
      refresh_token_validity: 3,
      scope: [
        'string'
      ],
      software_id: 'string',
      software_version: 'string'
    };
    describe('#updateClientByAdmin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateClientByAdmin(oAuthUpdateClientByAdminUsingPOST1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'updateClientByAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClientByClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getClientByClient('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.access_token_validity);
                assert.equal(true, Array.isArray(data.response.allowed_grant_types));
                assert.equal('object', typeof data.response.allowed_source_client_address);
                assert.equal('string', data.response.client_id);
                assert.equal('string', data.response.client_name);
                assert.equal('string', data.response.client_secret_expires_at);
                assert.equal('string', data.response.client_uri);
                assert.equal(true, Array.isArray(data.response.contacts));
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.expires_at);
                assert.equal('string', data.response.icon_uri);
                assert.equal(4, data.response.max_access_tokens);
                assert.equal(4, data.response.max_access_tokens_per_user);
                assert.equal(true, Array.isArray(data.response.redirect_uris));
                assert.equal(8, data.response.refresh_token_validity);
                assert.equal(true, Array.isArray(data.response.scope));
                assert.equal('string', data.response.software_id);
                assert.equal('string', data.response.software_version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'getClientByClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuthUpdateClientUsingPOST1BodyParam = {
      client_uri: 'string',
      contacts: [
        {
          email: 'string',
          phone: 'string'
        }
      ],
      description: 'string',
      icon_uri: 'string',
      name: 'string',
      redirect_uris: [
        'string'
      ],
      scope: [
        'string'
      ],
      software_id: 'string',
      software_version: 'string'
    };
    describe('#updateClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateClient(oAuthUpdateClientUsingPOST1BodyParam, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'updateClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClientByClient - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteClientByClient('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'deleteClientByClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshClientSecrect - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refreshClientSecrect('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'refreshClientSecrect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExternalOAuthTokenServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateExternalOAuthTokenServer('fakedata', null, 'fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'updateExternalOAuthTokenServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oAuthRefreshAccessTokenUsingPOST1BodyParam = {
      client_id: 'string',
      client_secret: 'string',
      grant_type: 'string',
      refresh_token: 'string'
    };
    describe('#refreshAccessToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.refreshAccessToken(oAuthRefreshAccessTokenUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.AuthenticationResult);
                assert.equal('string', data.response.access_token);
                assert.equal('object', typeof data.response.auth_context);
                assert.equal('string', data.response.expires_in);
                assert.equal('string', data.response.issued_at);
                assert.equal('string', data.response.refresh_token);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.token_type);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'refreshAccessToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.revokeToken((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.AuthenticationResult);
                assert.equal('string', data.response.access_token);
                assert.equal('object', typeof data.response.auth_context);
                assert.equal('string', data.response.expires_in);
                assert.equal('string', data.response.issued_at);
                assert.equal('string', data.response.refresh_token);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.token_type);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OAuth', 'revokeToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateUserEMAILSecureCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateUserEMAILSecureCode('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserLoginTwoFactor', 'generateUserEMAILSecureCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateUserSMSSecureCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.generateUserSMSSecureCode('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserLoginTwoFactor', 'generateUserSMSSecureCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyUserSecureCodeAndRegisterAsync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyUserSecureCodeAndRegisterAsync('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserLoginTwoFactor', 'verifyUserSecureCodeAndRegisterAsync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllVendors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllVendors((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.vendors));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'getAllVendors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uCPEVendorsAddProductTypeUsingPUT1BodyParam = {};
    describe('#addVendorProductType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addVendorProductType('fakedata', uCPEVendorsAddProductTypeUsingPUT1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'addVendorProductType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVendorProductType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVendorProductType('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'deleteVendorProductType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDefinedVendors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserDefinedVendors((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.vendors));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'getUserDefinedVendors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uCPEVendorsAddVendorUsingPOST1BodyParam = {
      name: 'string',
      productType: [
        {
          details: 'string',
          logoPath: 'string',
          name: 'string',
          serialNo: 'string',
          serviceFuntions: [
            'string'
          ],
          vmSpec: {
            cpuCount: 9,
            diskSize: 5,
            isSecondaryDiskNeeded: true,
            memory: 3,
            secondaryDiskSize: 8
          }
        }
      ],
      serialNo: 'string',
      vendorProductType: [
        {
          details: 'string',
          logoPath: 'string',
          name: 'string',
          serialNo: 'string',
          serviceFuntions: [
            'string'
          ],
          vmSpec: {
            cpuCount: 2,
            diskSize: 2,
            isSecondaryDiskNeeded: true,
            memory: 10,
            secondaryDiskSize: 4
          }
        }
      ]
    };
    describe('#addVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addVendor(uCPEVendorsAddVendorUsingPOST1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'addVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchUserDefinedVendor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchUserDefinedVendor('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.vendors));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'searchUserDefinedVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVendors((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.vendors));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'getVendors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVendor('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UCPEVendors', 'deleteVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailableOrganizationIds - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAvailableOrganizationIds((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getAvailableOrganizationIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateMetadata - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateMetadata('fakedata', 'fakedata', 555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMetadata', 'getTemplateMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddServiceTemplateBodyParam = {};
    describe('#addServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addServiceTemplate(templateAddServiceTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportServiceTemplate('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'exportServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateCloneMasterTemplateBodyParam = {};
    describe('#cloneMasterTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cloneMasterTemplate(templateCloneMasterTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'cloneMasterTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateCreateTemplateOrgLANZoneBodyParam = {};
    describe('#createTemplateOrgLANZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTemplateOrgLANZone('fakedata', templateCreateTemplateOrgLANZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'createTemplateOrgLANZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateImportaServiceTemplateBodyParam = {};
    describe('#importaServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importaServiceTemplate(templateImportaServiceTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'importaServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExistingSpokeGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExistingSpokeGroups(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpokeGroup', 'getExistingSpokeGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplianceList(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceView', 'getApplianceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAvailableIdWithSerial - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSDWANAvailableIdWithSerial((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANAvailableIdWithSerial', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSDWANAvailableVRFIds - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSDWANAvailableVRFIds(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SDWANGlobalIDAPI', 'getSDWANAvailableVRFIds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemOperationsGetLatitudeLongitudeofAddressBodyParam = {};
    describe('#getLatitudeLongitudeofAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLatitudeLongitudeofAddress('fakedata', 'fakedata', 'fakedata', systemOperationsGetLatitudeLongitudeofAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemOperations', 'getLatitudeLongitudeofAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrgApplianceUUID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrgApplianceUUID((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrgApplianceUUID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationWANNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrganizationWANNetworks('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getOrganizationWANNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationCreateOrganizationWANNetworkBodyParam = {};
    describe('#createOrganizationWANNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrganizationWANNetwork('fakedata', 'fakedata', organizationCreateOrganizationWANNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'createOrganizationWANNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationGetBodyParam = {};
    describe('#get - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.get(organizationGetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'get', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBulkCommunityGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBulkCommunityGroup((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Community', 'getBulkCommunityGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const solutionTierUpdateSolutionTieronsetofDevicesBodyParam = {};
    describe('#updateSolutionTieronsetofDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateSolutionTieronsetofDevices(solutionTierUpdateSolutionTieronsetofDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SolutionTier', 'updateSolutionTieronsetofDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInterface('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInterfaceDetails('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getInterfaceDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPSecTunnelInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIPSecTunnelInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getIPSecTunnelInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLogicalTunnelInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLogicalTunnelInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getLogicalTunnelInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPhysicalInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getPhysicalInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaTunnelVirtualInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVersaTunnelVirtualInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getVersaTunnelVirtualInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaPseudoTunnelVirtualInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVersaPseudoTunnelVirtualInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getVersaPseudoTunnelVirtualInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaWlanInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVersaWlanInterface('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getVersaWlanInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersaWwanInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVersaWwanInterface('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getVersaWwanInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPInformationforActiveInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIPInformationforActiveInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getIPInformationforActiveInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPInformationforDynamicInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIPInformationforDynamicInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getIPInformationforDynamicInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficonPhysicalInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTrafficonPhysicalInterfaces('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getTrafficonPhysicalInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficonAllInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTrafficonAllInterfaces('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getTrafficonAllInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficonSpecificInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTrafficonSpecificInterfaces('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getTrafficonSpecificInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAmetricsforAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSLAmetricsforAppliance('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLA', 'getSLAmetricsforAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSLAmetricsforApplianceFilter - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSLAmetricsforApplianceFilter('fakedata', null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SLA', 'getSLAmetricsforApplianceFilter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSDWANPoliciesofanAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllSDWANPoliciesofanAppliance('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getAllSDWANPoliciesofanAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAvailableLicenseTiers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllAvailableLicenseTiers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LicenseKeyController', 'getAllAvailableLicenseTiers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDHCPOptionsProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrganizationDHCPOptionsProfile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getOrganizationDHCPOptionsProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationZones - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrganizationZones('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zone', 'getOrganizationZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePhysicalInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePhysicalInterfaces('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePhysicalInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateCgnatPoolBodyParam = {};
    describe('#addPostStagingTemplateCgnatPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateCgnatPool('fakedata', 'fakedata', 'fakedata', templateAddPostStagingTemplateCgnatPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateCgnatPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateNetworks('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateCgnatPools - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateCgnatPools('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateCgnatPools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateCgnatRuleBodyParam = {};
    describe('#addPostStagingTemplateCgnatRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateCgnatRule('fakedata', 'fakedata', 'fakedata', templateAddPostStagingTemplateCgnatRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateCgnatRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateCgnatRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateCgnatRules('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateCgnatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClassOfServiceInterfaceNetworkAssociation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteClassOfServiceInterfaceNetworkAssociation('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deleteClassOfServiceInterfaceNetworkAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateDhcpAddressPoolBodyParam = {};
    describe('#addPostStagingTemplateDhcpAddressPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateDhcpAddressPool('fakedata', 'fakedata', templateAddPostStagingTemplateDhcpAddressPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateDhcpAddressPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpAddressPools - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcpAddressPools('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateDhcpAddressPools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateDhcpLeaseProfileBodyParam = {};
    describe('#addPostStagingTemplateDhcpLeaseProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateDhcpLeaseProfile('fakedata', 'fakedata', templateAddPostStagingTemplateDhcpLeaseProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateDhcpLeaseProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpLeaseProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcpLeaseProfiles('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateDhcpLeaseProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateDhcpOptionsProfileBodyParam = {};
    describe('#addPostStagingTemplateDhcpOptionsProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateDhcpOptionsProfile('fakedata', 'fakedata', templateAddPostStagingTemplateDhcpOptionsProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateDhcpOptionsProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpOptionsProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcpOptionsProfiles('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateDhcpOptionsProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateDhcpServerBodyParam = {};
    describe('#addPostStagingTemplateDhcpServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateDhcpServer('fakedata', 'fakedata', templateAddPostStagingTemplateDhcpServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateDhcpServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateDhcpServers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcpServers('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateDhcpServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateZoneBodyParam = {};
    describe('#addPostStagingTemplateZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateZone('fakedata', 'fakedata', templateAddPostStagingTemplateZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateOrgLANZone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateOrgLANZone('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deleteTemplateOrgLANZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateStatefulFirewallAccessPolicyRuleBodyParam = {};
    describe('#addPostStagingTemplateStatefulFirewallAccessPolicyRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallAccessPolicyRule('fakedata', 'fakedata', templateAddPostStagingTemplateStatefulFirewallAccessPolicyRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateStatefulFirewallAccessPolicyRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateStatefulFirewallSecurityRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallSecurityRules('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateStatefulFirewallSecurityRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateAddPostStagingTemplateStatefulFirewallSecurityRulesBodyParam = {};
    describe('#addPostStagingTemplateStatefulFirewallSecurityRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateStatefulFirewallSecurityRules('fakedata', 'fakedata', 'fakedata', templateAddPostStagingTemplateStatefulFirewallSecurityRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'addPostStagingTemplateStatefulFirewallSecurityRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePostStagingTemplateStatefulFirewallAccessPolicyRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateStatefulFirewallAccessPolicyRules('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deletePostStagingTemplateStatefulFirewallAccessPolicyRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateVrfs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateVrfs('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getPostStagingTemplateVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateUpdatePostStagingTemplateVrfBodyParam = {};
    describe('#updatePostStagingTemplateVrf - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateVrf('fakedata', 'fakedata', templateUpdatePostStagingTemplateVrfBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'updatePostStagingTemplateVrf', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importServiceTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importServiceTemplate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'importServiceTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateImportTemplateStringBodyParam = {};
    describe('#importTemplateString - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importTemplateString('fakedata', templateImportTemplateStringBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'importTemplateString', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceUpdatePhysicalInterfacesBodyParam = {};
    describe('#updatePhysicalInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePhysicalInterfaces('fakedata', 'fakedata', interfaceUpdatePhysicalInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'updatePhysicalInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceAddPostStagingTemplateNetworkInterfaceBodyParam = {};
    describe('#addPostStagingTemplateNetworkInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateNetworkInterface('fakedata', interfaceAddPostStagingTemplateNetworkInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'addPostStagingTemplateNetworkInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateNetworkInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateNetworkInterface('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getPostStagingTemplateNetworkInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateCgnatRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateCgnatRules('fakedata', 'fakedata', null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rule', 'getPostStagingTemplateCgnatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ruleUpdatePostStagingTemplateCgnatRulesBodyParam = {};
    describe('#updatePostStagingTemplateCgnatRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateCgnatRules('fakedata', 'fakedata', 'fakedata', null, ruleUpdatePostStagingTemplateCgnatRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rule', 'updatePostStagingTemplateCgnatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateDhcp6LeaseProfile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getPostStagingTemplateDhcp6LeaseProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPCreatePostStagingTemplateDhcp6LeaseProfileBodyParam = {};
    describe('#createPostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPostStagingTemplateDhcp6LeaseProfile('fakedata', 'fakedata', dHCPCreatePostStagingTemplateDhcp6LeaseProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'createPostStagingTemplateDhcp6LeaseProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPUpdatePostStagingTemplateDhcp6LeaseProfileBodyParam = {};
    describe('#updatePostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6LeaseProfile('fakedata', 'fakedata', 'fakedata', dHCPUpdatePostStagingTemplateDhcp6LeaseProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'updatePostStagingTemplateDhcp6LeaseProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPDeletePostStagingTemplateDhcp6LeaseProfileBodyParam = {};
    describe('#deletePostStagingTemplateDhcp6LeaseProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6LeaseProfile('fakedata', 'fakedata', 'fakedata', dHCPDeletePostStagingTemplateDhcp6LeaseProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deletePostStagingTemplateDhcp6LeaseProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateDhcp6OptionsProfile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getPostStagingTemplateDhcp6OptionsProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPCreatePostStagingTemplateDhcp6OptionsProfileBodyParam = {};
    describe('#createPostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPostStagingTemplateDhcp6OptionsProfile('fakedata', 'fakedata', dHCPCreatePostStagingTemplateDhcp6OptionsProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'createPostStagingTemplateDhcp6OptionsProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPUpdatePostStagingTemplateDhcp6OptionsProfileBodyParam = {};
    describe('#updatePostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6OptionsProfile('fakedata', 'fakedata', 'fakedata', dHCPUpdatePostStagingTemplateDhcp6OptionsProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'updatePostStagingTemplateDhcp6OptionsProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPDeletePostStagingTemplateDhcp6OptionsProfileBodyParam = {};
    describe('#deletePostStagingTemplateDhcp6OptionsProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6OptionsProfile('fakedata', 'fakedata', 'fakedata', dHCPDeletePostStagingTemplateDhcp6OptionsProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deletePostStagingTemplateDhcp6OptionsProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateDhcp6AddressPool('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getPostStagingTemplateDhcp6AddressPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPCreatePostStagingTemplateDhcp6AddressPoolBodyParam = {};
    describe('#createPostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPostStagingTemplateDhcp6AddressPool('fakedata', 'fakedata', dHCPCreatePostStagingTemplateDhcp6AddressPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'createPostStagingTemplateDhcp6AddressPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPUpdatePostStagingTemplateDhcp6AddressPoolBodyParam = {};
    describe('#updatePostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6AddressPool('fakedata', 'fakedata', 'fakedata', dHCPUpdatePostStagingTemplateDhcp6AddressPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'updatePostStagingTemplateDhcp6AddressPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPDeletePostStagingTemplateDhcp6AddressPoolBodyParam = {};
    describe('#deletePostStagingTemplateDhcp6AddressPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6AddressPool('fakedata', 'fakedata', 'fakedata', dHCPDeletePostStagingTemplateDhcp6AddressPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deletePostStagingTemplateDhcp6AddressPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateDhcp6Server - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateDhcp6Server('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'getPostStagingTemplateDhcp6Server', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPCreatePostStagingTemplateDhcp6ServerBodyParam = {};
    describe('#createPostStagingTemplateDhcp6Server - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPostStagingTemplateDhcp6Server('fakedata', 'fakedata', dHCPCreatePostStagingTemplateDhcp6ServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'createPostStagingTemplateDhcp6Server', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPUpdatePostStagingTemplateDhcp6ServerBodyParam = {};
    describe('#updatePostStagingTemplateDhcp6Server - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateDhcp6Server('fakedata', 'fakedata', 'fakedata', dHCPUpdatePostStagingTemplateDhcp6ServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'updatePostStagingTemplateDhcp6Server', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dHCPDeletePostStagingTemplateDhcp6ServerBodyParam = {};
    describe('#deletePostStagingTemplateDhcp6Server - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePostStagingTemplateDhcp6Server('fakedata', 'fakedata', 'fakedata', dHCPDeletePostStagingTemplateDhcp6ServerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DHCP', 'deletePostStagingTemplateDhcp6Server', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceNodeUpdatePostStagingTemplateServiceNodeGroupBodyParam = {};
    describe('#updatePostStagingTemplateServiceNodeGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateServiceNodeGroup('fakedata', 'fakedata', serviceNodeUpdatePostStagingTemplateServiceNodeGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceNode', 'updatePostStagingTemplateServiceNodeGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkAddPostStagingTemplateNetworkBodyParam = {};
    describe('#addPostStagingTemplateNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addPostStagingTemplateNetwork('fakedata', networkAddPostStagingTemplateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'addPostStagingTemplateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateOrgLimits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateOrgLimits('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'getPostStagingTemplateOrgLimits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationUpdatePostStagingTemplateOrgLimitsBodyParam = {};
    describe('#updatePostStagingTemplateOrgLimits - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateOrgLimits('fakedata', 'fakedata', organizationUpdatePostStagingTemplateOrgLimitsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organization', 'updatePostStagingTemplateOrgLimits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPostStagingTemplateIpsecVpnProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPostStagingTemplateIpsecVpnProfile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VPN', 'getPostStagingTemplateIpsecVpnProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vPNUpdatePostStagingTemplateIpsecVpnProfileBodyParam = {};
    describe('#updatePostStagingTemplateIpsecVpnProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePostStagingTemplateIpsecVpnProfile('fakedata', 'fakedata', 'fakedata', null, vPNUpdatePostStagingTemplateIpsecVpnProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VPN', 'updatePostStagingTemplateIpsecVpnProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceMgmtCreateApplianceFileObjectBodyParam = {};
    describe('#createApplianceFileObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createApplianceFileObject(applianceMgmtCreateApplianceFileObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'createApplianceFileObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceFileObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplianceFileObject('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'deleteApplianceFileObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceMgmtEditApplianceFileObjectBodyParam = {};
    describe('#editApplianceFileObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editApplianceFileObject('fakedata', applianceMgmtEditApplianceFileObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'editApplianceFileObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceMgmtEditAppliaceConfigurationBodyParam = {};
    describe('#editAppliaceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editAppliaceConfiguration(applianceMgmtEditAppliaceConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'editAppliaceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliaceConfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliaceConfiguration((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'getAppliaceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceFileObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplianceFileObject((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'getApplianceFileObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceById - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplianceById('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'getApplianceById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceNamesAndUuids - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getApplianceNamesAndUuids((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceMgmt', 'getApplianceNamesAndUuids', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeMgmtCreateRouteObjectBodyParam = {};
    describe('#createRouteObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createRouteObject(routeMgmtCreateRouteObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMgmt', 'createRouteObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouteObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRouteObject('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMgmt', 'deleteRouteObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routeMgmtEditRouteObjectBodyParam = {};
    describe('#editRouteObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editRouteObject('fakedata', 'fakedata', 'fakedata', routeMgmtEditRouteObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMgmt', 'editRouteObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouteObjectList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRouteObjectList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RouteMgmt', 'getRouteObjectList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseAlarmsSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLicenseAlarmsSummary((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FaultMgmt', 'getLicenseAlarmsSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const faultMgmtCreateBulkAlarmsBodyParam = {};
    describe('#createBulkAlarms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkAlarms(faultMgmtCreateBulkAlarmsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FaultMgmt', 'createBulkAlarms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const faultMgmtCreateBulkAlarmByUserBodyParam = {};
    describe('#createBulkAlarmByUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createBulkAlarmByUser(faultMgmtCreateBulkAlarmByUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FaultMgmt', 'createBulkAlarmByUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmsByAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAlarmsByAppliance('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FaultMgmt', 'getAlarmsByAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlarmsSummary - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAlarmsSummary((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FaultMgmt', 'getAlarmsSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const faultMgmtUpdateBulkAlarmBodyParam = {};
    describe('#updateBulkAlarm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateBulkAlarm(faultMgmtUpdateBulkAlarmBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FaultMgmt', 'updateBulkAlarm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtCreateTemplateDevicesBodyParam = {};
    describe('#createTemplateDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTemplateDevices(templateMgmtCreateTemplateDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'createTemplateDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtCreateTemplateAttributesBodyParam = {};
    describe('#createTemplateAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTemplateAttributes('fakedata', 'fakedata', templateMgmtCreateTemplateAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'createTemplateAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtCreateTemplateTunnelBodyParam = {};
    describe('#createTemplateTunnel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTemplateTunnel('fakedata', templateMgmtCreateTemplateTunnelBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'createTemplateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateDeviceGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateDeviceGroups('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'deleteTemplateDeviceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtEditTemplateDeviceGroupsBodyParam = {};
    describe('#editTemplateDeviceGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editTemplateDeviceGroups('fakedata', templateMgmtEditTemplateDeviceGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'editTemplateDeviceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateDeviceTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateDeviceTemplates('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'deleteTemplateDeviceTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtEditTemplateDeviceTemplatesBodyParam = {};
    describe('#editTemplateDeviceTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editTemplateDeviceTemplates('fakedata', templateMgmtEditTemplateDeviceTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'editTemplateDeviceTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateManagedDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateManagedDevices('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'deleteTemplateManagedDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtEditTemplateManagedDevicesBodyParam = {};
    describe('#editTemplateManagedDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editTemplateManagedDevices('fakedata', templateMgmtEditTemplateManagedDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'editTemplateManagedDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateAttributes('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'deleteTemplateAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtEditTemplateAttributesBodyParam = {};
    describe('#editTemplateAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editTemplateAttributes('fakedata', 'fakedata', 'fakedata', templateMgmtEditTemplateAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'editTemplateAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateTunnel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateTunnel('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'deleteTemplateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateMgmtEditTemplateTunnelBodyParam = {};
    describe('#editTemplateTunnel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editTemplateTunnel('fakedata', 'fakedata', templateMgmtEditTemplateTunnelBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'editTemplateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDeviceGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateDeviceGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'getTemplateDeviceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateDeviceTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateDeviceTemplates((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'getTemplateDeviceTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateManagedDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateManagedDevices((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'getTemplateManagedDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateAttributes('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'getTemplateAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTunnel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateTunnel('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplateMgmt', 'getTemplateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pOCEditClassOfServiceBodyParam = {};
    describe('#editClassOfService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editClassOfService('fakedata', 'fakedata', 'fakedata', pOCEditClassOfServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('POC', 'editClassOfService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pOCApplyTemplateBodyParam = {};
    describe('#applyTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applyTemplate('fakedata', true, 'fakedata', pOCApplyTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('POC', 'applyTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pOCTemplate = 'fakedata';
    const pOCOrganization = 'fakedata';
    const pOCPolicygroup = 'fakedata';
    const pOCPolicy = 'fakedata';
    const pOCEditTemplateSecurityPolicyBodyParam = {};
    describe('#editTemplateSecurityPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editTemplateSecurityPolicy(pOCTemplate, pOCOrganization, pOCPolicygroup, pOCPolicy, pOCEditTemplateSecurityPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('POC', 'editTemplateSecurityPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorMgmtUpdateMonitoringDatabaseBodyParam = {};
    describe('#updateMonitoringDatabase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateMonitoringDatabase(monitorMgmtUpdateMonitoringDatabaseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorMgmt', 'updateMonitoringDatabase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorMgmtUpdateMonitoringStatusBodyParam = {};
    describe('#updateMonitoringStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateMonitoringStatus('fakedata', monitorMgmtUpdateMonitoringStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorMgmt', 'updateMonitoringStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorMgmtStartMonitorPollingBodyParam = {};
    describe('#startMonitorPolling - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.startMonitorPolling(monitorMgmtStartMonitorPollingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorMgmt', 'startMonitorPolling', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const monitorMgmtRefreshMonitorDataBodyParam = {};
    describe('#refreshMonitorData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refreshMonitorData(monitorMgmtRefreshMonitorDataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorMgmt', 'refreshMonitorData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringCacheApplianceDataForTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMonitoringCacheApplianceDataForTenant('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorMgmt', 'getMonitoringCacheApplianceDataForTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringCacheApplianceData - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMonitoringCacheApplianceData('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorMgmt', 'getMonitoringCacheApplianceData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMonitoringCacheDataForTenant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMonitoringCacheDataForTenant('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MonitorMgmt', 'getMonitoringCacheDataForTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateFromOrganization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplateFromOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('template1', data.response.val[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getTemplateFromOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransportDomains - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransportDomains('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getTransportDomains', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDashboardApplianceHardware - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDashboardApplianceHardware('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getDashboardApplianceHardware', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancePageableRoutes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancePageableRoutes('fakedata', 'fakedata', 'fakedata', null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'getAppliancePageableRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancePageableArp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancePageableArp('fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'getAppliancePageableArp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancePageableArpOrgName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAppliancePageableArpOrgName('fakedata', 'fakedata', null, null, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'getAppliancePageableArp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const operationsAppliancePingBodyParam = {};
    describe('#appliancePing - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appliancePing('fakedata', operationsAppliancePingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Operations', 'appliancePing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOwnedRoutingInstances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOwnedRoutingInstances('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getOwnedRoutingInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPageableInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPageableInterfaces('fakedata', 'fakedata', 'fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getPageableInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceVni - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInterfaceVni('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getInterfaceVni', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoutingInstancesRoutingInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRoutingInstancesRoutingInstance('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interface', 'getRoutingInstancesRoutingInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworks('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-versa_director-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'getNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
